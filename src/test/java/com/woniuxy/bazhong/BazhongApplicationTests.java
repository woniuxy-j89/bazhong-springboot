package com.woniuxy.bazhong;

import com.woniuxy.bazhong.entity.Student;
import com.woniuxy.bazhong.service.*;
import com.woniuxy.bazhong.mapper.UserMapper;
import com.woniuxy.bazhong.service.UserService;
import org.apache.poi.ss.formula.functions.Today;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class BazhongApplicationTests {
    @Resource
    private UserService userService;
    @Resource
    private ArticleService articleService;
    @Resource
    private VideoService videoService;
    @Resource
    private StudentService studentService;
    @Resource
    private UserMapper userMapper;

    @Test
    void contextLoads() {
        System.out.println(userService.all());
    }

    @Test
    public void find() {
        System.out.println(userService.findTeacherUserBySchool(1));
        System.out.println(userService.findPlacementStudent(2022, 1));
    }

    @Test
    public void finds() {
        //System.out.println(articleService.findArticleByLike(1,2,"%q%"));
        //System.out.println(videoService.findVideoByLike(1,2,"1"));
        //System.out.println(articleService.findToAudit(1,2,1));

        studentService.updateStudentChoice("1", 1);
    }

    @Test
    public void findByUsername() {
//        System.out.println(userMapper.findUserByAccount("student"));
//        System.out.println(articleService.findAllArticle(1,1));
        System.out.println(videoService.findAllVideo(1, 1));
    }

    @Test
    public void test() {

        int a = 10;

        int b = 10;

        System.out.println(a == b);            //_____________

        Integer i1 = 100;

        int i2 = 100;

        System.out.println(i1 == i2);            //_____________

        Integer i3 = 200;

        Integer i4 = 200;

        System.out.println(i3 == i4);            //_____________

        System.out.println(i3.equals(i4));        //_____________

        Integer i5 = 256;

        Integer i6 = 256;

        System.out.println(i5 == i6);            //_____________

        System.out.println(i5.equals(i6));        //_____________

    }

    @Test
    public void test1() {
        String text = "Today I will tell you a sad story, I believe that you will understand after listening to.";

        char[] str = new char[text.length()];
        for (int i = 0; i < text.length(); i++) {
            str[i] = text.charAt(i); //使用charAt()方法将字符串中的每个元素转换为字符串
        }

        System.out.println(String.copyValueOf(str));

        char[] result = new char[str.length];

        for (int i = 0; i < str.length; i++) {
            if (str[i] != ' ') {
                result[i] = str[i];
            } else {
                result[i] = str[i];
                if (str[i + 1] != 'I') {
                    result[i + 1] = (char) (str[i + 1] - 32);
                    i++;
                }
            }

        }

        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i]);
        }

    }

    @Test
    public void test11() {
        String text = "Today Y Y Y will tell you a sad story, I believe that you will understand after listening to.";

        //将字符串text 转换 为字符数组str
        char[] str = new char[text.length()];
        for (int i = 0; i < text.length(); i++) {
            str[i] = text.charAt(i);
        }

        System.out.println(String.copyValueOf(str));

        //准备一个新的字符数组来存结果
        char[] result = new char[str.length];

        //通过for循环，来找到每个单词的首字母
        for (int i = 0; i < str.length; i++) {
            //如果当前字符不是空格，就直接存入result数组
            if (str[i] != ' ') {
                result[i] = str[i];
            } else { //如果当前字符是空格，就处理下一个单词，即首字母
                result[i] = str[i];
                //如果下一个单词是大写就不处理
                if (str[i + 1] <= 65 && str[i + 1] >= 90 ) {
                    result[i + 1] = (char) (str[i + 1] - 32);
                    i++;
                }
            }

        }

        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i]);
        }
        System.out.println("");
        System.out.println("OK_RAO");
    }
}



