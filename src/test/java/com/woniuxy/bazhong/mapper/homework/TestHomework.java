package com.woniuxy.bazhong.mapper.homework;

import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.mapper.HomeworkMapper;
import com.woniuxy.bazhong.mapper.StudentMapper;
import com.woniuxy.bazhong.service.HomeworkService;
import com.woniuxy.bazhong.service.StudentService;
import com.woniuxy.bazhong.utils.CheckIsExpiredUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhangjy
 * @date 2022/7/23 16:29
 */
@Slf4j
@SpringBootTest
public class TestHomework {

    @Resource
    private HomeworkMapper homeworkMapper;
    @Resource
    private HomeworkService homeworkService;

    /**
     * 根据学生id查询所有作业
     */
    @Test
    public void testAll() {
        // 查询所有作业
        List<HomeworkStudent> homeworkStudents = homeworkMapper.findAllByStudentId(1);

        System.out.println(homeworkStudents);
    }

    @Test
    public void testFindById() {
        // 根据id查询作业
        Homework homework = homeworkMapper.findById(2);
        System.out.println(homework);
    }

    /**
     * 查看或编写一个作业
     */
    @Test
    public void testFindByStudentIdAndHomeworkId() {
        int studentId = 2;
        int homeworkId = 1;
        HomeworkStudent homeworkStudent = homeworkMapper.findByStudentIdAndHomeworkId(studentId, homeworkId);

        // 判断homeworkStudent是否过期，并设置isExpired属性
        homeworkStudent = CheckIsExpiredUtil.checkAndSetIsExpired(homeworkStudent);
        System.out.println("before查询对应题目表：" + homeworkStudent);

        List<HomeworkTopic> homeworkTopics = homeworkStudent.getHomeworkTopics();

        int questiontypesId = 0;
        int topicId = 0;

        log.info("作业过期状态---------" + homeworkStudent.getIsExpired());
        log.info("作业是否提交(0未提交，1已提交)---------" + homeworkStudent.getStatus());

        // 根据题型id和题目id，查询相应的题型表，得到指定题目的所有信息
        for (HomeworkTopic homeworkTopic : homeworkTopics) {
            // 得到题型id和题目id
            questiontypesId = homeworkTopic.getQuestiontypesId();
            topicId = homeworkTopic.getTopicId();

            log.info("这道题的作业题目id是：" + homeworkTopic.getId() + "...题型id为：" + questiontypesId + "...题目id为：" + topicId);

            // 判断题目的题型
            if (questiontypesId == 1) { // 选择题
                // 查询该题目信息
                Choice choice = homeworkMapper.findChoice(questiontypesId, topicId);
//                homeworkTopic.setTopicInfo(choice);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setChoice(choice);
            } else if (questiontypesId == 2) { // 填空题
                // 查询该题目信息
                Blank blank = homeworkMapper.findBlank(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setBlank(blank);
            } else if (questiontypesId == 3) { // 判断题
                // 查询该题目信息
                Bank bank = homeworkMapper.findBank(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setBank(bank);
            } else if (questiontypesId == 4) { // 阅读题
                // 查询该题目信息
                Reading reading = homeworkMapper.findReading(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setReading(reading);
            } else if (questiontypesId == 5) { // 解答题
                // 查询该题目信息
                Responsequestion responsequestion = homeworkMapper.findResponsequestion(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setResponsequestion(responsequestion);
            } else if (questiontypesId == 6) { // 作文题
                // 查询该题目信息
                Title title = homeworkMapper.findTitle(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setTitle(title);
            }
        }

        //
        System.out.println("after：" + homeworkStudent);
    }

    @Test
    public void testAddHomeworkAnswer() {
        int studentId = 1;
        int homeworkId = 1;
        int homeworkTopicId = 8;
        String answer = "D";
        boolean flag = homeworkMapper.addHomeworkAnswer(studentId, homeworkId, homeworkTopicId, answer);

        if (flag) {
            log.info("答案提交成功");

            // 查询该学生该作业的所有信息
            HomeworkStudent homeworkStudent = homeworkService.findByStudentIdAndHomeworkId(studentId, homeworkId);

            System.out.println(homeworkStudent);
        } else {
            log.info("提交失败");
        }
    }

    @Test
    public void testUpdateHomeworkAnswer() {
        int studentId = 1;
        int homeworkId = 1;
        int homeworkTopicId = 8;
        String answer = "C";

        homeworkService.addHomeworkAnswer(studentId, homeworkId, homeworkTopicId, answer);
    }

}
