package com.woniuxy.bazhong.mapper.questions;

import com.woniuxy.bazhong.entity.Questions;
import com.woniuxy.bazhong.entity.Student;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.entity.Video;
import com.woniuxy.bazhong.mapper.QuestionsMapper;
import com.woniuxy.bazhong.service.QuestionsService;
import com.woniuxy.bazhong.service.StudentService;
import com.woniuxy.bazhong.service.UserService;
import com.woniuxy.bazhong.service.VideoService;
import com.woniuxy.bazhong.utils.DateUtil;
import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhangjy
 * @date 2022/8/3 17:13
 */
@Slf4j
@SpringBootTest
public class TestQuestions {

    @Resource
    private QuestionsService questionsService;
    @Resource
    private QuestionsMapper questionsMapper;
    @Resource
    private StudentService studentService;
    @Resource
    private UserService userService;
    @Resource
    private VideoService videoService;

    // 测试插入问题
    @Test
    public void testAdd() {
        int studentId = 1;
        String questionsContent = "这是我的问题";
        String status = "1";    // 不公开

        boolean flag = false;

        // 获取学生提问时间
        String questionTime = DateUtil.getDate();

        Questions questions = new Questions();
        questions.setStudentId(studentId);
        questions.setQuestionsContent(questionsContent);
        questions.setStatus(status);
        questions.setQuestionTime(questionTime);

        log.info("插入前问题id为：" + questions.getId());

        // 将问题存入数据库
        flag = questionsMapper.add(questions);

        log.info("测试类插入结果：" + flag);

    }

    // 测试问题知识点第三方表插入数据
    @Test
    public void testAddQuestionsAndKnowledgePoint() {
        int questionsId = 3;
        int knowledgePointId = 1;

        boolean b = questionsMapper.addQuestionsAndKnowledgePoint(questionsId, knowledgePointId);

        log.info("插入结果：" + b);
    }

    // 学生根据知识点提问
    @Test
    public void testAddByKnowledgePoint() {
        int studentId = 1;
        int knowledgePointId = 1;
        String questionsContent = "这个问题公开！！！";
        String status = "0";    // 公开

        boolean flag = questionsService.addByKnowledgePoint(studentId, knowledgePointId, questionsContent, status);

        log.info("插入结果：" + flag);
    }

    // 根据学生id得到所在学生id
    @Test
    public void testFindSchoolIdByStudentId() {
        int studentId = 1;

        Student student = studentService.findById(studentId);
        log.info("该学生的学生信息：" + student);

        User user = userService.findById(student.getUserId());
        log.info("该学生的用户信息为：" + user);
    }

    // 分页查询所有视频
    @Test
    public void testFindAllVideoByCondition() {
        int studentId = 1;
        int page = 1;
        int size = 10;

        PageMessage<List<Video>> all = videoService.allByCondition(studentId, page, size);
    }

}
