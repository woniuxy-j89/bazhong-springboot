package com.woniuxy.bazhong.mapper.knowledgepoint;

import com.woniuxy.bazhong.entity.KnowledgePoint;
import com.woniuxy.bazhong.mapper.KnowledgePointMapper;
import com.woniuxy.bazhong.service.KnowledgePointService;
import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhangjy
 * @date 2022/8/3 0:27
 */
@Slf4j
@SpringBootTest
public class TestKnowledgePoint {

    @Resource
    private KnowledgePointMapper knowledgePointMapper;

    @Resource
    private KnowledgePointService knowledgePointService;

    // 条件查询所有知识点
    @Test
    public void findAll() {
        List<KnowledgePoint> knowledgePoints = knowledgePointMapper.findAllKnowledgePoint(0, null);

        log.info(knowledgePoints.toString());
    }

    // 分页、条件查询所有知识点
    @Test
    public void findAllByPageAndConditional() {
        int subjectId = 2;
        String status = "2";
        int page = 1;
        int size = 10;

        PageMessage<List<KnowledgePoint>> all = knowledgePointService.all(subjectId, status, page, size);
    }

    @Test
    public void testFindById() {
        KnowledgePoint knowledgePoint = knowledgePointMapper.findById(1);
        System.out.println(knowledgePoint);
    }
}
