
package com.woniuxy.bazhong.Filter;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.woniuxy.bazhong.utils.JWTUtil;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import com.woniuxy.bazhong.utils.TokenEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * 作用是：得到token中的账号信息，然后通过账号信息得到用户信息（权限）
 * 然后手动生成authentication对象，放到springsecurity的上下文中
 */


@Slf4j
@Component
public class JwtOncePerRequestFilter extends OncePerRequestFilter {

    @Resource
    private UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // 获取token
        String token = request.getHeader("authorization");
        log.info(token);

        if (requireAuth(request.getRequestURI())){

            // 设置响应头
            response.setContentType("application/json;charset=utf-8");
            //
            ResponseResult<Object> responseResult = new ResponseResult<>();
            responseResult.setCode(401);   // 401表示没登录
            responseResult.setStatus(ResponseStatus.NO_LOGIN);
            responseResult.setMessage("请登录之后再操作");
            //
            String json = new ObjectMapper().writeValueAsString(responseResult);

            // 对token进行校验
            if (token == null || token.length() == 0 || token.equals("null")){
                // 返回结果，叫用户去登录
                response.getWriter().write(json);
                return;
            }
            // 程序执行到此处说明前端提供了token
            TokenEnum result = JWTUtil.verify(token);

            if(result == TokenEnum.TOKEN_SUCCESS){

                // 得到当前账号
                String username = JWTUtil.getUname(token);

                // 得到用户权限信息
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                // 封装成token对象
                UsernamePasswordAuthenticationToken authenticationToken
                        = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());

                // 设置到上下文，表示当前用户已经登录了
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);

            }else{
                // 伪造或者过期，让去登录
                response.getWriter().write(json);
                return;
            }

        }

        //放行
        filterChain.doFilter(request,response);

    }

    // 判断请求的uri是否需要认证之后才能请求
    private boolean requireAuth(String uri){
        // 匿名访问
        List<String> urls = Arrays.asList(
                "auth/login","/video/","/WebSocketHandler/","/exam/","/homework/","/test/"
        );
        // 记录是否需要认证
        AtomicBoolean flag = new AtomicBoolean(true);
        urls.forEach(str -> {
            if (uri.endsWith(str)||uri.startsWith(str)){
                flag.set(false);
                return;
            }
        });
        return flag.get();
    }
}
