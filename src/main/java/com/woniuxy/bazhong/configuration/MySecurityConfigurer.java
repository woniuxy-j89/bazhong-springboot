//
//package com.woniuxy.bazhong.configuration;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//import org.springframework.security.web.SecurityFilterChain;
//
//@Configuration
//public class MySecurityConfigurer extends WebSecurityConfigurerAdapter {
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.
//                inMemoryAuthentication()
//                .withUser("root")
//                .password("{noop}123")
//                .roles("ADMIN", "SUPER");
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests().mvcMatchers("/hello").permitAll()
//                .anyRequest().authenticated()
//                .and().formLogin()
//                .and()
//                .csrf().disable();
//    }
//}
package com.woniuxy.bazhong.configuration;

import com.woniuxy.bazhong.Filter.JwtOncePerRequestFilter;
import com.woniuxy.bazhong.handler.ClassAuthenticationExceptionHandle;
import com.woniuxy.bazhong.handler.MyExceptionHandler;
import com.woniuxy.bazhong.service.impl.UserDetailServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;

@Configuration
public class MySecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Resource
    private UserDetailsService userDetailsService;


    @Resource
    private JwtOncePerRequestFilter jwtOncePerRequestFilter;
    //密码解析器
    //常见的两种 : 不加密直接比较 一种是 通过hash加密之后再比较密码
    @Bean
    public PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();//不加密
        //return new BCryptPasswordEncoder(); //登录是security会采用该加密方式进行加密之后再与数据库进行比较
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        /*auth.
                inMemoryAuthentication()
                .withUser("root")
                .password("{noop}123")
                .roles("ADMIN", "SUPER");*/
        // 配置用户信息来自数据库 并且 指定密码编码器，目的是告诉springsecurity是否需要对用户提交的密码进行加密然后再比较
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/auth/login").anonymous()  //注册url可以匿名访问，不需要登录
                .antMatchers("/video/**").anonymous()  //注册url可以匿名访问，不需要登录
                .antMatchers("/WebSocketHandler/**").anonymous()  //注册url可以匿名访问，不需要登录
                .antMatchers("/exam/**").anonymous()  //注册url可以匿名访问，不需要登录
                .antMatchers("/homework/**").anonymous()  //注册url可以匿名访问，不需要登录
                .antMatchers("/test/**").anonymous()  //注册url可以匿名访问，不需要登录


                .anyRequest().authenticated() //其它的url必须登录之后才能访问，任何角色的用户都可以访问
                .and()
                .exceptionHandling() //
                .accessDeniedHandler(new MyExceptionHandler()) // 将自定义的异常处理解析器配置到security
                .and().formLogin()// 重写了当前类，security就不再支持form表单登录，如果想要支持，就需要调用该方法
                .and()
                .csrf().disable()
                .cors()// 开启springsecurity的跨域
                .and()
                // 自定义分过滤器需要在UsernamePasswordAuthenticationFilter之前执行，目的是为了手动生成authentication对象
                // 放到上下文中，就是为了让UsernamePasswordAuthenticationFilter得到用户信息，就认为用户登录了
               .addFilterBefore(jwtOncePerRequestFilter, UsernamePasswordAuthenticationFilter.class);
        http.headers().frameOptions().disable();
    }

    @Bean   // 配置认证管理器
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
