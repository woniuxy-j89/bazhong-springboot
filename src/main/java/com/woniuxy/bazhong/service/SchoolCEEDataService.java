package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.EnrolmentRate;
import com.woniuxy.bazhong.entity.School;

import java.util.List;

public interface SchoolCEEDataService {
    /**
     * 各校同级数据对比模块，展示学校列表
     * @return List<School>学校列表
     */
    List<School> allSchool();

    /**
     * 各校同级数据对比模块，展示升学率
     * @param schoolId 学校id
     * @return List<EnrolmentRate>
     */
    List<EnrolmentRate> showEnrolmentRate(int schoolId);
}
