package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Student;

public interface StudentService {
    /**
     * 根据id查找学生
     * @param id
     * @return Student
     */
    public Student findById(int id);

    /**
     * 更改文理科
     * @param liberaScience
     * @param id
     * @return
     */
    public boolean updateStudentChoice(String liberaScience,int id);


    /**
     * 根据用户id查找学生
     * @param userId
     * @return
     */
    public Student findByUserId(int userId);
}
