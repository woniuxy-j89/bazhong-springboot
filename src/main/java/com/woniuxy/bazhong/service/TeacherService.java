package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.utils.PageMessage;

import java.util.List;

public interface TeacherService {
    public Teacher findById(int id);

    // 通过Qid查询问题
    public Questions findQuestionsByQid(int id);

    // 分页查询该教师所有问题
    public PageMessage<List<Questions>> findQuestionsByTid(int page, int size, int teacherId);

    // 回答问题
    public boolean answer(Questions questions);

    // 自动生成作业
    public boolean uploadHomework(Homework homework, int knowledgepointId, String questionsTypeName, int num);

    // 设置完成时间
    public boolean setFinalTime(int homeworkId, String finalTime);

    // 试卷上传
    public int uploadExam(Exam exam);

    public PageMessage<List<Video>> findVideoByTid(int page, int size, int teacherId);

    public PageMessage<Teacher> teacherOfClassess(int teacherId, int page, int size);
}
