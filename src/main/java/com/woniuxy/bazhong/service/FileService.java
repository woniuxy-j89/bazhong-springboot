package com.woniuxy.bazhong.service;


import com.woniuxy.bazhong.entity.QuestionTypes;
import com.woniuxy.bazhong.entity.Video;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface FileService {
    // 视频上传
    public boolean uploadVideo(String filePath, String name);

    // 上传选择题
    public boolean uploadChoice(String fileName, MultipartFile file, int examId) throws IOException;

    // 上传填空题
    public boolean uploadBlank(String fileName, MultipartFile file, int examId) throws IOException;

    // 上传阅读题
    public boolean uploadReading(String fileName, MultipartFile file, int examId) throws IOException;

    // 上传解答题
    public boolean uploadResponsequestion(String fileName, MultipartFile file, int examId);

    // 上传作文题
    public boolean uploadTitle(String fileName, MultipartFile file, int examId) throws IOException;

    public String findModel(int subjectId, String type);

    public List<QuestionTypes> findTypeBySid(int subjectId);
}
