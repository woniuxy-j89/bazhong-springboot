package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Student;
import com.woniuxy.bazhong.utils.PageMessage;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by raoyucheng
 * on 2022/7/22 14:25
 */
public interface HeadteacherService {
    //批量添加学生账号
    public boolean addStudents(String fileName, MultipartFile file) throws Exception;

    boolean resetStudentPassword(int studentNumber);

    boolean addStudentTotalPoint(String fileName, MultipartFile file)throws Exception;

    PageMessage<List<Student>> allStudent(int page, int size);

    String findStuEmailByNumber(String studentNumbers);

    boolean addHighTestScores(String fileName, MultipartFile file) throws Exception;
}
