package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Homework;
import com.woniuxy.bazhong.entity.HomeworkStudent;
import com.woniuxy.bazhong.utils.PageMessage;

import java.util.List;

/**
 * @author zhangjy
 * @date 2022/7/23 15:48
 */
public interface HomeworkService {
    /**
     * 分页查询：
     *  根据学生id查询该学生的所有学生作业（过期和未过期，以及提交和未提交）
     * @param studentId 学生id
     * @return  所有作业的list
     */
    public PageMessage<List<HomeworkStudent>> all(int studentId, int page, int size);

    /**
     * 根据id查询作业
     * @param id    作业id
     * @return  Homework对象
     */
    public Homework findById(int id);

    /**
     * 根据学生id和作业id查询一个学生作业的所有信息（学生作业基本信息、作业信息、所有题目信息以及作业学生答案）
     * @param studentId
     * @param homeworkId
     * @return
     */
    public HomeworkStudent findByStudentIdAndHomeworkId(int studentId, int homeworkId);

    /**
     * 根据学生id、作业id和作业题目id提交（只能insert）学生的一道作业题答案
     * @param studentId
     * @param homeworkId
     * @param homeworkTopicId
     * @return
     */
    public boolean addHomeworkAnswer(int studentId, int homeworkId, int homeworkTopicId, String answer);

    /**
     * 根据学生id、作业id和作业题目id修改学生的一道作业题答案
     * @param studentId
     * @param homeworkId
     * @param homeworkTopicId
     * @return
     */
    public boolean updateHomeworkAnswer(int studentId, int homeworkId, int homeworkTopicId, String answer);

}
