package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Scores;
import com.woniuxy.bazhong.utils.PageMessage;

import java.util.List;
import com.woniuxy.bazhong.entity.Scores;

import java.util.List;
import java.util.Map;

public interface ScoresService {
    Map<String, Integer> studentRank(String classes, String exam,int studentId);

    PageMessage<List<Scores>> findAllScoreByclass(String classes, String exam, int schoolId, int page , int size);
    //根据学生id和考试名称查询各科成绩
    public List<Scores> findScoresByStId(int id);


}
