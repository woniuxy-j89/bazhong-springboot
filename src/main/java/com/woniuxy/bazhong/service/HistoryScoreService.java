package com.woniuxy.bazhong.service;


import com.woniuxy.bazhong.entity.HistoryScore;

import java.util.List;

public interface HistoryScoreService {
    List<HistoryScore> findAllScoreByName(String exam);

    List<HistoryScore> findAllScoreByClass(String classes, String subject,int schoolId);


}
