package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Leader;
import com.woniuxy.bazhong.entity.School;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.utils.PageMessage;

import java.util.List;

/**
* @author lzw
* @date  
*/
public interface LeaderService {
    public int createLeader(Leader leader);

    public int getMaxId();

    public Leader findLeaderById(int id);

    public void add(User user);

    public int findMaxUserId();

    public void addUserRole(int id);

    public PageMessage<List<Leader>> findLeaders(int page, int size);

    public Leader findByName(String leaderName);
}
