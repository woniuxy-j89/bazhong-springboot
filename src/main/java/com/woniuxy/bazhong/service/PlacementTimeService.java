package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.PlacementTime;

public interface PlacementTimeService {

    public boolean addTime(PlacementTime placementTime);

    PlacementTime findPlacementTime();
}
