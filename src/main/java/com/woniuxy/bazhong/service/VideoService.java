package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Video;
import com.woniuxy.bazhong.utils.PageMessage;

import java.util.List;

public interface VideoService {
    public  PageMessage<List<Video>> findToAudit(int page, int size,int schoolId) ;

    public boolean updateStatus(Video video);

    public PageMessage<List<Video>> findVideoByLike(int page,int size,String info);

    /**
     * 分页查询所有审核成功的视频
     * @param studentId
     * @param page
     * @param size
     * @return
     */
    public PageMessage<List<Video>> allByCondition(int studentId, int page, int size);

    /**
     * 根据视频id查询一个视频
     * @param id
     * @return
     */
    public Video findById(int id);
    /**
     * 查询所有视频
     * @return
     */
    public PageMessage<List<Video>> findAllVideo(int page,int size);
}
