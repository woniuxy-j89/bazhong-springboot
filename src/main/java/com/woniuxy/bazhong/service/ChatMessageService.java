package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.ChatMessage;

public interface ChatMessageService {

    public boolean addMessage(ChatMessage chatMessage);
}
