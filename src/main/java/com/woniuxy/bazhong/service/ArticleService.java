package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Article;
import com.woniuxy.bazhong.utils.PageMessage;

import java.util.List;

public interface ArticleService {
    public  PageMessage<List<Article>> findToAudit(int page, int size ,int schoolId ) ;

    public boolean updateStatus(Article article);
    /**
     * 文章的模糊查询
     * @param info
     * @return
     */
    public PageMessage<List<Article>> findArticleByLike(int page,int size,String info);

    /**
     * 查询所有文章
     * @return
     */
    public PageMessage<List<Article>> findAllArticle(int page,int size);
}
