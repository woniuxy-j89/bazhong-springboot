package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.KnowledgePoint;
import com.woniuxy.bazhong.utils.PageMessage;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface KnowledgePointService {

    public boolean addKnowledgePoint(String fileName,MultipartFile file) throws Exception;

    /**
     * 分页查询所有知识点——条件查询(科目类型、知识点重要程度)
     *
     * @param subjectId
     * @param status
     * @param page
     * @param size
     * @return
     */
    public PageMessage<List<KnowledgePoint>> all(int subjectId, String status, int page, int size);


    PageMessage<List<KnowledgePoint>> findKnow(int page, int size);

    /**
     * 根据知识点id查询一个知识点
     * @param id
     * @return
     */
    public KnowledgePoint findById(int id);
}
