package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.User;

public interface AuthService {
    public boolean login(User user);

    User findByAccount(String account);
}
