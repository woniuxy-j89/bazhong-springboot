package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Data;
import com.woniuxy.bazhong.entity.Grade;

import java.util.List;

public interface DataService {
    public List<Long> getTestPaper(String semester, String startDate, String endDate);


    public List<Data<Object>> getData(String grade, String year, List<Long> list);

    public List<Data<Object>> getNums(String grade, String year, List<Long> list);

    public List<Data<Object>> getRate(String grade, String year, List<Long> list);

    public List getSemester();

    public List<Grade> getGrades();
}
