package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Exam;

import java.util.List;

public interface ExamService {
    List<Exam> findAllExamBySchool(int schoolId);
}
