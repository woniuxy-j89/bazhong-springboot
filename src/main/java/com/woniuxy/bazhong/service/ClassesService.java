package com.woniuxy.bazhong.service;

import java.util.List;

public interface ClassesService {
    List<String> findAllClassBySchool(int schoolId);
}
