package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.Student;
import com.woniuxy.bazhong.mapper.StudentMapper;
import com.woniuxy.bazhong.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liwei
 * @data 2022/7/21{} 16:58
 */
@Service
@Slf4j
public class StudentServiceImpl implements StudentService {
    @Resource
    private StudentMapper studentMapper;
    /**
     * 根据id查找学生
     * @param id
     * @return Student
     */
    @Override
    public Student findById(int id) {
        return studentMapper.findById(id);
    }

    @Transactional
    @Override
    public boolean updateStudentChoice(String liberaScience,int id) {
        Boolean result = studentMapper.updateStudentChoice(liberaScience,id);
        return  result;
    }

    @Override
    public Student findByUserId(int userId) {
        Student student = studentMapper.findByUserId(userId);

        log.info("根据用户id查询的学生：" + student);
        return student;
    }
}
