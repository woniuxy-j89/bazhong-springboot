package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.Exam;
import com.woniuxy.bazhong.mapper.ExamMapper;
import com.woniuxy.bazhong.service.ExamService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ExamServiceImpl implements ExamService {

    @Resource
    private ExamMapper examMapper;

    @Override
    public List<Exam> findAllExamBySchool(int schoolId) {

        return examMapper.findAllExamBySchool( schoolId);
    }
}
