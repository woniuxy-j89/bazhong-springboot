package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.mapper.*;
import com.woniuxy.bazhong.service.HeadteacherService;
import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by raoyucheng
 * on 2022/7/22 14:26
 */
@Slf4j
@Service
public class HeadteacherServiceImpl implements HeadteacherService {
    @Resource
    private HeadteacherMapper headteacherMapper;

    @Resource
    private UserMapper userMapper;

    @Resource
    private HighestScoreMapper highestScoreMapper;

    @Resource
    private TeacherMapper teacherMapper;

    @Resource
    private ClassesMapper classesMapper;

    @Override
    @Transactional
    public boolean addStudents(String fileName, MultipartFile file) throws Exception {
        log.info("已经进入addStudents");
        boolean flag = false;
        List<Student> list = new ArrayList<>();
        List<User> userList = new ArrayList<>();

        //判断文件格式是否是excel
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            //throw new MyException("上传文件格式不正确");
            return false;
        }
        //判断文件格式是否是excel2003
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }
        //创建数据流
        InputStream is = file.getInputStream();
        //Workbook是一个类，用于创建Excel对象（也就是Workbook对象）
        Workbook wb = null;
        if (isExcel2003) {
            wb = new HSSFWorkbook(is);
        } else {
            wb = new XSSFWorkbook(is);
        }
        //sheet类_java之操作excel类
        Sheet sheet = wb.getSheetAt(0);
        if(sheet!=null){
            flag = true;
        }

        Student student;
        User user;

        User userTea = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int teacherId = teacherMapper.findTeacherIdById(userTea.getId());
        Classes classes = classesMapper.findAllClassByTeacher(teacherId);


        String liberaScience = null;
        if(classes.getType() == 1){
            liberaScience = "1";
        }else if(classes.getType() == 2){
            liberaScience = "0";
        }




//        int i = 1;
        //遍历excel表
        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null){
                continue;
            }

            student = new Student();
            user = new User();



            if(row.getCell(0).getCellType() != 1){
                //throw new MyException("导入失败(第"+(r+1)+"行,姓名请设为文本格式)");
                return false;
            }
            String name = row.getCell(0).getStringCellValue();

            if(name == null || name.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,姓名未填写)");
                return false;
            }
            //设置导出excel中 单元格中的数据类型
            row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String age = row.getCell(1).getStringCellValue();
            /*if(detail==null || detail.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,电话未填写)");
                return false;
            }*/
            row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
            String phone = row.getCell(2).getStringCellValue();

            row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
            String gender = row.getCell(3).getStringCellValue();

            row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
            String number = row.getCell(4).getStringCellValue();

            row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
            String email = row.getCell(5).getStringCellValue();

            row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
            int middleScore = Integer.parseInt(row.getCell(6).getStringCellValue());

//            int maxScoresId = headteacherMapper.findMaxScoresId();
//            int studentScoresId = maxScoresId + i;
//            i++;





            student.setAge(Integer.parseInt(age));
            student.setName(name);
            student.setClassId(classes.getId());
            student.setEmail(email);
            student.setGender(gender);
            student.setPhone(phone);
            student.setGardeId(classes.getGradeId());
            student.setNumber(number);
            student.setStatus("0");
            student.setMiddleScore(middleScore);
            student.setEnrollmentYear(Integer.parseInt(classes.getEnrollmentYear()));


            if(liberaScience != null){
                student.setLiberaScience(liberaScience);
            }





            user.setAccount(number);
            user.setPhone(phone);
            user.setPassword(number);
            user.setSchoolId(classes.getSchoolId());//调用老师个人信息里面的学校id
            user.setStatus("1");

            list.add(student);
            userList.add(user);
        }
        log.info("生成学生表和对应的user表");
        for (Student student1 : list) {
            log.info("往数据库插入或更新学生信息");
            String number = student1.getNumber();

            int count = headteacherMapper.findStudentByNumber(number);
            if(0 == count){
                flag = headteacherMapper.insertStudent(student1);
            }else{
                flag = headteacherMapper.updateStudent(student1);
            }

        }

        for (User user1 : userList) {
            log.info("往数据库插入或更新User信息");
            String account = user1.getAccount();
            log.info("已经获取用户的账号");
            int schoolId = user1.getSchoolId();
            log.info("已经获取用户的学校id");

            int count = userMapper.findUserByAccountAndSchoolId(account,schoolId);
            if(0 == count){
                flag = userMapper.insertUser(user1);
            }else{
                flag = userMapper.updateUser(user1);
            }

            int userId = userMapper.getUserIdByAccountAndSchoolId(account,schoolId);
            userMapper.setStudentRole(userId);

            Student student2 = new Student();
            student2.setNumber(user1.getAccount());
            student2.setUserId(userId);
            headteacherMapper.updateStudentUserId(student2);
        }

        return flag;
    }

    @Override
    public boolean resetStudentPassword(int studentNumber) {
        int userId = headteacherMapper.findUserIdByNumber(studentNumber);

        return userMapper.resetStudentPassword(userId);
    }

    @Override
    @Transactional
    public boolean addStudentTotalPoint(String fileName, MultipartFile file) throws Exception {
        log.info("已经进入");
        boolean flag = false;
        List<HighestScore> list = new ArrayList<>();
        log.info(fileName);
        //判断文件格式是否是excel
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            //throw new MyException("上传文件格式不正确");
            return false;
        }
        //判断文件格式是否是excel2003
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }

        //创建数据流
        InputStream is = file.getInputStream();
        //Workbook是一个类，用于创建Excel对象（也就是Workbook对象）
        Workbook wb = null;
        if (isExcel2003) {
            wb = new HSSFWorkbook(is);
        } else {
            wb = new XSSFWorkbook(is);
        }
        //sheet类_java之操作excel类
        Sheet sheet = wb.getSheetAt(0);
        if(sheet!=null){
            flag = true;
        }

        HighestScore highestScore = null;

        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null){
                continue;
            }

           highestScore = new HighestScore();

            row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
            String student_id = row.getCell(0).getStringCellValue();

            if(student_id == null || student_id.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,姓名未填写)");
                return false;
            }

            double chinese  = 0;
            if(row.getCell(1).getCellType() !=3){
                 chinese = row.getCell(1).getNumericCellValue();
            }
            double math  = 0;
            if(row.getCell(2).getCellType() !=3){
                 math = row.getCell(2).getNumericCellValue();
            }
            double english  = 0;
            if(row.getCell(3).getCellType() !=3){
                english = row.getCell(3).getNumericCellValue();
            }
            double physics  = 0;
            if(row.getCell(4).getCellType() !=3){
                physics = row.getCell(4).getNumericCellValue();
            }
            double chemistry  = 0;
            if(row.getCell(5).getCellType() !=3){
                chemistry = row.getCell(5).getNumericCellValue();
            }
            double biology  = 0;
            if(row.getCell(6).getCellType() !=3){
                biology = row.getCell(6).getNumericCellValue();
            }
            double politics  = 0;
            if(row.getCell(7).getCellType() !=3){
                politics = row.getCell(7).getNumericCellValue();
            }
            double history  = 0;
            if(row.getCell(8).getCellType() !=3){
                history = row.getCell(8).getNumericCellValue();
            }
            double geography  = 0;
            if(row.getCell(9).getCellType() !=3){
               geography = row.getCell(9).getNumericCellValue();
            }

            row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
            String description = row.getCell(10).getStringCellValue();


            double total = row.getCell(11).getNumericCellValue();

            highestScore.setStudentId(Integer.parseInt(student_id));
            highestScore.setChineseScore(chinese);
            highestScore.setMathScore(math);
            highestScore.setEnglishScore(english);
            highestScore.setPhysicsScore(physics);
            highestScore.setChemistryScore(chemistry);
            highestScore.setBiologyScore(biology);
            highestScore.setPoliticsScore(politics);
            highestScore.setHistoryScore(history);
            highestScore.setGeographyScore(geography);
            highestScore.setDescription(description);
            highestScore.setTotalPoints(total);

            list.add(highestScore);
        }
        for (HighestScore h : list) {



            int count = highestScoreMapper.findhas(h);
            if(0 == count){
                flag = highestScoreMapper.addHighestScore(h);
            }else{
                flag = highestScoreMapper.updateHighestScore(h);
            }

        }
        return flag;
    }

    @Override
    public PageMessage<List<Student>> allStudent(int page, int size) {
        PageHelper.startPage(page,size);

        User userTea = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int teacherId = teacherMapper.findTeacherIdById(userTea.getId());
        Classes classes = classesMapper.findAllClassByTeacher(teacherId);

        List<Student> students = headteacherMapper.allStudent(classes.getId());
        for(Student stu : students){
            stu.setClassName(classes.getName());
        }

        /**
         * 将学生列表进行分页处理
         */
        PageInfo<Student> pageInfo = PageInfo.of(students);
        log.info(pageInfo.toString());
        // 封装信息
        PageMessage<List<Student>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        log.info("pages:" + pageInfo.getPages());
        log.info("total:" + pageInfo.getTotal());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(students);


        return pageMessage;
    }

    @Override
    public String findStuEmailByNumber(String studentNumbers) {
        String email = headteacherMapper.findEmailByNumber(studentNumbers);

        return email;
    }

    @Override
    public boolean addHighTestScores(String fileName, MultipartFile file) throws Exception {
        log.info("已经进入addHighTestScores");
        boolean flag = false;
        List<Collegeentranceexamination> list = new ArrayList<>();

        //判断文件格式是否是excel
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            //throw new MyException("上传文件格式不正确");
            return false;
        }
        //判断文件格式是否是excel2003
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }
        //创建数据流
        InputStream is = file.getInputStream();
        //Workbook是一个类，用于创建Excel对象（也就是Workbook对象）
        Workbook wb = null;
        if (isExcel2003) {
            wb = new HSSFWorkbook(is);
        } else {
            wb = new XSSFWorkbook(is);
        }
        //sheet类_java之操作excel类
        Sheet sheet = wb.getSheetAt(0);
        if(sheet!=null){
            flag = true;
        }

        Collegeentranceexamination scores;


        User userTea = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int teacherId = teacherMapper.findTeacherIdById(userTea.getId());
        Classes classes = classesMapper.findAllClassByTeacher(teacherId);


        String liberaScience = null;
        if(classes.getType() == 1){
            liberaScience = "1";
        }else if(classes.getType() == 2){
            liberaScience = "0";
        }

        log.info("文理科");


//        int i = 1;
        //遍历excel表
        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            log.info("开始遍历Excel表");
            Row row = sheet.getRow(r);
            if (row == null){
                continue;
            }

            scores = new Collegeentranceexamination();

            row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
            String studentId = row.getCell(0).getStringCellValue();

            //设置导出excel中 单元格中的数据类型
            row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String chinese = row.getCell(1).getStringCellValue();
            /*if(detail==null || detail.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,电话未填写)");
                return false;
            }*/
            row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
            String mathematics = row.getCell(2).getStringCellValue();

            row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
            String english = row.getCell(3).getStringCellValue();

            row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
            String synthesize = row.getCell(4).getStringCellValue();

            row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
            String total = row.getCell(5).getStringCellValue();

            row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
            String enroll_type = row.getCell(6).getStringCellValue();

            row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);
            String enroll = row.getCell(7).getStringCellValue();

            row.getCell(8).setCellType(Cell.CELL_TYPE_STRING);
            String specialty = row.getCell(8).getStringCellValue();

            row.getCell(9).setCellType(Cell.CELL_TYPE_STRING);
            String enrollscore = row.getCell(9).getStringCellValue();

            row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
            String examination_year = row.getCell(10).getStringCellValue();


//            int maxScoresId = headteacherMapper.findMaxScoresId();
//            int studentScoresId = maxScoresId + i;
//            i++;

            scores.setStudentId(Integer.parseInt(studentId));
            scores.setType(liberaScience);
            scores.setChinese(Double.parseDouble(chinese));
            scores.setMathematics(Double.parseDouble(mathematics));
            scores.setEnglish(Double.parseDouble(english));
            scores.setTotal(Double.parseDouble(total));
            scores.setSynthesize(Double.parseDouble(synthesize));
            scores.setEnrollType(enroll_type);
            scores.setEnroll(enroll);
            scores.setSpecialty(specialty);
            scores.setEnrollscore(Double.parseDouble(enrollscore));
            scores.setSchoolId(userTea.getSchoolId());
            scores.setExaminationYear(Integer.parseInt(examination_year));


            list.add(scores);
            log.info("加入list");
        }

        for (Collegeentranceexamination scoresCollage : list) {
            log.info("往数据库插入高考信息");

            flag = headteacherMapper.insertScoresCollage(scoresCollage);

        }

        return flag;
    }
}
