package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.HomeworkStudent;
import com.woniuxy.bazhong.entity.KnowledgePoint;

import com.woniuxy.bazhong.mapper.KnowledgePointMapper;
import com.woniuxy.bazhong.mapper.KnowledgePointMapper;
import com.woniuxy.bazhong.service.KnowledgePointService;

import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class KnowledgePointServiceImpl implements KnowledgePointService {

    @Resource
    private KnowledgePointMapper knowledgePointMapper;

    @Override
    @Transactional
    public boolean addKnowledgePoint(String fileName,MultipartFile file) throws Exception {
        boolean flag = false;
        List<KnowledgePoint> list = new ArrayList<>();

        //判断文件格式是否是excel
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            //throw new MyException("上传文件格式不正确");
            return false;
        }
        //判断文件格式是否是excel2003
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }
        //创建数据流
        InputStream is = file.getInputStream();
        //Workbook是一个类，用于创建Excel对象（也就是Workbook对象）
        Workbook wb = null;
        if (isExcel2003) {
            wb = new HSSFWorkbook(is);
        } else {
            wb = new XSSFWorkbook(is);
        }
        //sheet类_java之操作excel类
        Sheet sheet = wb.getSheetAt(0);
        if(sheet!=null){
            flag = true;
        }
        KnowledgePoint knowlegedPoint;
        //遍历excel表
        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null){
                continue;
            }

            knowlegedPoint = new KnowledgePoint();

            if( row.getCell(0).getCellType() !=1){
                //throw new MyException("导入失败(第"+(r+1)+"行,姓名请设为文本格式)");
                return false;
            }
            String name = row.getCell(0).getStringCellValue();

            if(name == null || name.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,姓名未填写)");
                return false;
            }
            //设置导出excel中 单元格中的数据类型
            row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String detail = row.getCell(1).getStringCellValue();
            /*if(detail==null || detail.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,电话未填写)");
                return false;
            }*/
            row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
            String subId = row.getCell(2).getStringCellValue();

            row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
            String status = row.getCell(3).getStringCellValue();

            knowlegedPoint.setName(name);
            knowlegedPoint.setDetails(detail);
            knowlegedPoint.setStatus(status);
            knowlegedPoint.setSubjectId(Integer.parseInt(subId));

            list.add(knowlegedPoint);
        }
        for (KnowledgePoint k: list) {
            String name = k.getName();

            int count = knowledgePointMapper.findPointByName(name);
            if(0 == count){
                flag = knowledgePointMapper.addKnowledgePoint(k);
            }else{
                flag = knowledgePointMapper.updatePoint(k);
            }

        }

        return flag;
    }

    @Override
    public PageMessage<List<KnowledgePoint>> all(int subjectId, String status, int page, int size) {
        List<KnowledgePoint> list = new ArrayList<>();

        // 设置页码和每一页大小
        PageHelper.startPage(page, size);

        // 调用mapper
        // 条件查询所有知识点
        list = knowledgePointMapper.findAllKnowledgePoint(subjectId, status);

        log.info("条件查询所有知识点：" + list);

        // 获取分页信息
        PageInfo<KnowledgePoint> pageInfo = PageInfo.of(list);

        // 设置信息
        PageMessage<List<KnowledgePoint>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(list);

        log.info("分页查询信息：" + pageMessage);

        return pageMessage;
    }

    @Override
    public PageMessage<List<KnowledgePoint>> findKnow(int page, int size) {
        PageHelper.startPage(page, size);

        List<KnowledgePoint> knowledgePoints = knowledgePointMapper.findKnow();

        PageInfo<KnowledgePoint> pageInfo = PageInfo.of(knowledgePoints);
        // 设置信息
        PageMessage<List<KnowledgePoint>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(knowledgePoints);

        log.info("分页查询信息：" + pageMessage);

        return pageMessage;

    }

    @Override
    public KnowledgePoint findById(int id) {
        return knowledgePointMapper.findById(id);
    }
}
