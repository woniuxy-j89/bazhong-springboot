package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.HistoryScore;
import com.woniuxy.bazhong.mapper.HistoryScoreMapper;
import com.woniuxy.bazhong.service.HistoryScoreService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class HistoryScoreServiceImpl implements HistoryScoreService {

    @Resource
    private HistoryScoreMapper historyScoreMapper;
    @Override
    public List<HistoryScore> findAllScoreByName(String exam) {

        List<HistoryScore> list= historyScoreMapper.findAllScoreByName(exam);
        return list;
    }

    @Override
    public List<HistoryScore> findAllScoreByClass(String classes, String subject,int schoolId) {

        List<HistoryScore> list= historyScoreMapper.findAllScoreByClass(classes,subject,schoolId);

        return list;
    }


}
