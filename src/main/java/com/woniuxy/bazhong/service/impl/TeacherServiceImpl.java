package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.mapper.*;
import com.woniuxy.bazhong.service.TeacherService;
import com.woniuxy.bazhong.service.UserService;
import com.woniuxy.bazhong.utils.DateUtil;
import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TeacherServiceImpl implements TeacherService {

    @Resource
    public StudentMapper studentMapper;

    @Resource
    private SemesterMapper semesterMapper;

    @Resource
    private TeacherMapper teacherMapper;

    @Resource
    private QuestionsMapper questionsMapper;

    @Resource
    private KnowledgePointMapper knowledgePointMapper;

    @Resource
    private HomeworkMapper homeworkMapper;

    @Override
    public Teacher findById(int id) {
        return teacherMapper.findById(id);
    }

    /**
     * 根据id查询问题
     * @param id
     * @return
     */
    @Override
    public Questions findQuestionsByQid(int id) {
        return questionsMapper.findQuestionsById(id);
    }

    /**
     * 分页查询老师的问题
     * @param page
     * @param size
     * @param teacherId
     * @return
     */
    @Override
    public PageMessage<List<Questions>> findQuestionsByTid(int page, int size, int teacherId) {
        // Teacher teacher = teacherMapper.findById(teacherId);

        //设置分页
        PageHelper.startPage(page,size);
        List<Questions> all = questionsMapper.findQuestionsByTid(teacherId);
        for(Questions questions : all){
            questions.setStudent(studentMapper.findById(questions.getStudentId()));
        }
        log.info(all.toString());
        PageInfo<Questions> pageInfo = PageInfo.of(all);
        PageMessage<List<Questions>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setData(all);
        return pageMessage;
    }

    /**
     * 回答学生问题
     * @param questions
     * @return
     */
    @Override
    @Transactional
    public boolean answer(Questions questions) {
        questions.setAnswerTime(DateUtil.getDate());
        int result = questionsMapper.answer(questions);
        return result > 0 ? true : false;
    }

    /**
     * 布置作业到所授班级所有学生的作业表中
     * @param knowledgepointId
     * @param questionsTypeName
     * @param num
     * @return
     */
    @Override
    @Transactional
    public boolean uploadHomework(Homework homework,int knowledgepointId, String questionsTypeName, int num) {
        int knowledgeId = knowledgepointId;
        int subjectId = homework.getSubjectId();
        int totalPoints = 0;

        String type = homework.getType();
        String name = homework.getName();
        int classId = homework.getClassId();


        HomeworkTopic homeworkTopic = new HomeworkTopic();
        HomeworkStudent homeworkStudent = new HomeworkStudent();
        int result = 0;
        if(questionsTypeName.equals("choice")){
            List<Choice> choiceList = questionsMapper.findQuestionsByTypeandNum1(knowledgeId, num);
            for (Choice choice: choiceList) {
                totalPoints += choice.getScoreValue();

                homework.setPublicTime(DateUtil.getDate());
                homework.setName(name);
                homework.setType(type);
                homework.setTeacherId(1); // 到时候从登录数据中取出

                result = homeworkMapper.autoHomework(homework);
                if(result == 0){
                    return false;
                }
                int homeworkId = homework.getId();

                homeworkTopic.setHomeworkId(homeworkId);
                homeworkTopic.setTopicId(choice.getId());

                int questionTypeId = questionsMapper.findQuestionTypeIdBySidandType(questionsTypeName, subjectId);
                homeworkTopic.setQuestiontypesId(questionTypeId);

                result =  homeworkMapper.addHomeworkTopic(homeworkTopic);

                if(result == 0){
                    return false;
                }

                List<Student> studentList = studentMapper.findStudentsByClassId(classId);
                for (Student stu : studentList) {
                    homeworkStudent.setStatus("0");
                    homeworkStudent.setStudentId(stu.getId());
                    homeworkStudent.setHomeworkId(homeworkId);
                    // 先不设置
                    homeworkStudent.setScore(0);
                    result = homeworkMapper.addHomeworkStudent(homeworkStudent);
                    if(result == 0){
                        return false;
                    }
                }
                return true;
            }
        } else if (questionsTypeName.equals("blank")) {
            List<Blank> blankList = questionsMapper.findQuestionsByTypeandNum2(knowledgepointId, num);
            for (Blank blank: blankList) {
                totalPoints += blank.getScoreValue();
                homework.setSubjectId(subjectId);
                homework.setPublicTime(DateUtil.getDate());
                homework.setName(name);
                homework.setType(type);

                homework.setTeacherId(1); // 到时候从登录数据中取出
                homework.setClassId(1); // 到时候从登录数据中取出
                homework.setSubjectId(1); // 到时候从登录数据中取出

                result = homeworkMapper.autoHomework(homework);
                if (result == 0) {
                    return false;
                }
                int homeworkId = homework.getId();

                homeworkTopic.setHomeworkId(homeworkId);
                homeworkTopic.setTopicId(blank.getId());

                int questionTypeId = questionsMapper.findQuestionTypeIdBySidandType(questionsTypeName, subjectId);
                homeworkTopic.setQuestiontypesId(questionTypeId);

                result = homeworkMapper.addHomeworkTopic(homeworkTopic);

                if (result == 0) {
                    return false;
                }

                List<Student> studentList = studentMapper.findStudentsByClassId(classId);
                for (Student stu : studentList) {
                    homeworkStudent.setStatus("0");
                    homeworkStudent.setStudentId(stu.getId());
                    homeworkStudent.setHomeworkId(homeworkId);
                    // 先不设置
                    homeworkStudent.setScore(0);
                    result = homeworkMapper.addHomeworkStudent(homeworkStudent);
                    if (result == 0) {
                        return false;
                    }
                }
                return true;
            }
        } else if (questionsTypeName.equals("reading")) {
            List<Reading> readingList = questionsMapper.findQuestionsByTypeandNum3(knowledgepointId, num);
                for (Reading reading: readingList) {
                    totalPoints += reading.getScoreValue();
                    homework.setSubjectId(subjectId);
                    homework.setPublicTime(DateUtil.getDate());
                    homework.setName(name);
                    homework.setType(type);

                    homework.setTeacherId(1); // 到时候从登录数据中取出
                    homework.setClassId(1); // 到时候从登录数据中取出
                    homework.setSubjectId(1); // 到时候从登录数据中取出

                    result = homeworkMapper.autoHomework(homework);
                    if (result == 0) {
                        return false;
                    }
                    int homeworkId = homework.getId();

                    homeworkTopic.setHomeworkId(homeworkId);
                    homeworkTopic.setTopicId(reading.getId());

                    int questionTypeId = questionsMapper.findQuestionTypeIdBySidandType(questionsTypeName, subjectId);
                    homeworkTopic.setQuestiontypesId(questionTypeId);

                    result = homeworkMapper.addHomeworkTopic(homeworkTopic);

                    if (result == 0) {
                        return false;
                    }

                    List<Student> studentList = studentMapper.findStudentsByClassId(classId);
                    for (Student stu : studentList) {
                        homeworkStudent.setStatus("0");
                        homeworkStudent.setStudentId(stu.getId());
                        homeworkStudent.setHomeworkId(homeworkId);
                        // 先不设置
                        homeworkStudent.setScore(0);
                        result = homeworkMapper.addHomeworkStudent(homeworkStudent);
                        if (result == 0) {
                            return false;
                        }
                    }
                    return true;
                }
        } else if (questionsTypeName.equals("title")) {
            List<Title> titleList = questionsMapper.findQuestionsByTypeandNum4(knowledgeId, num);
                for (Title title: titleList) {
                    totalPoints += title.getScoreValue();
                    homework.setSubjectId(subjectId);
                    homework.setPublicTime(DateUtil.getDate());
                    homework.setName(name);
                    homework.setType(type);

                    homework.setTeacherId(1); // 到时候从登录数据中取出
                    homework.setClassId(1); // 到时候从登录数据中取出
                    homework.setSubjectId(1); // 到时候从登录数据中取出

                    result = homeworkMapper.autoHomework(homework);
                    if (result == 0) {
                        return false;
                    }
                    int homeworkId = homework.getId();

                    homeworkTopic.setHomeworkId(homeworkId);
                    homeworkTopic.setTopicId(title.getId());

                    int questionTypeId = questionsMapper.findQuestionTypeIdBySidandType(questionsTypeName, subjectId);
                    homeworkTopic.setQuestiontypesId(questionTypeId);

                    result = homeworkMapper.addHomeworkTopic(homeworkTopic);

                    if (result == 0) {
                        return false;
                    }

                    List<Student> studentList = studentMapper.findStudentsByClassId(classId);
                    for (Student stu : studentList) {
                        homeworkStudent.setStatus("0");
                        homeworkStudent.setStudentId(stu.getId());
                        homeworkStudent.setHomeworkId(homeworkId);
                            // 先不设置
                        homeworkStudent.setScore(0);
                        result = homeworkMapper.addHomeworkStudent(homeworkStudent);
                        if (result == 0) {
                            return false;
                        }
                    }
                    return true;
                }
        }
       return true;
    }

    @Override
    public boolean setFinalTime(int homeworkId,String finalTime) {
        int result = homeworkMapper.setFinalTime(homeworkId,finalTime);
        return result > 0 ? true : false;
    }

    /**
     * 试卷上传
     * @param exam
     * @return 生成的试卷id
     */
    @Override
    @Transactional
    public int uploadExam(Exam exam) {

        int result = teacherMapper.uploadExam(exam);
        if(result < 1){
            return 0;
        }
        int examId = exam.getId();
        return examId;
    }

    @Override
    public PageMessage<List<Video>> findVideoByTid(int page,int size,int teacherId) {
        PageHelper.startPage(page,size);
        List<Video> list = teacherMapper.findVideoByTid(teacherId);
        PageInfo<Video> pageInfo = PageInfo.of(list);

        // 封装信息
        PageMessage<List<Video>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(list);
        return pageMessage;
    }

    @Override
    public PageMessage<Teacher> teacherOfClassess(int teacherId, int page, int size) {

        //查询当前学期

        String nowSemester = semesterMapper.findNowSemeSter();
        log.info(nowSemester);

        PageHelper.startPage(page,size);


        Teacher teacher = teacherMapper.teacherOfClassess(teacherId,nowSemester);

        //  将教师信息中的当前学期所带班级信息进行分页处理

        PageInfo<Classes> pageInfo = PageInfo.of(teacher.getClasses());

        // 封装信息
        PageMessage<Teacher> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(teacher);


        return pageMessage;
    }
}
