package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.ChatMessage;
import com.woniuxy.bazhong.mapper.ChatMessageMapper;
import com.woniuxy.bazhong.service.ChatMessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service
public class ChatMessageServiceImpl implements ChatMessageService {

    @Resource
    private ChatMessageMapper chatMessageMapper;


    @Transactional
    @Override
    public boolean addMessage(ChatMessage chatMessage) {
        boolean result = chatMessageMapper.addMessage(chatMessage);

        return result;
    }
}
