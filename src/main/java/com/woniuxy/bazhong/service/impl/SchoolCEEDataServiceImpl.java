package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.EnrolmentRate;
import com.woniuxy.bazhong.entity.School;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.mapper.SchoolCEEDataMapper;
import com.woniuxy.bazhong.service.SchoolCEEDataService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SchoolCEEDataServiceImpl implements SchoolCEEDataService {
    /**
     * 各校同级数据对比模块mapper
     */
    @Resource
    private SchoolCEEDataMapper schoolCEEDataMapper;

    /**
     * 各校同级数据对比模块，展示学校列表
     * @return List<School>学校列表
     */
    @Override
    public List<School> allSchool() {
        return schoolCEEDataMapper.allSchool();
    }

    /**
     * 各校同级数据对比模块，展示升学率
     * @param schoolId 学校id
     * @return List<EnrolmentRate>
     */
    @Override
    public List<EnrolmentRate> showEnrolmentRate(int schoolId) {
        //方法一：判断是否所传学校id是否用户所在学校id
        /*if(schoolId == 6){
            return schoolCEEDataMapper.showEnrolmentRate(schoolId);
        } else{
            //如果不是查看本校升学率，则查询本校和传入id学校升学率
            //查询本校升学率
            List<EnrolmentRate> enrolmentRatesLocal = schoolCEEDataMapper.showEnrolmentRate(6);
            //查询传入id学校升学率
            List<EnrolmentRate> enrolmentRatesOther = schoolCEEDataMapper.showEnrolmentRate(schoolId);
            enrolmentRatesOther.forEach(enrolmentRate -> {
                enrolmentRatesLocal.add(enrolmentRate);
            });
            return enrolmentRatesLocal;
        }*/

        //方法二：将本校id和传入id一起查询,6为暂定的本校id

        //获取当前学校id
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int theschoolId = user.getSchoolId();
        return schoolCEEDataMapper.showEnrolmentRate(theschoolId,schoolId);
    }
}
