package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.mapper.FileMapper;
import com.woniuxy.bazhong.mapper.KnowledgePointMapper;
import com.woniuxy.bazhong.mapper.QuestionsMapper;
import com.woniuxy.bazhong.mapper.TeacherMapper;
import com.woniuxy.bazhong.service.FileService;
import com.woniuxy.bazhong.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class FileServiceImpl implements FileService {

    @Resource
    private FileMapper fileMapper;

    @Resource
    private TeacherMapper teacherMapper;

    @Resource
    private QuestionsMapper questionsMapper;

    @Resource
    private KnowledgePointMapper knowledgePointMapper;

    /**
     * 教师上传视频
     * @param filePath
     * @param fileName
     * @return
     */
    @Override
    @Transactional
    public boolean uploadVideo(String filePath, String fileName) {
        Video video = new Video();
        video.setId(0);
        video.setName(fileName);
        video.setDate(DateUtil.getDate());
        video.setStatus("0");
        video.setFile(filePath);

        //中间的查询逻辑后面写
        video.setSchoolId(1); // 先写死，后面登录弄好再改
        video.setTeacherId(1); // 先写死，后面登录弄好再改
        video.setKnowledgepointId(1); //  先写死，后面登录弄好再改
        video.setAuditStatus("0");

        log.info(video.toString());

        boolean flag = fileMapper.uploadVideo(video);

        return flag;
    }

    /**
     * 教师上传填空题
     * @param fileName
     * @param file
     * @return
     */
    @Override
    @Transactional
    public boolean uploadBlank(String fileName, MultipartFile file, int examId) throws IOException {
        boolean flag = false;
        List<Blank> list = new ArrayList<>();

        //判断文件格式是否是excel
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            //throw new MyException("上传文件格式不正确");
            return false;
        }

        //判断文件格式是否是excel2003
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }

        //创建数据流
        InputStream is = file.getInputStream();
        //Workbook是一个类，用于创建Excel对象（也就是Workbook对象）
        Workbook wb = null;
        if (isExcel2003) {
            wb = new HSSFWorkbook(is);
        } else {
            wb = new XSSFWorkbook(is);
        }
        //sheet类_java之操作excel类
        Sheet sheet = wb.getSheetAt(0);
        if(sheet!=null){
            flag = true;
        }

        Blank blank;
        //遍历excel表
        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null){
                continue;
            }
            blank = new Blank();

            //设置导出excel中 单元格中的数据类型
            row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String content = row.getCell(1).getStringCellValue();
            /*if(detail==null || detail.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,电话未填写)");
                return false;
            }*/
            row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
            String answer = row.getCell(2).getStringCellValue();

            double scoreValue = row.getCell(3).getNumericCellValue();

            row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
            String knowledgeName = row.getCell(5).getStringCellValue();
            int knowledgeId = knowledgePointMapper.findPointIdByName(knowledgeName);

            row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
            String answerAnalysis = row.getCell(4).getStringCellValue();

            blank.setId(examId+1); // 此处的题目id应该与哪个年级的月考做一个字符串拼接
            blank.setContent(content);
            blank.setAnswer(answer);
            blank.setAnswerAnalysis(answerAnalysis);
            blank.setScoreValue(scoreValue);
            blank.setKnowledgepointId(knowledgeId);

            list.add(blank);
            log.info(list.toString());
        }
        for (Blank b: list) {
            int id = b.getId();
            ExamTopic examTopic = new ExamTopic();
            Blank blank1= fileMapper.findBlankById(id);
            if(blank1 == null){
                flag = fileMapper.addBlank(b);

                // 根据examId查出科目ID
                int subjectId = teacherMapper.findSidByexamId(examId);

                // 根据科目与题型查出questiontypesId
                int questiontypesId = questionsMapper.findQuestionTypeIdBySidandType("blank", subjectId);
                examTopic.setExamId(examId);
                examTopic.setTopicId(b.getId());
                examTopic.setQuestiontypesId(questiontypesId);

                // 将题目与试卷绑定
                int result = teacherMapper.addExamTopic(examTopic);
                if(result < 1){
                    flag = false;
                }

            }else {
                flag = false;
            }
        }
        return flag;
    }

    /***
     * 上传阅读题
     * @param fileName
     * @param file
     * @return
     */
    @Override
    @Transactional
    public boolean uploadReading(String fileName, MultipartFile file, int examId) throws IOException {
        boolean flag = false;
        List<Reading> list = new ArrayList<>();

        //判断文件格式是否是excel
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            //throw new MyException("上传文件格式不正确");
            return false;
        }

        //判断文件格式是否是excel2003
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }

        //创建数据流
        InputStream is = file.getInputStream();
        //Workbook是一个类，用于创建Excel对象（也就是Workbook对象）
        Workbook wb = null;
        if (isExcel2003) {
            wb = new HSSFWorkbook(is);
        } else {
            wb = new XSSFWorkbook(is);
        }
        //sheet类_java之操作excel类
        Sheet sheet = wb.getSheetAt(0);
        if(sheet!=null){
            flag = true;
        }

        Reading reading;
        //遍历excel表
        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null){
                continue;
            }
            reading = new Reading();

            row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
            int id = Integer.parseInt("100" + row.getCell(0).getStringCellValue());

            //设置导出excel中 单元格中的数据类型
            row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String content = row.getCell(1).getStringCellValue();
            /*if(detail==null || detail.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,电话未填写)");
                return false;
            }*/

            row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
            String question = row.getCell(2).getStringCellValue();

            row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
            String answer = row.getCell(3).getStringCellValue();

            double scoreValue = row.getCell(4).getNumericCellValue();

            row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
            String knowledgeName = row.getCell(6).getStringCellValue();
            int knowledgeId = knowledgePointMapper.findPointIdByName(knowledgeName);

            row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
            String answerAnalysis = row.getCell(5).getStringCellValue();

            reading.setId(examId+1); // 此处的题目id应该与哪个年级的月考做一个字符串拼接
            reading.setContent(content);
            reading.setQuestion(question);
            reading.setAnswer(answer);
            reading.setAnswerAnalysis(answerAnalysis);
            reading.setScoreValue(scoreValue);
            reading.setKnowledgepointId(knowledgeId);

            list.add(reading);
            log.info(list.toString());
        }
        for (Reading r: list) {
            int id = r.getId();
            ExamTopic examTopic = new ExamTopic();
            Reading reading1= fileMapper.findReadingById(id);
            if(reading1 == null){
                flag = fileMapper.addReading(r);

                // 根据examId查出科目ID
                int subjectId = teacherMapper.findSidByexamId(examId);

                // 根据科目与题型查出questiontypesId
                int questiontypesId = questionsMapper.findQuestionTypeIdBySidandType("reading", subjectId);
                examTopic.setExamId(examId);
                examTopic.setTopicId(r.getId());
                examTopic.setQuestiontypesId(questiontypesId);

                // 将题目与试卷绑定
                int result = teacherMapper.addExamTopic(examTopic);
                if(result < 1){
                    flag = false;
                }
            }else {
                flag = false;
            }
        }
        return flag;
    }

    /**
     * 上传解答题
     * @param fileName
     * @param file
     * @return
     */
    @Override
    @Transactional
    public boolean uploadResponsequestion(String fileName, MultipartFile file, int examId) {

        return false;
    }

    /**
     * 上传选择题
     * @param fileName
     * @param file
     * @return
     */
    @Override
    @Transactional
    public boolean uploadChoice(String fileName, MultipartFile file, int examId) throws IOException {
        boolean flag = false;
        List<Choice> list = new ArrayList<>();

        //判断文件格式是否是excel
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            //throw new MyException("上传文件格式不正确");
            return false;
        }

        //判断文件格式是否是excel2003
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }
        System.out.println(flag + "2");
        //创建数据流
        InputStream is = file.getInputStream();
        //Workbook是一个类，用于创建Excel对象（也就是Workbook对象）
        Workbook wb = null;
        if (isExcel2003) {
            wb = new HSSFWorkbook(is);
        } else {
            wb = new XSSFWorkbook(is);
        }
        //sheet类_java之操作excel类
        Sheet sheet = wb.getSheetAt(0);
        if(sheet!=null){
            flag = true;
        }

        Choice choice;
        //遍历excel表
        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null){
                continue;
            }
            choice = new Choice();

            //设置导出excel中 单元格中的数据类型
            row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String content = row.getCell(1).getStringCellValue();
            /*if(detail==null || detail.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,电话未填写)");
                return false;
            }*/
            row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
            String choiceA = row.getCell(2).getStringCellValue();

            row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
            String choiceB = row.getCell(3).getStringCellValue();

            row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
            String choiceC = row.getCell(4).getStringCellValue();

            row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
            String choiceD = row.getCell(5).getStringCellValue();

            double scoreValue = row.getCell(6).getNumericCellValue();

            row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);
            String answer = row.getCell(7).getStringCellValue();

            row.getCell(8).setCellType(Cell.CELL_TYPE_STRING);
            String knowledgeName = row.getCell(8).getStringCellValue();
            int knowledgeId = knowledgePointMapper.findPointIdByName(knowledgeName);

            row.getCell(9).setCellType(Cell.CELL_TYPE_STRING);
            String answerAnalysis = row.getCell(9).getStringCellValue();

            row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
            int type = Integer.parseInt(row.getCell(10).getStringCellValue());

            choice.setId(examId+1); // 此处的题目id应该与哪个年级的月考做一个字符串拼接
            choice.setContent(content);
            choice.setChoiceA(choiceA);
            choice.setChoiceB(choiceB);
            choice.setChoiceC(choiceC);
            choice.setChoiceD(choiceD);
            choice.setAnswer(answer);
            choice.setAnswerAnalysis(answerAnalysis);
            choice.setScoreValue(scoreValue);
            choice.setType(type);
            choice.setKnowledgepointId(1);

            list.add(choice);

        }

        for (Choice c: list) {
            ExamTopic examTopic = new ExamTopic();
            int id = c.getId();

            Choice choice1 = fileMapper.findChoiceById(id);
            if(choice1 == null){
                flag = fileMapper.addChoice(c);

                // 根据examId查出科目ID
                int subjectId = teacherMapper.findSidByexamId(examId);

                // 根据科目与题型查出questiontypesId
                int questiontypesId = questionsMapper.findQuestionTypeIdBySidandType("choice", subjectId);
                examTopic.setExamId(examId);
                examTopic.setTopicId(c.getId());
                examTopic.setQuestiontypesId(questiontypesId);

                // 将题目与试卷绑定
                int result = teacherMapper.addExamTopic(examTopic);
                if(result < 1){
                    flag = false;
                }
            }else {
                flag = false;
            }
        }

        return flag;
    }

    /***
     * 上传作文题
     * @param fileName
     * @param file
     * @return
     */
    @Override
    @Transactional
    public boolean uploadTitle(String fileName, MultipartFile file, int examId) throws IOException {
        boolean flag = false;
        List<Title> list = new ArrayList<>();

        //判断文件格式是否是excel
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            //throw new MyException("上传文件格式不正确");
            return false;
        }

        //判断文件格式是否是excel2003
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }

        //创建数据流
        InputStream is = file.getInputStream();
        //Workbook是一个类，用于创建Excel对象（也就是Workbook对象）
        Workbook wb = null;
        if (isExcel2003) {
            wb = new HSSFWorkbook(is);
        } else {
            wb = new XSSFWorkbook(is);
        }
        //sheet类_java之操作excel类
        Sheet sheet = wb.getSheetAt(0);
        if(sheet!=null){
            flag = true;
        }

        Title title;
        //遍历excel表
        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null){
                continue;
            }
            title = new Title();

            row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
            int id = Integer.parseInt("100" + row.getCell(0).getStringCellValue());

            //设置导出excel中 单元格中的数据类型
            row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String content = row.getCell(1).getStringCellValue();
            /*if(detail==null || detail.isEmpty()){
                //throw new MyException("导入失败(第"+(r+1)+"行,电话未填写)");
                return false;
            }*/

            double scoreValue = row.getCell(2).getNumericCellValue();

            row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
            String knowledgeName = row.getCell(4).getStringCellValue();
            int knowledgeId = knowledgePointMapper.findPointIdByName(knowledgeName);

            row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
            String answerAnalysis = row.getCell(3).getStringCellValue();

            title.setId(examId+1); // 此处的题目id应该与哪个年级的月考做一个字符串拼接
            title.setContent(content);
            title.setAnswerAnalysis(answerAnalysis);
            title.setScoreValue(scoreValue);
            title.setKnowledgepointId(knowledgeId);

            list.add(title);
            log.info(list.toString());
        }
        for (Title t: list) {
            int id = t.getId();
            ExamTopic examTopic = new ExamTopic();
            Title title1= fileMapper.findTitleById(id);
            if(title1 == null){
                flag = fileMapper.addTitle(t);

                // 根据examId查出科目ID
                int subjectId = teacherMapper.findSidByexamId(examId);

                // 根据科目与题型查出questiontypesId
                int questiontypesId = questionsMapper.findQuestionTypeIdBySidandType("title", subjectId);
                examTopic.setExamId(examId);
                examTopic.setTopicId(t.getId());
                examTopic.setQuestiontypesId(questiontypesId);

                // 将题目与试卷绑定
                int result = teacherMapper.addExamTopic(examTopic);
                if(result < 1){
                    flag = false;
                }
            }else {
                flag = false;
            }
        }
        return flag;
    }

    @Override
    public String findModel(int subjectId, String type) {
        String modelPath = fileMapper.findModel(subjectId, type);
        return modelPath;
    }

    @Override
    public List<QuestionTypes> findTypeBySid(int subjectId) {
        List<QuestionTypes> list = fileMapper.findTypeBySid(subjectId);
        return list;
    }
}
