package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.mapper.ClassesMapper;
import com.woniuxy.bazhong.service.ClassesService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClassesServiceImpl implements ClassesService {

    @Resource
    private ClassesMapper classesMapper;

    @Override
    public List<String> findAllClassBySchool(int schoolId) {
        List<Classes> classes = classesMapper.findAllClassBySchool(schoolId);
        List<String> list = new ArrayList<>();
        for (Classes c: classes ) {
            String s = c.getEnrollmentYear()+"级"+c.getName();
            list.add(s);
        }

        return list;
    }
}
