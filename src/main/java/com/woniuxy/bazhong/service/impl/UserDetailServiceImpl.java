package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.mapper.UserMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
/**
 * 用户信息service实现类，需要实现接口UserDetailsService
 *
 * springsecurity在进行认证（登录）时会自动调用该实现类中的loadUserByUsername方法，不需要程序员写代码调用
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {


    @Resource
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userMapper.findUserByAccount(username);

        //判断用户是否存在
        if(user == null)throw new UsernameNotFoundException("账号不存在");


        return user;
    }
}
