package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.mapper.UserMapper;
import com.woniuxy.bazhong.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class AuthServiceImpl implements AuthService {

    @Resource
    private AuthenticationManager authenticationManager;

    @Resource
    private UserMapper userMapper;

    @Override
    public boolean login(User user) {
        // 将用户的账号密码封装成 token对象
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword());

        // 调用认证管理器的authenticate方法进行认证（调用springsecurity内置的流程）
        Authentication authentication = authenticationManager.authenticate(authenticationToken);

        // 判断是否登录成功
        if (authentication != null){
            log.info("登录成功");

            return true;
        }

        return false;

    }

    @Override
    public User findByAccount(String account) {
        return userMapper.findUserByAccount(account);
    }
}
