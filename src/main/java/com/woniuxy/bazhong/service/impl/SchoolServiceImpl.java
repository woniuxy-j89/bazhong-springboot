package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.School;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.mapper.SchoolMapper;
import com.woniuxy.bazhong.service.SchoolService;
import com.woniuxy.bazhong.utils.PageMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author lzw
*/
@Service
public class SchoolServiceImpl implements SchoolService {
    @Resource
    private SchoolMapper schoolMapper;

    @Override
    public Integer findMaxId() {
        return schoolMapper.findMaxId();
    }

    @Override
    public String findNumber(Integer maxId) {
        return schoolMapper.findNumber(maxId);
    }

    @Override
    public int createSchool(School school) {
        return schoolMapper.createSchool(school);
    }

    @Override
    public School findSchoolById(int maxId) {
        return schoolMapper.findSchoolById(maxId);
    }

    @Override
    public void add(User user) {
        schoolMapper.add(user);
    }

    @Override
    public int findMaxUserId() {
        return schoolMapper.findMaxUserId();
    }

    @Override
    public void addUserRole(int id) {
        schoolMapper.addUserRole(id);
    }

    @Override
    public PageMessage<List<School>> findSchools(int page, int size) {
        PageHelper.startPage(page,size);
        List<School> list = schoolMapper.findSchools();
        PageInfo<School> info = PageInfo.of(list);

        PageMessage<List<School>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(info.getPages());
        pageMessage.setTotal(info.getTotal());
        pageMessage.setData(list);
        return pageMessage;
    }

    @Override
    public School findByNumber(String schoolNumber) {
        return schoolMapper.findByNumber(schoolNumber);
    }
}
