package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.Article;
import com.woniuxy.bazhong.mapper.ArticleMapper;
import com.woniuxy.bazhong.service.ArticleService;
import com.woniuxy.bazhong.utils.PageMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Resource
    private ArticleMapper articleMapper;
    @Override
    public PageMessage<List<Article>> findToAudit(int page, int size , int schoolId) {
        PageHelper.startPage(page,size);

        List<Article> list = articleMapper.findToAudit(schoolId);

        PageInfo<Article> pageInfo = PageInfo.of(list);

        // 封装信息
        PageMessage<List<Article>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(list);


        return pageMessage;
    }

    @Transactional
    @Override
    public boolean updateStatus(Article article) {


        boolean result= articleMapper.updateStatus(article);

        return result;




    }

    @Override
    public PageMessage<List<Article>> findArticleByLike(int page,int size,String info) {
        PageHelper.startPage(page,size);
        info = "%"+info+"%";

        List<Article> list = articleMapper.findArticleByLike(info);

        PageInfo<Article> pageInfo = PageInfo.of(list);

        // 封装信息
        PageMessage<List<Article>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(list);
        return pageMessage;
    }

    @Override
    public PageMessage<List<Article>> findAllArticle(int page, int size) {
        PageHelper.startPage(page,size);
        List<Article> list = articleMapper.findAllArticle();

        PageInfo<Article> pageInfo = PageInfo.of(list);

        // 封装信息
        PageMessage<List<Article>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(list);
        return pageMessage;
    }

}
