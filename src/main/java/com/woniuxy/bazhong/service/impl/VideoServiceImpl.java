package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.entity.Student;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.entity.Video;
import com.woniuxy.bazhong.mapper.VideoMapper;
import com.woniuxy.bazhong.service.StudentService;
import com.woniuxy.bazhong.service.UserService;
import com.woniuxy.bazhong.service.VideoService;
import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class VideoServiceImpl implements VideoService {

    @Resource
    private VideoMapper videoMapper;
    @Resource
    private StudentService studentService;
    @Resource
    private UserService userService;

    @Override
    public PageMessage<List<Video>> findToAudit(int page, int size ,int schoolId) {

        PageHelper.startPage(page,size);

        List<Video> list =videoMapper.findToAudit(schoolId);

        PageInfo<Video> pageInfo = PageInfo.of(list);

        // 封装信息
        PageMessage<List<Video>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(list);

        return pageMessage;
    }


    //审核视频修改文档
    @Transactional
    @Override
    public boolean updateStatus(Video video) {


        boolean result= videoMapper.updateStatus(video);

        return result;
    }

    @Override
    public PageMessage<List<Video>> findVideoByLike(int page,int size,String info) {
        PageHelper.startPage(page,size);
        info = "%"+info+"%";
        List<Video> list =videoMapper.findVideoByLike(info);

        PageInfo<Video> pageInfo = PageInfo.of(list);

        // 封装信息
        PageMessage<List<Video>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(list);

        return pageMessage;
    }

    @Override
    public PageMessage<List<Video>> findAllVideo(int page, int size) {
        PageHelper.startPage(page,size);
        List<Video> list = videoMapper.findAllVideo();

        PageInfo<Video> pageInfo = PageInfo.of(list);

        // 封装信息
        PageMessage<List<Video>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(list);
        return pageMessage;
    }

    @Override
    public PageMessage<List<Video>> allByCondition(int studentId, int page, int size) {
        // 根据学生id得到该学生的用户id
        Student student = studentService.findById(studentId);
        log.info("该学生的学生信息：" + student);

        // 根据用户id得到该学生所在学校id
        User user = userService.findById(student.getUserId());
        log.info("该学生的用户信息为：" + user);

        Video video = new Video();

        // 判断条件——视频公开状态
        /*if (status.equals("0")) {   // 本班

        } else if (status.equals("1")) {    // 本校

        } else {    // status=2，显示所有

        }*/

        video.setAuditStatus("1");  // 1-审核成功

        // 设置页码和每一页大小
        PageHelper.startPage(page, size);

        // 条件查询所有知识点
//        list = videoMapper.findAllByCondition(status, knowledgepointId, schoolId, teacherId, auditStatus);
        List<Video> list = videoMapper.findAllByCondition(video);

        log.info("条件查询所有视频：" + list);

        // 获取分页信息
        PageInfo<Video> pageInfo = PageInfo.of(list);

        // 设置信息
        PageMessage<List<Video>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(list);

        log.info("条件分页查询信息：" + pageMessage);

        return pageMessage;
    }

    @Override
    public Video findById(int id) {
        return videoMapper.findById(id);
    }
}
