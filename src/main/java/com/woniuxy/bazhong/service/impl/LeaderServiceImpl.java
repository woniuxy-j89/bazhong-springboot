package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.Leader;
import com.woniuxy.bazhong.entity.School;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.mapper.LeaderMapper;
import com.woniuxy.bazhong.service.LeaderService;
import com.woniuxy.bazhong.utils.PageMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LeaderServiceImpl implements LeaderService {
    @Resource
    private LeaderMapper leaderMapper;

    @Override
    public int createLeader(Leader leader) {
        return leaderMapper.createLeader(leader);
    }

    @Override
    public int getMaxId() {
        return leaderMapper.getMaxId();
    }

    @Override
    public Leader findLeaderById(int id) {
        return leaderMapper.findLeaderById(id);
    }

    @Override
    public void add(User user) {
        leaderMapper.add(user);
    }

    @Override
    public int findMaxUserId() {
        return leaderMapper.findMaxUserId();
    }

    @Override
    public void addUserRole(int id) {
        leaderMapper.addUserRole(id);
    }

    @Override
    public PageMessage<List<Leader>> findLeaders(int page, int size) {
        PageHelper.startPage(page,size);
        List<Leader> list = leaderMapper.findLeaders();
        PageInfo<Leader> info = PageInfo.of(list);

        PageMessage<List<Leader>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(info.getPages());
        pageMessage.setTotal(info.getTotal());
        pageMessage.setData(list);
        return pageMessage;
    }

    @Override
    public Leader findByName(String leaderName) {
        return leaderMapper.findByName(leaderName);
    }
}
