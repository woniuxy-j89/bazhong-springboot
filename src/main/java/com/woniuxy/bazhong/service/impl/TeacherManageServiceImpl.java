package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.entity.Teacher;
import com.woniuxy.bazhong.entity.TeacherClasses;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.mapper.SemesterMapper;
import com.woniuxy.bazhong.mapper.TeacherManageMapper;
import com.woniuxy.bazhong.mapper.UserMapper;
import com.woniuxy.bazhong.service.TeacherManageService;
import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class TeacherManageServiceImpl implements TeacherManageService {

    /**
     * 教师及班级管理的mapper层类
     */
    @Resource
    private TeacherManageMapper teacherManageMapper;

    /**
     * 用户管理mapper
     */
    @Resource
    private UserMapper userMapper;

    /**
     * 学期管理mapper
     */
    @Resource
    private SemesterMapper semesterMapper;

    /**
     * 查询展示所有老师
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    @Override
    public PageMessage<List<Teacher>> allTeacher( int page, int size) {
        //获取当前学校id
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user.getSchoolId();
        PageHelper.startPage(page,size);

        List<Teacher> teachers = teacherManageMapper.allTeacher(schoolId);
        /**
         * 将教师列表进行分页处理
         */
        PageInfo<Teacher> pageInfo = PageInfo.of(teachers);
        log.info(pageInfo.toString());
        // 封装信息
        PageMessage<List<Teacher>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        log.info("pages:" + pageInfo.getPages());
        log.info("total:" + pageInfo.getTotal());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(teachers);


        return pageMessage;
    }

    /**
     * 查询展示科目对应的授课老师
     * @param subjectId 科目id
     * @return
     */
    @Override
    public List<Teacher> teacherOfSubject(int subjectId) {
        //获取当前学校id
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user.getSchoolId();

        List<Teacher> teachers = teacherManageMapper.teacherOfSubject(schoolId,subjectId);
        return teachers;
    }

    /**
     * 通过教师名字查询教师信息及当前学期所带班级信息，并封装为分页数据
     * @param teacherName 教师姓名
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    @Override
    public PageMessage<List<Classes>> classessOfTeacher(String teacherName, int page, int size) {

        //查询当前学期

        String nowSemester = semesterMapper.findNowSemeSter();
        log.info(nowSemester);

        PageHelper.startPage(page,size);


        List<Classes> classes = teacherManageMapper.classessOfTeacher(teacherName,nowSemester);

        //  将教师信息中的当前学期所带班级信息进行分页处理

        PageInfo<Classes> pageInfo = PageInfo.of(classes);
        log.info(pageInfo.toString());
        // 封装信息
        PageMessage<List<Classes>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        log.info("pages:" + pageInfo.getPages());
        log.info("total:" + pageInfo.getTotal());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(classes);


        return pageMessage;
    }

    /**
     * 教师账号管理中，重置教师密码
     * @param teacher 所要修改班级的信息
     */
    @Transactional
    @Override
    public Boolean resetTeacherPassword(Teacher teacher) {
        int result = userMapper.resetTeacherPassword(teacher.getUserId());
        return result > 0 ?true:false;
    }

    /**
     * 教师账号管理下，重新指定班級中刪除所选班级
     * @param teacherClasses 所要修改班级的信息
     */
    @Override
    @Transactional
    public Boolean delClassesOfTeacher(TeacherClasses teacherClasses) {
        int result = teacherManageMapper.delClassesOfTeacher(teacherClasses);
        return result > 0 ?true:false;
    }

    /**
     * 教师账号管理中，添加班级下，没有指定老师对应课程的老师的班级
     * @param teacher 指定老师信息
     * @return Classes 班級
     */
    @Override
    public List<Classes> nullSubTeacherOfClasses(Teacher teacher) {
        //获取当前学校id
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user.getSchoolId();

        //查询当前学期

        String nowSemester = semesterMapper.findNowSemeSter();
        log.info(nowSemester);

        List<Classes> classes = teacherManageMapper.nullSubTeacherOfClasses(schoolId,nowSemester,teacher.getSubject().getType(),teacher.getSubjectId());

        return classes;
    }


}
