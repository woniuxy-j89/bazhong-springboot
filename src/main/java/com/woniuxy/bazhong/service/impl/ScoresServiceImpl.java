package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.HighestScore;
import com.woniuxy.bazhong.entity.Scores;
import com.woniuxy.bazhong.entity.Video;
import com.woniuxy.bazhong.mapper.HighestScoreMapper;
import com.woniuxy.bazhong.mapper.ScoresMapper;
import com.woniuxy.bazhong.service.ScoresService;
import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ScoresServiceImpl implements ScoresService {

    @Resource
    private ScoresMapper scoresMapper;

    @Resource
    private HighestScoreMapper highestScoreMapper;

    @Override
    public Map<String, Integer> studentRank(String classes, String exam,int schoolId) {
        //
        int one = 0,
                two = 0,
                three = 0,
                four = 0,
                five = 0,
                six = 0,
                seven = 0,
                eight = 0,
                nine = 0,
                ten = 0;

        int index = 0;


        if(exam.contains("总分")){
            List<HighestScore> scores = highestScoreMapper.findAll(classes,exam,schoolId);

            List<HighestScore> studentScores = highestScoreMapper.findClassStudents(classes, exam,schoolId);
            int num = scores.size()/10;
            //循环班级学生成绩 判断学生成绩属于那个区间
            for (HighestScore score:
                    studentScores) {
                index = scores.indexOf(score);
                if(index < num){
                    one++;
                }else if(index>=num && index <2*num) {
                    two++;
                }else if( index >=2*num && index<3*num ){
                    three++;
                }else if(index >=3*num && index<4*num){
                    four++;
                }else if(index >=4*num && index<5*num){
                    five++;
                }else if(index >=5*num && index<6*num){
                    six++;
                }else if(index >=6*num && index<7*num){
                    seven++;
                }else if(index >=7*num && index<8*num){
                    eight++;
                }else if(index >=8*num && index<9*num){
                    nine++;
                }else {
                    ten++;
                }

            }
        }else{
            //查询有多少学生 和其考试成绩 排序
            List<Scores> scores= scoresMapper.findAll(classes,exam,schoolId);
            //查询班级中所有学生的成绩
            List<Scores> studentScores = scoresMapper.findClassStudents(classes,exam,schoolId);
            double num =scores.size()/10.0 ;
            //循环班级学生成绩 判断学生成绩属于那个区间
            for (Scores score:
                    studentScores) {
                index = scores.indexOf(score);

                if(index < num){
                    one++;
                }else if(index>=num && index <2*num) {
                    two++;
                }else if( index >=2*num && index<3*num ){
                    three++;
                }else if(index >=3*num && index<4*num){
                    four++;
                }else if(index >=4*num && index<5*num){
                    five++;
                }else if(index >=5*num && index<6*num){
                    six++;
                }else if(index >=6*num && index<7*num){
                    seven++;
                }else if(index >=7*num && index<8*num){
                    eight++;
                }else if(index >=8*num && index<9*num){
                    nine++;
                }else {
                    ten++;
                }

            }
        }

        Map<String,Integer> map = new HashMap<>();
        map.put("前0~10%",one);
        map.put("前10~20%",two);
        map.put("前20~30%",three);
        map.put("前30~40%",four);
        map.put("前40~50%",five);
        map.put("前50~60%",six);
        map.put("前60~70%",seven);
        map.put("前70~80%",eight);
        map.put("前80~90%",nine);
        map.put("前90~100%",ten);

        return map;
    }

    @Override
    public PageMessage<List<Scores>> findAllScoreByclass(String classes, String exam, int schoolId,int page ,int size) {
        PageHelper.startPage(page,size);


        List<Scores> scores= scoresMapper.findAllStudentScores(classes,exam,schoolId);

        PageInfo<Scores> pageInfo = PageInfo.of(scores);

        PageMessage<List<Scores>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(scores);

        return pageMessage;
    }

    @Override
    public List<Scores> findScoresByStId(int id) {
        return scoresMapper.findScoresByStId(id);
    }
}
