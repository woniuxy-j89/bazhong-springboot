package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.entity.Subject;
import com.woniuxy.bazhong.entity.TeacherClasses;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.mapper.ClassesManageMapper;
import com.woniuxy.bazhong.mapper.SemesterMapper;
import com.woniuxy.bazhong.service.ClassesManageService;
import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Service
public class ClassesManageServiceImpl implements ClassesManageService {

    /**
     * 学期管理mapper
     */
    @Resource
    private SemesterMapper semesterMapper;

    /**
     * 班级管理mapper
     */
    @Resource
    private ClassesManageMapper classesManageMapper;

    /**
     * 查询展示当前学期的所有班级
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageMessage<List<Classes>> allClasses(int page, int size) {
        //获取当前学校id
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user.getSchoolId();
        /**
         * 查询当前学期
         */
        String nowSemester = semesterMapper.findNowSemeSter();
        log.info(nowSemester);

        PageHelper.startPage(page,size);

        List<Classes> classes = classesManageMapper.allClasses(nowSemester,schoolId);

        /**
         * 将所有班级信息进行分页处理
         */
        PageInfo<Classes> pageInfo = PageInfo.of(classes);

        // 封装信息
        PageMessage<List<Classes>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(classes);


        return pageMessage;
    }

    /**
     * 根据所选班级的班级类型查询展示学科
     * @param classesId
     * @return
     */
    @Override
    public List<Subject> subOfClasses(int classesId) {
        List<Subject> subjects = classesManageMapper.subOfClasses(classesId);
        return subjects;
    }

    /**
     * 授课教师安排中，安排各学科授课教师
     * @param teacherClasses 所要添加班级的信息
     */
    @Override
    @Transactional
    public Boolean addsubTeacherOfClasses(TeacherClasses teacherClasses) {
        int result = classesManageMapper.addsubTeacherOfClasses(teacherClasses);
        return result > 0 ?true:false;
    }

    /**
     * 班主任任命中，根据所选班级，展示所有任课教师
     * @param classesId 班级id
     * @return
     */
    @Override
    public Classes allSubTeacherOfClasses(int classesId) {
        Classes classes = classesManageMapper.allSubTeacherOfClasses(classesId);
        return classes;
    }

    /**
     * 班主任任命中，任命或更换班主任
     * @param classesId 班级id
     * @param teacherId 老师id
     */
    @Override
    @Transactional
    public Boolean updateHTeacherOfClasses(int classesId, int teacherId) {
        int result = classesManageMapper.updateHTeacherOfClasses(classesId, teacherId);
        return result > 0 ?true:false;
    }

    /**
     * 添加班级
     * @param classes
     * @return
     */
    @Override
    @Transactional
    public Boolean addClasses(Classes classes) {
        //获取当前学校id
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user.getSchoolId();
        classes.setSchoolId(schoolId);
        //查询当前学期

        String nowSemester = semesterMapper.findNowSemeSter();
        log.info(nowSemester);
        classes.setSemesterId(nowSemester);

        int result = classesManageMapper.addClasses(classes);
        return result > 0 ?true:false;
    }

    /**
     * 修改班级类型
     * @param classes 所要修改班级的信息
     */
    @Override
    @Transactional
    public Boolean updateClassesType(Classes classes) {
        int result = classesManageMapper.updateClassesType(classes);
        return result > 0 ?true:false;
    }

    /**
     * 更换班主任
     * @param classes 所要修改班级的信息
     */
    @Override
    @Transactional
    public Boolean updateClassesHT(Classes classes) {
        int result = classesManageMapper.updateClassesHT(classes);
        return result > 0 ?true:false;
    }
}
