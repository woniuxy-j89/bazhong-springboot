package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.Questions;
import com.woniuxy.bazhong.entity.Video;
import com.woniuxy.bazhong.mapper.QuestionsMapper;
import com.woniuxy.bazhong.service.QuestionsService;
import com.woniuxy.bazhong.service.VideoService;
import com.woniuxy.bazhong.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author zhangjy
 * @date 2022/8/3 1:11
 */
@Slf4j
@Service
public class QuestionsServiceImpl implements QuestionsService {

    @Resource
    private QuestionsMapper questionsMapper;
    @Resource
    private VideoService videoService;

    @Transactional
    @Override
    public boolean addByKnowledgePoint(int studentId, int knowledgePointId, String questionsContent, String status) {
        log.info("学生id：" + studentId + "...知识点id：" + knowledgePointId + "...提问内容：" + questionsContent + "...公开状态：" + status);

        boolean flag = false;   // 默认插入失败

        // 获取学生提问时间
        String questionTime = DateUtil.getDate();

        // 设置学生提问的问题相关属性
        Questions questions = new Questions();
        questions.setStudentId(studentId);
        questions.setQuestionsContent(questionsContent);
        questions.setStatus(status);
        questions.setQuestionTime(questionTime);

        log.info("插入前问题id为：" + questions.getId());

        // 将问题存入数据库
        flag = questionsMapper.add(questions);

        log.info("插入后问题id为..." + questions.getId());

        // 问题知识点第三关联表插入问题id和知识点id
        flag = questionsMapper.addQuestionsAndKnowledgePoint(questions.getId(), knowledgePointId);

        return flag;
    }

    @Transactional
    @Override
    public boolean addByVideo(int studentId, int videoId, String questionsContent, String status) {
        log.info("学生id：" + studentId + "...知识点id：" + videoId + "...提问内容：" + questionsContent + "...公开状态：" + status);

        boolean flag = false;   // 默认插入失败

        // 获取学生提问时间
        String questionTime = DateUtil.getDate();

        // 设置学生提问的问题相关属性
        Questions questions = new Questions();
        questions.setStudentId(studentId);
        questions.setQuestionsContent(questionsContent);
        questions.setStatus(status);
        questions.setQuestionTime(questionTime);

        log.info("插入前问题id为：" + questions.getId());

        // 将问题插入数据库
        flag = questionsMapper.add(questions);

        log.info("插入后问题id为..." + questions.getId());

        // 根据视频id查询一个视频，从而得到其知识点id
        Video video = videoService.findById(videoId);
        log.info("该video的信息为：" + video);

        // 问题知识点第三关联表插入问题id和知识点id
        flag = questionsMapper.addQuestionsAndVideo(questions.getId(), video.getKnowledgepointId());

        return flag;
    }
}
