package com.woniuxy.bazhong.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.mapper.HomeworkMapper;
import com.woniuxy.bazhong.service.HomeworkService;
import com.woniuxy.bazhong.utils.CheckIsExpiredUtil;
import com.woniuxy.bazhong.utils.PageMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zhangjy
 * @date 2022/7/23 15:58
 */
@Service
@Slf4j
public class HomeworkServiceImpl implements HomeworkService {

    @Resource
    private HomeworkMapper homeworkMapper;

    @Override
    public PageMessage<List<HomeworkStudent>> all(int studentId, int page, int size) {
        List<HomeworkStudent> homeworkStudents = new ArrayList<>();

        // 设置页码和每一页大小
        PageHelper.startPage(page, size);

        homeworkStudents = homeworkMapper.findAllByStudentId(studentId);
//        log.info("没有设置isExpired属性前-----" + homeworkStudents);

        // 给每个作业的isExpired属性赋值——用于前端根据该属性的不同值显示不同的按钮
        for (HomeworkStudent homeworkStudent : homeworkStudents) {
            // 调用工具类判断作业是否过期，并给每个作业学生isExpired属性赋值
            CheckIsExpiredUtil.checkAndSetIsExpired(homeworkStudent);
        }

        // 获取分页信息
        PageInfo<HomeworkStudent> pageInfo = PageInfo.of(homeworkStudents);
       /* System.out.println("总记录数：" + pageInfo.getTotal());    // 总记录数
        System.out.println("总页数：" + pageInfo.getPages());    // 总页数
        System.out.println("当前页大小：" + pageInfo.getSize()); // 当前页大小
        System.out.println("当前页数据：" + pageInfo.getList()); // 当前页数据*/

        // 设置信息
        PageMessage<List<HomeworkStudent>> pageMessage = new PageMessage<>();
        pageMessage.setCurrentPage(page);
        pageMessage.setSize(size);
        pageMessage.setTotalPage(pageInfo.getPages());
        pageMessage.setTotal(pageInfo.getTotal());
        pageMessage.setData(homeworkStudents);

//        log.info("判断是否过期后...." + pageMessage);

        return pageMessage;
    }

    @Override
    public Homework findById(int id) {
        Homework homework = homeworkMapper.findById(id);
        return homework;
    }

    @Override
    public HomeworkStudent findByStudentIdAndHomeworkId(int studentId, int homeworkId) {

        // 查询作业学生基本信息、所有作业题目信息（此时不包括题目信息、作业学生答案）
        HomeworkStudent homeworkStudent = homeworkMapper.findByStudentIdAndHomeworkId(studentId, homeworkId);

        // 判断homeworkStudent是否过期，并设置isExpired属性
        homeworkStudent = CheckIsExpiredUtil.checkAndSetIsExpired(homeworkStudent);
//        log.info("before查询对应题目表：" + homeworkStudent);

        // 得到该学生该作业的所有作业题目信息
        List<HomeworkTopic> homeworkTopics = homeworkStudent.getHomeworkTopics();

        int questiontypesId = 0;
        int topicId = 0;

        log.info("作业过期状态---------" + homeworkStudent.getIsExpired() + "...作业是否提交(0未提交，1已提交)---------" + homeworkStudent.getStatus());

        // 根据题型id和题目id，查询相应的题型表，得到指定题目的所有题目信息
        for (HomeworkTopic homeworkTopic : homeworkTopics) {
            // 得到题型id和题目id
            questiontypesId = homeworkTopic.getQuestiontypesId();
            topicId = homeworkTopic.getTopicId();

//            log.info("这道题的作业题目id是：" + homeworkTopic.getId() + "...题型id为：" + questiontypesId + "...题目id为：" + topicId);

            // 判断题目的题型，根据题型查询不同题型表，得到该题目信息
            if (questiontypesId == 1) { // 选择题
                // 查询该题目信息
                Choice choice = homeworkMapper.findChoice(questiontypesId, topicId);
//                homeworkTopic.setTopicInfo(choice);

                // 查询学生提交的答案（未提交则为null）；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为对应属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setChoice(choice);
            } else if (questiontypesId == 2) { // 填空题
                // 查询该题目信息
                Blank blank = homeworkMapper.findBlank(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setBlank(blank);
            } else if (questiontypesId == 3) { // 判断题
                // 查询该题目信息
                Bank bank = homeworkMapper.findBank(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setBank(bank);
            } else if (questiontypesId == 4) { // 阅读题
                // 查询该题目信息
                Reading reading = homeworkMapper.findReading(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setReading(reading);
            } else if (questiontypesId == 5) { // 解答题
                // 查询该题目信息
                Responsequestion responsequestion = homeworkMapper.findResponsequestion(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setResponsequestion(responsequestion);
            } else if (questiontypesId == 6) { // 作文题
                // 查询该题目信息
                Title title = homeworkMapper.findTitle(questiontypesId, topicId);

                // 查询学生提交的答案；根据学生id、作业id、作业题目id唯一确定一个学生提交的一个作业中的一道题的答案
                HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopic.getId());

                // 并将学生提交的答案设置为属性
                homeworkTopic.setHomeworkAnswer(homeworkAnswer);

                homeworkTopic.setTitle(title);
            }
        }

        return homeworkStudent;
    }

    @Transactional
    @Override
    public boolean addHomeworkAnswer(int studentId, int homeworkId, int homeworkTopicId, String answer) {
        boolean flag = false;

        // 根据学生id、作业id、作业题目id查询对的应唯一的学生作业答案
        HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopicId);
        log.info("该道题的学生提交答案是：" + answer);

        // 判断该道题的学生作业答案是否存在
        if (homeworkAnswer == null) {   // 不存在
            // 将学生答案存入数据库
            flag = homeworkMapper.addHomeworkAnswer(studentId, homeworkId, homeworkTopicId, answer);

            // 得到学生作业
            HomeworkStudent homeworkStudent = homeworkMapper.findByStudentIdAndHomeworkId(studentId, homeworkId);
            log.info("作业提交状态为：" + homeworkStudent.getStatus());

            // 修改作业学生提交状态status
            homeworkStudent.setStatus("1");   // 1表示提交，0表示未提交
            flag = homeworkMapper.updateHomeworkStudent(homeworkStudent);

            log.info("修改后，作业提交状态为：" + homeworkStudent.getStatus());
        }

        return flag;
    }

    @Override
    public boolean updateHomeworkAnswer(int studentId, int homeworkId, int homeworkTopicId, String answer) {
        boolean flag = false;

        // 根据学生id、作业id、作业题目id查询对的应唯一的学生作业答案
        HomeworkAnswer homeworkAnswer = homeworkMapper.findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(studentId, homeworkId, homeworkTopicId);
        log.info("该题目原学生提交答案是：" + homeworkAnswer);

        // 判断该道题的学生作业答案是否存在
        if (homeworkAnswer != null) {   // 不存在
            // 修改答案
            homeworkAnswer.setAnswer(answer);

            // 将学生答案存入数据库
            flag = homeworkMapper.updateHomeworkAnswer(homeworkAnswer);

            log.info("修改答案后为：" + homeworkAnswer);
        }

        return flag;
    }
}
