package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.PlacementTime;
import com.woniuxy.bazhong.mapper.PlacementTimeMapper;
import com.woniuxy.bazhong.service.PlacementTimeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class PlacementTimeServiceImpl implements PlacementTimeService {

    @Resource
    private PlacementTimeMapper placementTimeMapper;


    //添加分班时间
    @Transactional
    @Override
    public boolean addTime(PlacementTime placementTime) {

        boolean result=placementTimeMapper.addTime(placementTime);

        return result;
    }

    @Override
    public PlacementTime findPlacementTime() {

        return placementTimeMapper.findPlacementTime();
    }
}
