package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.mapper.DataMapper;
import com.woniuxy.bazhong.service.DataService;
import com.woniuxy.bazhong.utils.NumberFormateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
* @author lzw
* 数据统计
*/
@Service
public class DataServiceImpl implements DataService {
    @Resource
    private DataMapper dataMapper;

    //
    @Override
    //获取各科试卷的id
    public List<Long> getTestPaper(String semester, String startDate, String endDate) {
        return dataMapper.getTestPaper(semester,startDate,endDate);
    }

    @Override

    public List<Data<Object>> getData(String grade, String year, List<Long> list) {
        List<Double> scores = getScores(grade,list);
        System.out.println(scores);
        List<Data<Object>> result = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();

        //分数段对应的人数
        ScoreLine scoreLine=dataMapper.getScoreLine(year);
        ScoreOfNums scoreOfNums=getScoreOfNums(scoreLine,scores);
        //计算百分比
        map.put("未达"+year+"年专科线", NumberFormateUtil.getNumFormate(scoreOfNums.getNumUnderSpecialty(),scores.size()));
        map.put("达到"+year+"年专科线",NumberFormateUtil.getNumFormate(scoreOfNums.getNumSpecialty(),scores.size()));
        map.put("达到"+year+"年本科线",NumberFormateUtil.getNumFormate(scoreOfNums.getNumRegular(),scores.size()));
        map.put("达到"+year+"年重本线",NumberFormateUtil.getNumFormate(scoreOfNums.getNumHeavy(),scores.size()));
        getDataList(result,map);
        return result;
    }

    //获取升学人数
    @Override
    public List<Data<Object>> getNums(String grade, String year, List<Long> list) {
        List<Double> scores = getScores(grade,list);
        ScoreLine scoreLine=dataMapper.getScoreLine(year);
        ScoreOfNums scoreOfNums=getScoreOfNums(scoreLine,scores);
        List<Data<Object>> dataList = new ArrayList<>();
        dataList.add(new Data<>().setKey("未升学").setValue(scoreOfNums.getNumUnderSpecialty()));
        dataList.add(new Data<>().setKey("专科").setValue(scoreOfNums.getNumSpecialty()));
        dataList.add(new Data<>().setKey("本科").setValue(scoreOfNums.getNumRegular()));
        dataList.add(new Data<>().setKey("重本").setValue(scoreOfNums.getNumHeavy()));
        return dataList;
    }

    @Override
    //获取升学率
    public List<Data<Object>> getRate(String grade, String year, List<Long> list) {
        Map<String,Object> map=new HashMap<>();
        List<Double> scores = getScores(grade, list);
        ScoreLine scoreLine=dataMapper.getScoreLine(year);
        ScoreOfNums scoreOfNums=getScoreOfNums(scoreLine,scores);
        List<Data<Object>> dataList=new ArrayList<>();

        map.put("未升学",NumberFormateUtil.getNumFormate(scoreOfNums.getNumUnderSpecialty(),scores.size()));
        map.put("预估专科升学率",NumberFormateUtil.getNumFormate(scoreOfNums.getNumSpecialty(),scores.size()));
        map.put("预估本科升学率",NumberFormateUtil.getNumFormate(scoreOfNums.getNumRegular(),scores.size()));
        map.put("预估重本升学率",NumberFormateUtil.getNumFormate(scoreOfNums.getNumHeavy(),scores.size()));
        getDataList(dataList,map);
        return dataList;
    }

    @Override
    public List getSemester() {
        return dataMapper.getSemester();
    }

    @Override
    public List<Grade> getGrades() {
        return dataMapper.getGrades();
    }

    //map转set
    private void getDataList(List<Data<Object>> result, Map<String, Object> map) {
        Set<Map.Entry<String,Object>> sets=map.entrySet();
        Iterator<Map.Entry<String,Object>> iterator=sets.iterator();
        while (iterator.hasNext()){
            Map.Entry<String,Object> entry=iterator.next();
            String key=entry.getKey();
            Object value=entry.getValue();
            Data<Object> data=new Data<>();
            data.setKey(key);
            data.setValue(value);
            result.add(data);
        }
    }

    //获取每个学生总分
    private List<Double> getScores(String grade, List<Long> list) {
        Set<String> numbers=new HashSet<>();
        List<Double> scores=new ArrayList<>();
        List<ExamAnswer> examAnswers=new ArrayList<>();
        for (Long id : list) {
            //根据年级，试卷id获取答题卡信息
            examAnswers= dataMapper.getExamAnswer(grade,id);
        }
        //获取学号，set去重
        for (ExamAnswer examAnswer:examAnswers) {
            numbers.add(examAnswer.getNumber());
        }
        // 再根据学号查出成绩，再算总分
        for (String number:numbers) {
            List<Double> numberScores=dataMapper.getScore(number);

            //每个学号对应的总分
            double score=0;
            for (Double numberScore:numberScores) {
                score+=numberScore;
            }
            scores.add(score);
        }
        return scores;
    }

    //分数段对应的人数
    private ScoreOfNums getScoreOfNums(ScoreLine scoreLine,List<Double> scores){
        ScoreOfNums scoreOfNums=new ScoreOfNums();
        int numUnderSpecialty=0;
        int numSpecialty=0;
        int numRegular=0;
        int numHeavy=0;
        double specialty=scoreLine.getSpecialty();
        double regular=scoreLine.getRegular();
        double heavy=scoreLine.getHeavy();
        for (Double score:scores) {
            if (0<=score&&score<=specialty){
                numUnderSpecialty++;
            }
            else if (score<=regular){
                numSpecialty++;
            }
            else if (score<=heavy){
                numRegular++;
            }else {
                numHeavy++;
            }
        }
        scoreOfNums.setNumUnderSpecialty(numUnderSpecialty);
        scoreOfNums.setNumSpecialty(numSpecialty);
        scoreOfNums.setNumRegular(numRegular);
        scoreOfNums.setNumHeavy(numHeavy);
        return scoreOfNums;
    }

}
