package com.woniuxy.bazhong.service.impl;

import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.mapper.*;
import com.woniuxy.bazhong.service.StudentService;
import com.woniuxy.bazhong.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.annotation.Resources;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private SchoolMapper schoolMapper;
    @Resource
    private LeaderMapper leaderMapper;
    @Resource
    private EducationBureauMapper educationBureauMapper;
    @Resource
    private TeacherMapper teacherMapper;

    @Resource
    private SubjectMapper subjectMapper;

    @Resource
    private StudentMapper studentMapper;

    @Resource
    private ClassesMapper classesMapper;

    @Resource
    private ChatMessageMapper chatMessageMapper;

    @Override
    public List<User> all() {
        return userMapper.all();
    }


    //根据学校id查询为老师的用户
    @Override
    public List<User> findTeacherUserBySchool(int i) {
        List<User> teachers= userMapper.findTeacherUserBySchool(i);
        return teachers;
    }

    @Override
    public List<User> findPlacementStudent(int enrollmentYear, int schoolId) {
        List<User> students = userMapper.findPlacementStudent(enrollmentYear,schoolId);
        return students;
    }

    @Override
    public User findById(int id) {
        return userMapper.findById(id);
    }
    @Override
    public School findSchool(int schoolId) {

        return schoolMapper.findSchoolById(schoolId);
    }

    @Override
    public EducationBureau findeducationBureau(int userId) {


        return educationBureauMapper.findOne(userId);
    }

    @Override
    public Leader findLeader(int schoolId) {

        return leaderMapper.findLeaderBySchoolId(schoolId);
    }

    @Override
    public Teacher findTeacher(int userId) {
        return teacherMapper.findByUserId(userId);
    }

    @Override
    public Student findStudnetByUid(int userId) {
        return studentMapper.findByUserId(userId);
    }

    @Override
    public Classes findByStudent(int classId) {

        return classesMapper.findByClassId(classId);
    }

    @Override
    public Subject findSubject(int subjectId) {

        return subjectMapper.findSubject(subjectId);
    }

    @Override
    public List<ChatMessage> findInform(int userId) {
        return chatMessageMapper.findInform(userId);
    }


}
