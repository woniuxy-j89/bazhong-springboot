package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.entity.School;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.utils.PageMessage;

import java.util.List;

/**
* @author lzw
*/
public interface SchoolService {

    public Integer findMaxId();

    public String findNumber(Integer maxId);

    public int  createSchool(School school);

    public School findSchoolById(int maxId);

    public void add(User user);

    public int findMaxUserId();

    public void addUserRole(int id);

    public PageMessage<List<School>> findSchools(int page, int size);

    public School findByNumber(String schoolNumber);
}
