package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Questions;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhangjy
 * @date 2022/8/2 23:42
 */
public interface QuestionsService {

    /**
     * 根据知识点提问
     * @param studentId
     * @param knowledgePointId
     * @param questionsContent
     * @param status
     * @return
     */
    public boolean addByKnowledgePoint(int studentId, int knowledgePointId, String questionsContent, String status);

    /**
     * 学生根据视频提问
     * @param studentId
     * @param videoId
     * @param questionsContent
     * @param status
     * @return
     */
    public boolean addByVideo(int studentId, int videoId, String questionsContent, String status);
}
