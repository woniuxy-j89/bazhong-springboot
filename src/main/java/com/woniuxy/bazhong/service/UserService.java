package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.*;

import java.util.List;

public interface UserService {
    public List<User> all();

    //查询一个学校的所有老师
    public List<User> findTeacherUserBySchool(int i);

    //查询某一届某一学校的所有学生用户
    public List<User> findPlacementStudent(int enrollmentYear, int schoolId);

    /**
     * 根据用户id查询用户
     * @param id
     * @return
     */
    public User findById(int id);

    School findSchool(int schoolId);

    EducationBureau findeducationBureau(int id);

    Leader findLeader(int schoolId);

    Teacher findTeacher(int id);

    Student findStudnetByUid(int id);

    Classes findByStudent(int classId);

    Subject findSubject(int subjectId);

    List<ChatMessage> findInform(int id);
}
