package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.entity.Subject;
import com.woniuxy.bazhong.entity.TeacherClasses;
import com.woniuxy.bazhong.utils.PageMessage;

import java.util.List;

public interface ClassesManageService {
    /**
     * 查询展示当前学期的所有班级
     * @param page
     * @param size
     * @return
     */
    public PageMessage<List<Classes>> allClasses(int page, int size);

    /**
     * 根据所选班级的班级类型查询展示学科
     * @param classesId
     * @return
     */
    public List<Subject> subOfClasses(int classesId);

    /**
     * 授课教师安排中，安排各学科授课教师
     * @param teacherClasses 所要添加班级的信息
     */
    public Boolean addsubTeacherOfClasses(TeacherClasses teacherClasses);

    /**
     * 班主任任命中，根据所选班级，展示所有任课教师
     * @param classesId 班级id
     * @return
     */
    Classes allSubTeacherOfClasses(int classesId);

    /**
     *  班主任任命中，任命或更换班主任
     * @param classesId 班级id
     * @param teacherId 老师id
     */
    public Boolean updateHTeacherOfClasses(int classesId, int teacherId);

    /**
     * 添加班级
     * @param classes
     * @return
     */
    public Boolean addClasses(Classes classes);

    /**
     * 修改班级类型
     * @param classes 所要修改班级的信息
     */
    public Boolean updateClassesType(Classes classes);

    /**
     * 更换班主任
     * @param classes 所要修改班级的信息
     */
    public Boolean updateClassesHT(Classes classes);
}
