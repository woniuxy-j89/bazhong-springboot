package com.woniuxy.bazhong.service;

import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.entity.Teacher;
import com.woniuxy.bazhong.entity.TeacherClasses;
import com.woniuxy.bazhong.utils.PageMessage;

import java.util.List;

public interface TeacherManageService {
    /**
     * 查询展示所有老师
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    public PageMessage<List<Teacher>> allTeacher( int page, int size);

    /**
     * 查询展示科目对应的授课老师
     * @param subjectId 科目id
     * @return
     */
    List<Teacher> teacherOfSubject(int subjectId);

    /**
     * 通过教师名字查询教师信息及所带班级信息，并封装为分页数据
     * @param teacherName 教师姓名
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    PageMessage<List<Classes>> classessOfTeacher(String teacherName, int page, int size);

    /**
     * 教师账号管理中，重置教师密码
     * @param teacher 所要修改班级的信息
     */
     Boolean resetTeacherPassword(Teacher teacher);

    /**
     * 教师账号管理下，重新指定班級中刪除所选班级
     * @param teacherClasses 所要修改班级的信息
     */
    Boolean delClassesOfTeacher(TeacherClasses teacherClasses);
    /**
     * 教师账号管理中，添加班级下，没有指定老师对应课程的老师的班级
     * @param teacher 指定老师信息
     * @return Classes 班級
     */
    List<Classes> nullSubTeacherOfClasses(Teacher teacher);


}
