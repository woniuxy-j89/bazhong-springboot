package com.woniuxy.bazhong.entity;

import lombok.Data;

/**
 * @author: lzw
 * @description: 分数线
 */
@Data
public class ScoreLine {
    private int id;
    private String year;//年份
    private double specialty;//专科分数线
    private double regular;//本科分数线
    private double heavy;//重本分数线
}