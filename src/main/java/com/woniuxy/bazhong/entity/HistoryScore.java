package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lvjiabin
 * @data 2020-07-21
 *历史成绩表
 */
@Data
public class HistoryScore implements Serializable {
    private int id; //历史成绩id
    private String classes; //班级名称(班级详细描述)
    private int num; //班级人数
    private double totalScore; //总成绩
    private double averageScore; //平均成绩
    private int examId; //考试试卷id
    private double gradeAverage; //年级平均分
    private Exam exam;


}
