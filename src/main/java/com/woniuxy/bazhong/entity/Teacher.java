package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhangjy
 * 老师实体类
 */
@Data
public class Teacher implements Serializable {
    private int id;
    private String name;    // 老师姓名
    private String type;    // 老师类型：0-普通任课教师 1-班主任 2-年级主任
    private String headImage;   // 个人头像
    private String phone;   // 手机号
    private String email;   // 邮箱
    private int userId; // 用户id
    private int subjectId;  // 老师教的科目id
    private Subject subject;//老师所教科目
    private List<Classes> classes; //老师所带班级
}
