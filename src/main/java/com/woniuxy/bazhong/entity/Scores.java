package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by raoyucheng
 * on 2022/7/21 16:21
 */
@Data
public class Scores implements Serializable {
    private Integer id;             //成绩id，主键
    private Double score;           //学生的成绩分数
    private String type;            //考试类型
    private Integer subjectId;      //科目id，可以从科目表查学生信息
    private Integer studentId;      //学生id，可以从学生表查学生信息
    private Integer examId;         //考试试卷表id，可以从考试试卷表查考试试卷信息
    private Student student; 
}
