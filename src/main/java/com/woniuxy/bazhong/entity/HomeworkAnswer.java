package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lvjiabin
 * @data 2020-07-21
 * 学生作业答案表
 */
@Data
public class HomeworkAnswer implements Serializable {
    private int id; //学生作业答案表id
    private int homeworkTopicId; //作业题目表id
    private int studentId; //学生id
    private String answer; //学生答案
    private String comments; //老师批注
    private double gscore; //学生获得的成绩
    private int homeworkId; //作业id
}
