package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by raoyucheng
 * on 2022/7/21 16:05
 */
@Data
public class Role implements Serializable {
    private Integer id;             //角色id，主键
    private String name;            //角色名称
    private String status;          //角色状态，值：0（账号不可用）或1（账号可用）
    private String description;     //角色名称的描述

    private List<Perms> perms;      //一个角色拥有多个权限
}
