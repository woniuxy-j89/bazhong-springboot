package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiaxin-yang
 * @data 2022/7/21 15:42
 * 已做题库
 */

@Data
public class FinishedQuestion implements Serializable {
    private int id;               //已做题库表id
    private String status;        //是否做错 0-错误 1-正确
    private int questiontypes_id; //题型id
    private int topic_id;         //题目id
    private int student_id;       //学生id
}
