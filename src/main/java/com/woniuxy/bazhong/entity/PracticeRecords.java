package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 这张表用于记录学生尚未做完的题目（做到一半中途退出）
 * @author liwei
 * @data 2022/7/21{} 15:07
 */
@Data
public class PracticeRecords implements Serializable {
    private int id;                     //做题记录id
    private String questionBankType;    //所刷题库类型
    private int questionTypesId;        //题型id
    private int topicId;                //题目id
    private int studentId;              //学生id
}
