package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 权限实体类
 * @author liwei
 * @data 2022/7/21{} 14:54
 */
@Data
public class Perms implements Serializable {
    private int id;             //权限id
    private String name;        //权限名字
    private String description; //权限描述

}
