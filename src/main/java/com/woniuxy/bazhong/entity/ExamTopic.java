package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiaxin-yang
 * @data 2022/7/21 15:42
 * 考试题目表
 */

@Data
public class ExamTopic implements Serializable {
    private int id; //考试题目id
    private int topicScore; //题目实际设置分值
    private int examId; //考试id
    private int questiontypesId; //题型id
    private int topicId; //题目id

}
