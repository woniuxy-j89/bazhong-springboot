package com.woniuxy.bazhong.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * @author lzw
 */
@Data
public class Blank implements Serializable {        //填空题
    private int id;
    private String content;     //题目内容
    private String answer;      //答案
    private String answerAnalysis;   //解析
    private double scoreValue;       //分值
    private int questiontypesId;     //题型id
    private int knowledgepointId;        //知识点id
}
