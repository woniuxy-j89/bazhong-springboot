package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author WangQi
 * @date 2022/7/21 15:21
 * 作业题目实体类
 */
@Data
public class HomeworkTopic implements Serializable {
    private int id;                 // 作业题目id
    private Double topicScore;      // 题目实际分值
    private int homeworkId;         // 作业_id
    private int questiontypesId;    // 题型_id
    private int topicId;            // 题目_id

    private HomeworkAnswer homeworkAnswer;  // 作业题目和学生作业答案为一对一关系

    // 6种题型的题目信息
    private Choice choice;
    private Blank blank;
    private Bank bank;
    private Reading reading;
    private Responsequestion responsequestion;
    private Title title;
}
