package com.woniuxy.bazhong.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhangjy
 * 学生实体类
 */
@Data
public class Student implements Serializable {
    private int id;
    private String name;    // 学生姓名
    private int age;        // 学生年龄
    private String phone;   // 手机号
    private String gender;  // 性别
    private String number;  // 学生学号
    private String email;   // 邮箱
    private String headImage;   // 个人头像
    private String badStudent;  // 差生标志；1——正常，0——差生
    private String status;  // 0可用；1毕业；2退学；3正在处理退学； 4正在处理转班
    private String liberaScience;   //1表示文科，0表示理科
    private Classes classes;            //一个学生对应一个班级
    private List<Subject> subjects;      //一个学生对应多门课程
    private int classId;   // 所在班级id
    private int userId;    // 学生的userid
    private int middleScore;   // 中考成绩
    private int gardeId;    //年级表id
    private int enrollmentYear; //入学年份
    private String className;   //班级的中文名称

}
