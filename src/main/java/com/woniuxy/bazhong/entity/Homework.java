package com.woniuxy.bazhong.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * @author lvjiabin
 * 作业表
 */
@Data
public class Homework implements Serializable {
    private int id;             //作业id
    private String name;        //作业名称
    private String publicTime;  // 布置时间
    private String finishTime;  //计划完成时间
    private double totalPoints; //作业总分
    private String type;        //0-课后作业 1-周末作业 2-寒暑假作业
    private int teacherId;      //布置作业老师id
    private int subjectId;      //科目id
    private int classId;    // 班级id
}
