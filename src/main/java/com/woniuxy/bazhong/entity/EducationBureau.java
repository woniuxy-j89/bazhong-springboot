package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiaxin-yang
 * @data 2022/7/21 15:11
 * 教育局表
 */

@Data
public class EducationBureau implements Serializable {
    private int id;      //教育局用户id
    private String name; //教育局用户名称
    private int userId;  //用户id

}
