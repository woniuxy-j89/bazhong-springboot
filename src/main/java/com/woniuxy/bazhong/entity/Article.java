package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lzw
 */
@Data
public class Article implements Serializable {      //上传文章
    private int id;
    private String name;  //文章名称
    private String file;  //文章文件路径
    private String date;    //上传时间
    private String status;  //公开方式（0-本班、1-本校、2-完全显示）
    private int konwledgepointId;   //知识点ID
    private int schoolId;   //学校ID
    private int teacherId;  //教室ID
    private String auditOpinion ; //审核意见
    private String auditStatus; //审核状态
    private Teacher teacher;            //发布老师信息
    private KnowledgePoint knowledgePoint; //发布的知识点信息
}
