package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author WangQi
 * @date 2022/7/21 15:27
 * 知识点实体类
 */
@Data
public class KnowledgePoint implements Serializable {
    private int id;            // 知识点id
    private String name;       // 知识点名字
    private String details;    // 知识点细节
    private int subjectId;     // 科目id
    private String status;     // 是否为重点（1:重点,2:不为重点）
}
