package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by raoyucheng
 * on 2022/7/21 15:14
 */
@Data
public class Reading implements Serializable {
    private Integer id;                     //阅读题id，主键
    private String content;                 //阅读题文章
    private String question;                 //阅读题题目内容
    private String answer;                  //阅读题的学生回答
    private String answerAnalysis;          //阅读题解析
    private Double scoreValue;              //阅读题分数
    private Integer knowledgepointId;       //知识点id，可以从知识点表里找到对应的知识点
    private Integer questiontypesId;        //题型id，可以从题型表里找到对应的题型
}
