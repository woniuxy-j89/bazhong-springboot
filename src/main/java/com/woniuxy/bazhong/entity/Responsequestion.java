package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by raoyucheng
 * on 2022/7/21 15:38
 */
@Data
public class Responsequestion implements Serializable {
    private Integer id;                 //解答题id,主键
    private String content;             //解答题内容
    private String answer;              //解答题的学生回答
    private String answerAnalysis;      //解答题解析
    private Double scoreValue;          //解答题分数
    private Integer knowledgepointId;   //知识点id，可以从知识点表里找到对应的知识点
    private Integer questiontypesId;    //题型id，可以从题型表里找到对应的题型
}
