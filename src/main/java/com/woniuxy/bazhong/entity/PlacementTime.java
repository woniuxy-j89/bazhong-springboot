package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;


/**
 * 分班时间类
 * @author lvjiabin
 * @data 2022/7/21{} 14:54
 */
@Data
public class PlacementTime implements Serializable {
    private int id;             //id
    private String startTime;   //开始分班时间
    private String finishTime;  //结束分班时间
    private int enrollmentYear; //需要分班的入学年份

}
