package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 学生提问表
 * @author liwei
 * @data 2022/7/21{} 15:12
 */
@Data
public class Questions implements Serializable{
    private int id;                     //问题id
    private String questionsContent;    //提问内容
    private String questionsFile;       //问题文件
    private String answerContent;       //回答内容
    private String answerFile;          //回答文件
    private String questionTime;        //提问时间
    private String answerTime;          //回答时间
    private String status;
    private int studentId;              //提问者id
    private int teacherId;              //回答者id
    private String qstatus;              // 问题状态 0：未回答 1：已回答

    private Student student;
}
