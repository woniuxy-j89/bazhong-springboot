package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author jiaxin-yang
 * @data 2022/8/1 17:00
 * 升学率表
 */

@Data
public class EnrolmentRate {
    private int id;                             //id
    private int schoolId;                       //学校id
    private String schoolName;                  //学校名称
    private int number;                         //学校当年参考总人数
    private int keyuniversitiesNum;             //重点本科人数
    private BigDecimal keyuniversitiesScale;    //重点本科比例
    private int generaluniversitiesNum;         //普通本科人数(包括重本)
    private BigDecimal generaluniversitiesScale;//普通本科比例(包括重本)
    private int juniorcollegesNum;              //专科人数
    private BigDecimal juniorcollegesScale;     //专科比例
    private int othersNum;                      //其他人数
    private BigDecimal othersScale;             //其他比例
    private int examinationYear;                //参考年份：同时代表哪一届的学生，如 2019为2019届，即2016级的学生。

}
