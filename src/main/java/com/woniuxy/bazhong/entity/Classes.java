package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 班级表
* @author lzw
*/
@Data
public class Classes implements Serializable {      //班级
    private int id;
    private int code;          //班级的独有code，格式为开班年份+班级号，如2016级1班：201601
    private String name;       //班级名
    private String enrollmentYear;//开班年份，同时代表哪一级的班级，如2016为2016级
    private int number;        //学生人数
    private int schoolId;      //学校id
    private int gradeId;       //年级id
    private String semesterId;    //学期id
    private int type;          //0，高一； 1，文科； 2，理科
    private int headteacherId; //班主任id
    private List<Teacher> teachers;//授课教师的集合
}
