package com.woniuxy.bazhong.entity;

import lombok.Data;

/**
 * @author lvjiabin
 * @data 2020-07-21
 *学生历届考试总分
 */
@Data
public class HighestScore {
    private int id;
    private int studentId;          //学生id

    private double chineseScore;    //语文成绩

    private double mathScore;       //数学成绩

    private double englishScore;    //英语成绩

    private double physicsScore;    //物理成绩

    private double chemistryScore;  //化学成绩

    private double biologyScore;    //生物成绩

    private double politicsScore;   //政治成绩

    private double historyScore;    //历史成绩

    private double geographyScore;  //地理成绩

    private String description;    //对考试的描述

    private double totalPoints;     //学生总分
}
