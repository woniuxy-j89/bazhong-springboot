package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wangqi
 * 上传视频的类
 */
@Data
public class Video implements Serializable {
    private int id;                     // 视频id
    private String  name;               // 视频名字
    private String  file;               // 文件路径
    private String  date;               // 上传时间
    private String status;              // 公开方式：0-本班，1-本校，2-完全显示
    private int knowledgepointId;       // 知识点id
    private int schoolId;               // 学校id
    private int teacherId;              // 上传老师的id
    private String auditStatus;         // 审核状态,0:未审核,1：审核成功,2：审核失败
    private String auditOpinion;        // 审核意见
    private Teacher teacher;            //发布老师信息
    private KnowledgePoint knowledgePoint; //发布的知识点信息
}
