package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiaxin-yang
 * @data 2022/7/21 15:30
 * 答题卡
 */

@Data
public class ExamAnswer implements Serializable {
    private int id; //考试答案表id
    private int examId; //考试试卷id
    private String number;  // 学生学号
    private String grade; //年级
    private String classes; //班级
    private String school; //学校
    private String teacher; //阅卷老师
    private String time; //阅卷时间
    private double gscore; //考试总成绩
    private int status; //试卷批阅状态（0：未批阅；1：部分批阅；2：批阅完成）
    private int exemTopicId; //考试题目表id
    private int studentId; //学生id
    private String answer; //题目答案

}
