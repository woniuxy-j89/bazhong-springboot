package com.woniuxy.bazhong.entity;

import lombok.Data;

@Data
public class DataData {
    private String semester;
    private String startDate;
    private String endDate;
    private String grade;
    private String year;
}
