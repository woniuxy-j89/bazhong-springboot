package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiaxin-yang
 * @data 2022/7/21 15:00
 * 高考成绩表
 */

@Data
public class Collegeentranceexamination implements Serializable {
    private int id;
    private int studentId;       //学生id
    private String type;         //考生类型：1-文科 0-理科
    private double chinese;      //语文成绩
    private double mathematics;  //数学成绩
    private double english;      //英语成绩
    private double synthesize;   //理综（文综）成绩
    private double total;        //总分
    private String enrollType;   //录取学校类别（默认全日制）：0-其他 1-专科 2-本科 3-重本（一本）
    private String enroll;       //录取学校
    private String specialty;    //录取学校专业
    private double enrollscore;  //录取分数
    private int schoolId;        //所属学校id
    private int examinationYear; //参考年份：同时代表哪一届的学生，如 2019为2019届，即2016级的学生。

}
