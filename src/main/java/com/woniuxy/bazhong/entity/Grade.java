package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;


/**
 * @author lvjiabin
 * @data 2020-07-21
 *年级表
 */
@Data
public class Grade implements Serializable {

    private int id; //年级id
    private String name; //年级名称
    private int schoolId; //学校id
}
