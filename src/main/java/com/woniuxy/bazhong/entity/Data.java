package com.woniuxy.bazhong.entity;


import lombok.experimental.Accessors;

import java.io.Serializable;
/**
* @author lzw
* 封装数据类
*/
@lombok.Data
@Accessors(chain = true)
public class Data<Object> implements Serializable {
    private String key;
    private Object value;
}
