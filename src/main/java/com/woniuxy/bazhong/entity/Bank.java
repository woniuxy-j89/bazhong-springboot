package com.woniuxy.bazhong.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * @author lzw
 */
@Data
public class Bank implements Serializable {     //判断题
    private int id;
    private String content;     //题目内容
    private String answer;      //答案
    private String answerAnaysis;   //解析
    private double scoreValue;       //分值
    private int questiontypeId;     //题型id
    private int knowledgepointId;        //知识点id

}
