package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by raoyucheng
 * on 2022/7/21 16:14
 */
@Data
public class School implements Serializable {
    private Integer id;             //学校id，主键
    private String name;            //学校名称
    private String address;     //学校地址
    private String password;    //学校密码
    private String number;      //学校编号
}
