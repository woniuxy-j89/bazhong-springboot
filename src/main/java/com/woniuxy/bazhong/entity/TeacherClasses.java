package com.woniuxy.bazhong.entity;

import lombok.Data;

/**
 * 老师跟班级之间的关系表
 */

@Data
public class TeacherClasses {
    private int teacherId;  //教师id
    private int classesId;  //班级id
    private int subjectId;  //科目id
}
