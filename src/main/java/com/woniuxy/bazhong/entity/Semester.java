package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhangjy
 * 学期实体类
 */
@Data
public class Semester implements Serializable {
    private int id;
    private String status;    // 是否当前学期,0或无值：非当前学期  1：当前学期
    private String code;      //学期，格式为：2018-2019-1 (1-上学期,2-下学期)

}
