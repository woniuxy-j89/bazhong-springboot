package com.woniuxy.bazhong.entity;


import lombok.Data;

import java.io.Serializable;

/**
* @author lzw
* @date
*/
@Data
public class Choice implements Serializable {       //选择题
    private int id;
    private String content;     //题目内容
    private String choiceA;     //选项A
    private String choiceB;     //选项B
    private String choiceC;     //选项C
    private String choiceD;     //选项D
    private String answer;      //答案（以，分割）
    private int type;           //类别ID（0单选，1多选）
    private double scoreValue;  //分值
    private int knowledgepointId;      //知识点ID
    private String answerAnalysis;  //解析
    private int questiontypesId;    //题型ID
}
