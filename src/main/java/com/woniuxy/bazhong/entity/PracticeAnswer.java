package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 这张表记录学生刷过的所有题的答案
 * @author liwei
 * @data 2022/7/21{} 14:57
 */
@Data
public class PracticeAnswer implements Serializable {
    private int id;                 //习题答案
    private String finishedDate;    //完成时间
    private String answer;          //答案
    private int studentId;          //学生id
    private int questiontypesId;    //题型id
    private int topicID;            //题目编号
}
