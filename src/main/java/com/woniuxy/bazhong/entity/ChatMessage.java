package com.woniuxy.bazhong.entity;


import lombok.Data;

import java.io.Serializable;
/**
 * @author lvjiabin
 * @data 2020-07-21
 *历史信息记录表
 */
@Data
public class ChatMessage implements Serializable {
    private int id ; //通知消息id
    private String content ; //消息内容
    private int fromUserId ; //发送消息人id
    private int toUserId; //接收人id
    private String date ; //发送时间
}
