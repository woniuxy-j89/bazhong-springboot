package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiaxin-yang
 * @data 2022/7/21 15:19
 * 考试试卷表
 */

@Data
public class Exam implements Serializable {
    private int id;                //试卷id
    private String name;           //试卷名称
    private String publicTime;     //试卷发布时间
    private String examPeople;      //出题人
    private String date;            //考试时间
    private double totalPoints;    //试卷总分
    private String examDuration;    //考试时长
    private String type;           //考试类型 0-月考 1-期中考试 2-期末考 3-高考
    private String description;    //试卷描述
    private int semesterId;     //学期ID
    private int schoolId;      //所属学校id
    private int userId;          //
    private int subjectId;         //科目id



}
