package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhangjy
 * 科目实体类
 */
@Data
public class Subject implements Serializable {
    private int id;
    private String subject; // 科目名(英文)
    private String name;    // 科目名(中文)
    private String type;    // 科目类型：0-默认学科，语数外 1-文科，政史地 2-理科，理化生
}
