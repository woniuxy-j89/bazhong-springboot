package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 题型表
 * @author liwei
 * @data 2022/7/21{} 15:22
 */
@Data
public class QuestionTypes implements Serializable {
    private int id;         //题型id
    private String type;    //题目类型
    private int subjectId;  //科目id
    private String model;   // 题型模板
}
