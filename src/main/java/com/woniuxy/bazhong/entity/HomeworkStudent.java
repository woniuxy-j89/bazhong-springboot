package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author lvjiabin
 * @data 2020-07-21
 * 作业学生表
 */
@Data
public class HomeworkStudent implements Serializable {
    private int id; //id
    private double score; //本次作业分数
    private String status; // 是否提交作业 0-未提交 1-已提交
    private int homeworkId; //作业id
    private int studentId; //学生id

    private String isExpired;   // 是否过期（不是数据库表的字段）；yes——已经过期，no——未过期，null——未给该属性赋值

    private Homework homework;  // 学生对应的作业
    private List<HomeworkTopic> homeworkTopics; // 一个作业有多道题目
//    private List<HomeworkAnswer> homeworkAnswers; // 学生提交的每道题目的答案；所以一个作业有多个题目及学生提交的答案
}
