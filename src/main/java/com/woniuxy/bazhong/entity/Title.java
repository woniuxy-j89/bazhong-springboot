package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhangjy
 * 作文实体类
 */
@Data
public class Title implements Serializable {
    private int id; // 作文题id
    private String content; // 题目内容
    private String answerAnalysis; // 解析
    private double scoreValue;  // 所得分数
    private int knowledgepointId;    // 知识点id
    private int questiontypesId;    // 题型id
}
