package com.woniuxy.bazhong.entity;

import lombok.Data;
/**
* @author lzw
* 封装各个分段对应的人数
*/

@Data
public class ScoreOfNums {
    int numUnderSpecialty;//未升学人数
    int numSpecialty;//专科人数
    int numRegular;//本科人数
    int numHeavy;//重本人数
}