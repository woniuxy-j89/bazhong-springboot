package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiaxin-yang
 * @data 2022/7/21 15:00
 * 错题库
 */

@Data
public class ErrorsQuestion implements Serializable {
    private int id;               //错题库id
    private int correctNumber;    //做对次数
    private int errorsNumber;     //做错次数
    private int questiontypesId;  //题型id
    private int topicId;          //题目id
    private int studentId;        //学生id

}
