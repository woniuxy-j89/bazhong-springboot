package com.woniuxy.bazhong.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author WangQi
 * @date 2022/7/21 15:17
 * 用户类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class User implements UserDetails, Serializable  {
    private int id;                 //用户ID
    private String account;         // 账号
    private String phone;           // 手机号
    private String password;        // 密码
    private int schoolId;           // 所在学校id
    private String status;            // 账号状态：1可用，0不可用

    private List<Role> roles;       //一个用户拥有多个角色


    //返回当前用户的所有角色、权限信息： 所有角色权限信息封装成GrantedAuthority类对象
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        //遍历角色、权限信息进行封装
        for(Role role : roles){
            //封装角色信息
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
            //遍历权限信息进行封装
            for(Perms perms : role.getPerms()){
                grantedAuthorities.add(new SimpleGrantedAuthority(perms.getName()));
            }
        }
        log.info("权限信息：" + grantedAuthorities);
        return grantedAuthorities;
    }

    @Override
    public String getUsername() {

        return this.account;
    }


    // 当前用户的密码是否没有过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }


    // 当前用户是否没有被锁定
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    // 当前用户的密码是否没有过期
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // 当前用户是否可用
    @Override
    public boolean isEnabled() {
        return true;
    }
}
