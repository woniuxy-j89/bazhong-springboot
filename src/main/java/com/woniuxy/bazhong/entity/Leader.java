package com.woniuxy.bazhong.entity;

import lombok.Data;

import java.io.Serializable;
/**
 * @author WangQi
 * @date 2022/7/21 15:32
 * 校长表
 */
@Data
public class Leader implements Serializable {
    private int id;                 // 校长id
    private String account;         //校长账号
    private String password;        //校长密码
    private String name;            // 校长姓名
    private String phone;           // 手机号
    private String graduate;        // 校长毕业院校
    private String graduateTime;    // 校长毕业时间
    private String gender;          // 校长性别
    private int schoolId;           // 负责的学校id


}
