package com.woniuxy.bazhong.utils;

import lombok.Data;

@Data
public class PageMessage<T> {
    private int currentPage;    // 当前页
    private int size;   // 每一页的大小
    private int totalPage;  // 总页数
    private long total; // 总记录数；mybatis分页插件的总记录数就long类型
    private T data; // 数据
}
