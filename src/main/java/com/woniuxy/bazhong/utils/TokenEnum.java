package com.woniuxy.bazhong.utils;

/**
 * 主要定义校验token时的三种状态：过期、伪造、成功
  */

public enum TokenEnum {
    TOKEN_EXPIRE,TOKEN_BAD,TOKEN_SUCCESS
}