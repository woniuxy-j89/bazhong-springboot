package com.woniuxy.bazhong.utils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {
    static public boolean sendEmail(String to, String from) {

        // 指定发送邮件的服务器
        String host = "smtp.qq.com";   //发件人使用发邮件的电子信箱服务器

        // 获取系统属性
        Properties props = System.getProperties();

        // Setup mail server
        props.put("mail.smtp.host", host);

        // Get session
        props.put("mail.smtp.auth", "true"); //这样才能通过验证

        MyAuthenticator myauth = new MyAuthenticator("949606970@qq.com", "znvxnbydstngbbea");
        Session session = Session.getDefaultInstance(props, myauth);


        try {
            // 创建默认的 MimeMessage 对象
            MimeMessage message = new MimeMessage(session);

            // Set From: 头部头字段
            message.setFrom(new InternetAddress(from));

            // Set To: 头部头字段
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));

            // Set Subject: 头部头字段
            message.setSubject("消息提醒!");

            // 设置消息体
            message.setText("密码已重置！！");

            // 发送消息
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
            return false;
        }

        return true;
    }
}