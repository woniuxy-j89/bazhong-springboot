package com.woniuxy.bazhong.utils;

import lombok.Data;

@Data
public class ResponseResult <T> {
    private int code;       // 状态码：自定义的，不同的码表示不同的含义，具体可以参考http状态码
                            // 200-成功  401-没权限   403-没登录   400-参数有问题  500-后台出了问题
    private ResponseStatus status;  // 响应状态：主要是为了方便前端程序员根据此属性的值进行判断，做下一步的处理

    private String message; // 后台返回的消息，例如：你还没登录，没权限操作，服务器正忙请稍后再试，账号或者密码错误......

    private T data;         // 返回的数据
}
