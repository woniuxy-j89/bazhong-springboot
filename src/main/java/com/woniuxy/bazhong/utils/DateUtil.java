package com.woniuxy.bazhong.utils;

import com.sun.org.apache.bcel.internal.generic.RETURN;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
/**
 * @author WangQi
 * @date 2022/7/21 20:46
 * 这是获取当前时间的工具类
 */
public class DateUtil {
    public static String getDate(){
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        return sdf.format(d);
    }

}
