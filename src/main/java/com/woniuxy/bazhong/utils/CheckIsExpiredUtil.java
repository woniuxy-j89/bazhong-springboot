package com.woniuxy.bazhong.utils;

import com.woniuxy.bazhong.entity.HomeworkStudent;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * 用来判断Homework是否过期，并且将判断结果赋值给对应的HomeworkStudent属性ixExpired
 * @author zhangjy
 * @date 2022/7/29 15:49
 */
@Slf4j
public class CheckIsExpiredUtil {

    // 判断作业学生是否过期，并设置isExpired属性
    public static HomeworkStudent checkAndSetIsExpired(HomeworkStudent homeworkStudent) {
        // 判断每个作业是否过期
        try {
            // 获取该作业截止时间
            String str_finishTime = homeworkStudent.getHomework().getFinishTime();
//            log.info("数据库存储的截止时间：" + str_finishTime);

            // 指定时间格式
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // 设置时区
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
            // 将作业截止时间转换为date格式
            Date finishTime = sdf.parse(str_finishTime);
//            log.info("Date格式的截止时间：" + finishTime);

            Date date = new Date();
//            log.info("Date格式——当前时间：" + date);

            // 判断作业是否过期
            if (finishTime.before(date)) {  // 过期
                // 设置isExpired属性值
                homeworkStudent.setIsExpired("yes");
            } else {    // 没有过期
                // 设置isExpired属性值
                homeworkStudent.setIsExpired("no");
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        return homeworkStudent;
    }

}
