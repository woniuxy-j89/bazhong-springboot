package com.woniuxy.bazhong.utils;

public enum ResponseStatus {
    // 每一个常量对应一种状态
    NO_LOGIN,NO_PERMS,SUCCESS,FAIL,LOGIN_SUCCESS
}
