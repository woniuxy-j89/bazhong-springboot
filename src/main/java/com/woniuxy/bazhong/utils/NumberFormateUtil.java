package com.woniuxy.bazhong.utils;

import java.text.NumberFormat;

/**
 * @author: lzw
 * @description: 格式化数据
 */
public class NumberFormateUtil {
    public static double getNumFormate(int num1,int num2) {
        NumberFormat format=NumberFormat.getInstance();
        format.setMaximumFractionDigits(2);
        String result=format.format(((double) num1)/((double) num2)*100);
        return Double.parseDouble(result);
    }
}