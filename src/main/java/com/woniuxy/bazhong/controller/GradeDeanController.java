package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.service.*;
import com.woniuxy.bazhong.utils.ResponseStatus;
import com.woniuxy.bazhong.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/GradeDean")
//@CrossOrigin
public class GradeDeanController {

    /**
     * 教师管理的service层类
     */
    @Resource
    private TeacherManageService teacherManageService;

    /**
     * 班级管理的service层类
     */
    @Resource
    private ClassesManageService classesManageService;

    /**
     * 各校同级数据对比模块service
     */
    @Resource
    private SchoolCEEDataService schoolCEEDataService;

    /**
     * 知识点的service层类
     */
    @Resource
    private KnowledgePointService knowledgePointService;

    @Resource
    private ArticleService articleService;
    /**
     * video的service层类
     */
    @Resource
    private VideoService videoService;
    /**
     * User的service层类
     */
    @Resource
    private UserService userService;
    /**
     * PlacementTime的service层类
     */
    @Resource
    private PlacementTimeService placementTimeService;
    /**
     * chatMessage的service层类
     */
    @Resource
    private ChatMessageService chatMessageService;


    /**
     * HistoryScore的service层类
     * 关于历史成绩的
     */
    @Resource
    private HistoryScoreService historyScoreService;

    /**
     * ScoresScore的service层类
     *
     */
    @Resource
    private ScoresService scoresService;

    @Resource
    private ExamService examService;

    @Resource
    private ClassesService classesService;
    /**
     * 查询展示所有老师
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    @GetMapping("/allTeacher/{page}/{size}")
    public ResponseResult<PageMessage<List<Teacher>>> allTeacher(@PathVariable("page")int page, @PathVariable("size")int size){
        PageMessage<List<Teacher>> all = teacherManageService.allTeacher(page,size);
        ResponseResult<PageMessage<List<Teacher>>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(all);

        return responseResult;
    }

    /**
     * 查询展示科目对应的授课老师
     * @param subjectId 科目id
     * @return
     */
    @GetMapping("/teacherOfSubject/{subjectId}")
    public ResponseResult<List<Teacher>> teacherOfSubject(@PathVariable("subjectId")int subjectId){
        List<Teacher> all = teacherManageService.teacherOfSubject(subjectId);
        log.info(all.toString());
        ResponseResult<List<Teacher>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(all);

        return responseResult;
    }

    /**
     * 教师管理，根据教师姓名，查询展示教师当前学期所带班级
     * @param teacherName 教师姓名
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    @GetMapping("/classessOfTeacher/{teacherName}/{page}/{size}")
    public ResponseResult<PageMessage<List<Classes>>> classessOfTeacher(@PathVariable("teacherName")String teacherName,@PathVariable("page")int page, @PathVariable("size")int size){
        PageMessage<List<Classes>> all = teacherManageService.classessOfTeacher(teacherName,page,size);
        ResponseResult<PageMessage<List<Classes>>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(all);

        return responseResult;
    }

    /**
     * 班级管理中，查询展示当前学期的所有班级
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    @GetMapping("/allClasses/{page}/{size}")
    public ResponseResult<PageMessage<List<Classes>>> allClasses(@PathVariable("page")int page, @PathVariable("size")int size){
        PageMessage<List<Classes>> all = classesManageService.allClasses(page,size);
        ResponseResult<PageMessage<List<Classes>>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(all);

        return responseResult;
    }

    /**
     * 授课教师安排中，根据所选班级的班级类型查询展示学科
     * @param classesId 班级id
     * @return
     */
    @GetMapping("/subOfClasses/{classesId}")
    public ResponseResult<List<Subject>> subOfClasses(@PathVariable("classesId")int classesId){
        List<Subject> subjects = classesManageService.subOfClasses(classesId);
        ResponseResult<List<Subject>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(subjects);

        return responseResult;
    }

    /**
     * 授课教师安排中，安排各学科授课教师或教师账号管理中给老师添加班级
     * @param teacherClasses 所要添加班级的信息
     */
    @PostMapping("/addSubTeacherOfClasses")
    public ResponseResult<Boolean> addSubTeacherOfClasses(@RequestBody TeacherClasses teacherClasses){
        Boolean b = classesManageService.addsubTeacherOfClasses(teacherClasses);

        ResponseResult<Boolean> responseResult=new ResponseResult<>();

        if(b){

            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("添加授课老师成功");
            responseResult.setData(b);
        } else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("添加授课老师失败，请检查所选班级或教师信息是否有误，稍后再试");
            responseResult.setData(b);
        }

        return responseResult;
    }

    /**
     * 班主任任命中，根据所选班级，展示所有任课教师
     * @param classesId 班级id
     * @return Classes班級
     */
    @GetMapping("/allSubTeacherOfClasses/{classesId}")
    public ResponseResult<Classes> allSubTeacherOfClasses(@PathVariable("classesId")int classesId){
        Classes classes = classesManageService.allSubTeacherOfClasses(classesId);
        ResponseResult<Classes> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(classes);

        return responseResult;
    }

    /**
     * 班主任任命中，任命或更换班主任
     * @param classesId 班级id
     * @param teacherId 老师id
     */
    @PutMapping("/updateHTeacherOfClasses/{ClassesId}/{TeacherId}")
    public ResponseResult<Boolean> updateHTeacherOfClasses(@PathVariable("ClassesId")int classesId, @PathVariable("TeacherId")int teacherId){
        Boolean b = classesManageService.updateHTeacherOfClasses(classesId,teacherId);

        ResponseResult<Boolean> responseResult=new ResponseResult<>();

        if(b){

            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("任命班主任成功");
            responseResult.setData(b);
        } else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("任命班主任成功，请检查所选班级或教师信息是否有误，稍后再试");
            responseResult.setData(b);
        }

        return responseResult;
    }

    /**
     * 班级管理中，创建添加班级
     * @param classes 所要添加班级的信息
     */
    @PostMapping("/addClasses")
    public ResponseResult<Boolean> allClasses(@RequestBody Classes classes){
        log.info(classes.toString());
        Boolean b = classesManageService.addClasses(classes);

        ResponseResult<Boolean> responseResult=new ResponseResult<>();

        if(b){

            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("创建班级成功");
            responseResult.setData(b);
        } else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("创建班级失败，请检查班级信息是否有误，稍后再试");
            responseResult.setData(b);
        }

        return responseResult;
    }

    /**
     * 班级管理中，修改班级类型
     * @param classes 所要修改班级的信息
     */
    @PutMapping("/updateClassesType")
    public ResponseResult<Boolean> updateClassesType(@RequestBody Classes classes){
        Boolean b = classesManageService.updateClassesType(classes);

        ResponseResult<Boolean> responseResult=new ResponseResult<>();

        if(b){

            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("修改班级类型成功");
            responseResult.setData(b);
        } else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("修改班级类别失败，请检查修改信息是否有误，稍后再试");
            responseResult.setData(b);
        }

        return responseResult;
    }

    /**
     * 班级管理中，更换班主任
     * @param classes 所要修改班级的信息
     */
    @PutMapping("/updateClassesHT")
    public ResponseResult<Boolean> updateClassesHT(@RequestBody Classes classes){
        Boolean b = classesManageService.updateClassesHT(classes);

        ResponseResult<Boolean> responseResult=new ResponseResult<>();

        if(b){

            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("更换班主任成功");
            responseResult.setData(b);
        } else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("更换班主任失败，请检查修改信息是否有误，稍后再试");
            responseResult.setData(b);
        }

        return responseResult;
    }

    /**
     * 教师账号管理中，重置教师密码
     * @param teacher 所要修改班级的信息
     */
    @PutMapping("/resetTeacherPassword")
    public ResponseResult<Boolean> resetTeacherPassword(@RequestBody Teacher teacher){
        log.info(teacher.toString());
        Boolean b = teacherManageService.resetTeacherPassword(teacher);

        ResponseResult<Boolean> responseResult=new ResponseResult<>();

        if(b){

            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("重置密码成功");
            responseResult.setData(b);
        } else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("重置密码失败，服务器正忙，稍后再试");
            responseResult.setData(b);
        }

        return responseResult;
    }

    /**
     * 教师账号管理下，重新指定班級中刪除所选班级
     * @param teacherClasses 所要修改班级的信息
     */
    @PostMapping ("/delClassesOfTeacher")
    public ResponseResult<Boolean> delClassesOfTeacher(@RequestBody TeacherClasses teacherClasses){
        Boolean b = teacherManageService.delClassesOfTeacher(teacherClasses);

        ResponseResult<Boolean> responseResult=new ResponseResult<>();

        if(b){

            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("删除班级成功");
            responseResult.setData(b);
        } else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("删除班级失败，服务器正忙，稍后再试");
            responseResult.setData(b);
        }

        return responseResult;
    }

    /**
     * 教师账号管理中，添加班级下，没有指定老师对应课程的老师的班级
     * @param teacher 指定老师信息
     * @return Classes 班級
     */
    @PostMapping("/nullSubTeacherOfClasses/")
    public ResponseResult<List<Classes>> nullSubTeacherOfClasses(@RequestBody Teacher teacher){
        List<Classes> classess = teacherManageService.nullSubTeacherOfClasses(teacher);
        ResponseResult<List<Classes>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(classess);

        return responseResult;
    }
    /**
     * 各校同级数据对比模块，展示学校列表
     * @return List<School>学校列表
     */
    @GetMapping("/allSchool")
    public ResponseResult<List<School>> allSchool(){
        List<School> schools = schoolCEEDataService.allSchool();
        ResponseResult<List<School>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(schools);

        return responseResult;
    }

    /**
     * 各校同级数据对比模块，展示升学率
     * @param schoolId 学校id
     * @return List<EnrolmentRate>
     */
    @GetMapping("/showEnrolmentRate/{schoolId}")
    public ResponseResult<List<EnrolmentRate>> showEnrolmentRate(@PathVariable("schoolId")int schoolId){
        List<EnrolmentRate> enrolmentRates = schoolCEEDataService.showEnrolmentRate(schoolId);
        ResponseResult<List<EnrolmentRate>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(enrolmentRates);

        return responseResult;
    }

    //知识点文档上传
    @PostMapping ("/knowledge/add")
    @ResponseBody

    public ResponseResult<Boolean> addKnowledgePoint(@RequestParam("filename") MultipartFile file){

        boolean flag = false;

        String fileName = file.getOriginalFilename();
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            flag = knowledgePointService.addKnowledgePoint(fileName,file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("上传知识点成功");
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("上传知识点失败");

        }
        return responseResult;
    }

    /**
     * *
     * 查询审核视频
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    @GetMapping("/video/toaudit/{page}/{size}")
    public ResponseResult<PageMessage<List<Video>>> findToAuditVideo(@PathVariable("page")int page, @PathVariable("size")int size){

        //从security上下文中获取用户信息
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        int school_id = user.getSchoolId();

        PageMessage<List<Video>> all = videoService.findToAudit(page,size,school_id);
        ResponseResult<PageMessage<List<Video>>> responseResult = new ResponseResult<>();

        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(all);

        log.debug(all.toString());
        return responseResult;
    }
    /**
     * *
     * 审核视频
     * @param status 判断审核是否通过
     * @param video 审核的是那一条视频--附带审核意见
     * @return
     */
    @PutMapping("/video/audit/{status}")
    public ResponseResult<Boolean> AuditVideo(@PathVariable("status")String status,@RequestBody Video video){
        ResponseResult<Boolean> responseResult = new ResponseResult<>();
        video.setAuditStatus(status);
        boolean result = videoService.updateStatus(video);

        if(result){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("审核视频完毕");
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("审核视频失败");
        }
        return responseResult;
    }
    /**
     * *
     * 查询审核文章
     * 是否可以获取到id属性值 如果不能 用SQL用name来判断
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    @GetMapping("/article/toaudit/{page}/{size}")
    public ResponseResult<PageMessage<List<Article>>> findToAuditArticle(@PathVariable("page")int page, @PathVariable("size")int size){

        //从security上下文中获取用户信息
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        int school_id = user.getSchoolId();
        ResponseResult<PageMessage<List<Article>>> responseResult = new ResponseResult<>();

        PageMessage<List<Article>> all = articleService.findToAudit(page,size,school_id );

        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(all);
        log.debug(all.toString());

        return responseResult;
    }

    /**
     * *
     * 审核文章
     * 是否可以获取到id属性值 如果不能 用SQL用name来判断
     * @param status 判断审核是否通过
     * @param article 审核的是那一条视频--附带审核意见
     * @return
     */
    @PutMapping("/article/audit/{status}")
    public ResponseResult<Boolean> AuditArticle(@PathVariable("status")String status,@RequestBody Article article){
        ResponseResult<Boolean> responseResult = new ResponseResult<>();
        article.setAuditStatus(status);
        boolean result = articleService.updateStatus(article);

        if(result){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("审核文章完毕");
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("审核文章失败");
        }
        return responseResult;
    }
    /**
     * *
     * 指定分班时间
     * @param placementTime 分班时间 加年级
     * @return
     */
    @PostMapping("/placementTime")
    public ResponseResult<String> checkPlacementTime(@RequestBody PlacementTime placementTime){
        ResponseResult<String> responseResult = new ResponseResult<>();
        log.debug(placementTime.toString());
        boolean result = placementTimeService.addTime(placementTime);
        String message = "分班开始了！"+placementTime.getEnrollmentYear()+"届学生,"
                +"从"+placementTime.getStartTime()+"开始，"+placementTime.getFinishTime()+"结束！";
        if(result){
            //查询需要接收消息的用户 当届的学生和所有老师
            //查询为老师的用户 根据学校id 暂时为1 后面可以登录后 改为学校id
            //从security上下文中获取用户信息
            User user1 = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            int school_id = user1.getSchoolId();

            List<User> teachers =  userService.findTeacherUserBySchool(school_id);
            List<User> students = userService.findPlacementStudent(placementTime.getEnrollmentYear(),school_id);

            //将老师学生合并
            List<User> all = new ArrayList<>();
            all.addAll(teachers);
            all.addAll(students);
            ChatMessage chatMessage = new ChatMessage();
            for (User user : all){
                for(String account: WebSocketUtil.MESSAGEMAP.keySet()){
                    //向所有在线的学生老师发送分班信息
                    if(account.equals(user.getAccount())){
                        //根据token 写出是谁发的消息
                        WebSocketUtil.sendMessage(WebSocketUtil.MESSAGEMAP.get(account),message);
                    }
                }
                chatMessage.setContent(message);
                chatMessage.setToUserId(user.getId());
                chatMessage.setFromUserId(user1.getId());

                chatMessage.setDate(DateUtil.getDate());
                boolean flag = chatMessageService.addMessage(chatMessage);
            }
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("设置分班时间成功");
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("设置分班时间失败");
        }
        return responseResult;
    }



    /**
     * *
     * 班级学生数据
     * @param exam 考试名称
     * @return 那场考试的所有班级的成绩集合
     */
    @GetMapping("/class/classScore/{exam}")
    public ResponseResult<List<HistoryScore>> classStudentData(@PathVariable("exam")String exam){
        ResponseResult<List<HistoryScore>> responseResult = new ResponseResult<>();

        List<HistoryScore> list = historyScoreService.findAllScoreByName(exam);

        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(list);

        return responseResult;
    }

    /**
     * *
     * 班级学生数据  年级学生数据
     * @param classes 年级名称 哪一届 高几 几班 或 或者 那一级 高几
     * @param subject 考试科目
     * @return 那场考试的所有班级的成绩集合
     */
    @GetMapping("/class/classScore/{classes}/{subject}")
    public ResponseResult<List<HistoryScore>> classesHistoryScore(@PathVariable("classes")String classes,@PathVariable("subject")String subject){
        ResponseResult<List<HistoryScore>> responseResult = new ResponseResult<>();
        User user1 = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user1.getSchoolId();
        List<HistoryScore> list = historyScoreService.findAllScoreByClass(classes,subject,schoolId);
        log.debug(list.toString());
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(list);
        return responseResult;
    }
    /**
     * *
     * 学生排名分布
     * @param classes 年级名称 哪一届 高几 几班 或 或者 那一级 高几
     * @param exam 考试信息 那一场考试 或者 那一轮考试
     * @return 那场考试的所有班级的成绩集合
     */
    @GetMapping("/rank/studentRank/{classes}/{exam}")
    public ResponseResult<Map<String,Integer>> studentRank(@PathVariable("classes")String classes, @PathVariable("exam")String exam){
        ResponseResult<Map<String,Integer>> responseResult = new ResponseResult<>();
        User user1 = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user1.getSchoolId();
        Map<String,Integer> map = scoresService.studentRank(classes,exam,schoolId);
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(map);


        return responseResult;
    }

    /**
     * *
     * 查询学校所有的考试
     * @return 返回考试信息
     */
    @GetMapping("/exam")
    public ResponseResult<List<Exam>> findAllExamBySchool(){
        ResponseResult<List<Exam>> responseResult = new ResponseResult<>();
        User user1 = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user1.getSchoolId();
        List<Exam> list = examService.findAllExamBySchool(schoolId);
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(list);


        return responseResult;
    }
    /**
     * *
     * 查询学校所有的班级
     * @return 返回考试信息
     */
    @GetMapping("/findallclass")
    public ResponseResult<List<String>> findAllClassBySchool(){
        ResponseResult<List<String>> responseResult = new ResponseResult<>();
        User user1 = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user1.getSchoolId();
        List<String> list = classesService.findAllClassBySchool(schoolId);
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(list);
        return responseResult;
    }
    /**
     * *
     * 学生排名分布
     * @param classes 年级名称 哪一届 高几 几班 或 或者 那一级 高几
     * @param exam 考试信息 那一场考试 或者 那一轮考试
     * @return 那场考试的所有班级的成绩集合
     */
    @GetMapping("/studentScore/{classes}/{exam}/{page}/{size}")
    public ResponseResult<PageMessage<List<Scores>>> studentScore(@PathVariable("classes")String classes, @PathVariable("exam")String exam,@PathVariable("page")int page, @PathVariable("size")int size){
        ResponseResult<PageMessage<List<Scores>>> responseResult = new ResponseResult<>();
        User user1 = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int schoolId = user1.getSchoolId();
        PageMessage<List<Scores>> list = scoresService.findAllScoreByclass(classes,exam,schoolId,page,size);
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(list);


        return responseResult;
    }

    @GetMapping("/findKnow/{page}/{size}")
    public ResponseResult<PageMessage<List<KnowledgePoint>>> findKnow(@PathVariable("page")int page, @PathVariable("size")int size){
        ResponseResult<PageMessage<List<KnowledgePoint>>> responseResult = new ResponseResult<>();

        PageMessage<List<KnowledgePoint>> list = knowledgePointService.findKnow(page,size);
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(list);

        return responseResult;
    }

    @GetMapping("/placementTime")
    public ResponseResult<PlacementTime> findTime(){
        ResponseResult<PlacementTime> responseResult = new ResponseResult<>();

       PlacementTime placementTime=placementTimeService.findPlacementTime();

        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(placementTime);

        return responseResult;
    }
}




