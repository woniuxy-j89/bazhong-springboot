package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.entity.School;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.service.SchoolService;
import com.woniuxy.bazhong.utils.PageMessage;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* @author lzw
*/
@RestController
@RequestMapping("/school")
public class SchoolController {

    @Resource
    private SchoolService schoolService;

    /**
     * 创建学校账号并创建User账号并指定权限
     * @param school
     * @return
     */
    @PostMapping("/create")
    public ResponseResult<Void> createSchool(@RequestBody School school){
        ResponseResult<Void> responseResult = new ResponseResult<>();
        Integer maxId = schoolService.findMaxId();
        String number = "";
        if(maxId == null){
            number = "S1000";
            school.setNumber(number);
        }else{
            number = "S" + (Integer.parseInt(schoolService.findNumber(maxId).substring(1)) +1 );
            school.setNumber(number);
        }
        int res = schoolService.createSchool(school);
        if(res>0){
            responseResult.setMessage("创建学校成功");
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setCode(200);
            School school1 = schoolService.findSchoolById(schoolService.findMaxId());
            //创建User账号
            User user  = new User();
            user.setAccount(school1.getNumber());
            user.setPassword(school1.getPassword());
            user.setSchoolId(school1.getId());
            user.setStatus("1");
            schoolService.add(user);
            int id = schoolService.findMaxUserId();
            //创建User_Role权限
            schoolService.addUserRole(id);
        }
        return responseResult;
    }

    /**
     * 分页展示查询
     * @param page  页码
     * @param size  每页展示数据条数
     * @return
     */
    @GetMapping("/findSchools/{page}/{size}")
    public ResponseResult<PageMessage<List<School>>> findSchools(@PathVariable("page") int page, @PathVariable("size") int size){
        PageMessage<List<School>> schools = schoolService.findSchools(page, size);
        ResponseResult<PageMessage<List<School>>> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(schools);

        return responseResult;
    }
    @GetMapping("/findByNumber/{schoolNumber}")
    public ResponseResult<School> findByNumber(@PathVariable("schoolNumber") String schoolNumber){
        School school = schoolService.findByNumber(schoolNumber);
        ResponseResult<School> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(school);

        return responseResult;
    }
}
