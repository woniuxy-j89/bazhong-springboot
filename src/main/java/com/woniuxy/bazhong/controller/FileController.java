package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.QuestionTypes;
import com.woniuxy.bazhong.service.FileService;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
//import org.jetbrains.annotations.NotNull;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@Slf4j
@RequestMapping("/file")
public class FileController {

    @Resource
    private FileService fileService;

    @Value("${filedir}")
    private String filedir;

    /**
     * 上传视频
     * @param file
     * @param request
     * @return ResponseResult<String>
     */
    @PostMapping("/uploadVideo")
    public @ResponseBody ResponseResult<String> uploadVideo(@RequestParam("filename") MultipartFile file, HttpServletRequest request){
        ResponseResult<String> responseResult = new ResponseResult<>();
        if(!file.isEmpty()){
            String uploadPath = filedir + "video";
            // 如果目录不存在则创建
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdirs();
            }
            String fileName = file.getOriginalFilename();//获取原文件名
            // String fileName = UUID.randomUUID().toString().replace("-","") + OriginalFilename.substring(OriginalFilename.lastIndexOf("."));

            // 得到文件的路径
            String filePath = "video" + File.separator + fileName;

            File localFile = new File(filePath);
            try {
                FileUtils.copyInputStreamToFile(file.getInputStream(), localFile); //把上传的文件保存至本地
                fileService.uploadVideo(filePath, fileName);
                // 成功
                responseResult.setCode(200);
                responseResult.setStatus(ResponseStatus.SUCCESS);
                responseResult.setMessage("上传成功");
                // responseResult.setData("video/" + fileName);
                return responseResult;//上传成功，返回保存的文件地址
            }catch (IOException e){
                e.printStackTrace();
                // 失败
                responseResult.setCode(500);
                responseResult.setStatus(ResponseStatus.FAIL);
                responseResult.setMessage("上传失败");
                return responseResult;
            }
        }else{
            System.out.println("文件为空");
            return responseResult;
        }
    }

    /**
     * 下载对应题目类型上传的模板
     * @param subjectId
     * @param type
     * @return
     */
    @GetMapping("/downloadModel/{subjectId}/{type}")
    public ResponseResult<String> downloadModel(@PathVariable("subjectId")int subjectId, @PathVariable("type")String type){
        ResponseResult<String> responseResult = new ResponseResult<>();
        String modelPath = fileService.findModel(subjectId, type);
        if(modelPath != null){
            // 成功
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("下载传功");
            responseResult.setData(modelPath);
        }else{
            // 失败
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("下载失败");
        }
        return responseResult;
    }

    @GetMapping("/findTypeBySid/{subjectId}")
    public ResponseResult<List<QuestionTypes>> findTypeBySid(@PathVariable("subjectId")int subjectId){
        ResponseResult<List<QuestionTypes>> responseResult = new ResponseResult<>();
        List<QuestionTypes> list = fileService.findTypeBySid(subjectId);
        if(list != null){
            // 成功
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("查询成功");
            responseResult.setData(list);
        }else{
            // 失败
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("查询失败");
        }
        return responseResult;
    }

    /**
     * 上传选择题
     * @param file
     * @return
     */
    @PostMapping ("/exam/choice/{examId}")
    public @ResponseBody ResponseResult<Boolean> uploadChoice(@RequestParam("filename") MultipartFile file,@PathVariable("examId") String examId, HttpServletRequest request){
        System.out.println(file.getOriginalFilename());
        System.out.println(examId);
        boolean flag = false;

        String fileName = file.getOriginalFilename();
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            flag = fileService.uploadChoice(fileName,file,Integer.parseInt(examId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("上传选择题成功");
            // 暂时先不返回文档
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("上传选择题失败");

        }
        return responseResult;
    }

    /**
     * 上传解答题
     * @param file
     * @return
     */
    /*@PostMapping ("/exam/responsequestion")
    public @ResponseBody ResponseResult<Boolean> uploadResponsequestion(@RequestParam MultipartFile file, HttpServletRequest request){

        boolean flag = false;

        String fileName = file.getOriginalFilename();
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            flag = fileService.uploadResponsequestion(fileName,file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("上传选择题成功");
            // 暂时先不返回文档
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);33
            responseResult.setMessage("上传选择题失败");

        }
        return responseResult;
    }*/

    /**
     * 上传阅读题
     * @param file
     * @param request
     * @return
     */
    @PostMapping ("/exam/reading/{examId}")
    public @ResponseBody ResponseResult<Boolean> uploadReading(@RequestParam("filename") MultipartFile file,@PathVariable("examId") String examId, HttpServletRequest request){

        boolean flag = false;

        String fileName = file.getOriginalFilename();
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            flag = fileService.uploadReading(fileName,file,Integer.parseInt(examId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("上传阅读题成功");
            // 暂时先不返回文档
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("上传阅读题失败");

        }
        return responseResult;
    }

    /**
     * 上传作文题
     * @param file
     * @param request
     * @return
     */
    @PostMapping ("/exam/title/{examId}")
    public @ResponseBody ResponseResult<Boolean> uploadTitle(@RequestParam("filename") MultipartFile file, @PathVariable("examId")String examId, HttpServletRequest request){

        boolean flag = false;

        String fileName = file.getOriginalFilename();
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            flag = fileService.uploadTitle(fileName,file,Integer.parseInt(examId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("上传作文题成功");
            // 暂时先不返回文档
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("上传作文题失败");

        }
        return responseResult;
    }

    /**
     * 上传填空题
     * @param file
     * @param request
     * @return
     */
    @PostMapping ("/exam/blank/{examId}")
    public @ResponseBody ResponseResult<Boolean> uploadBlank(@RequestParam("filename") MultipartFile file, @PathVariable("examId") String examId, HttpServletRequest request){

        boolean flag = false;

        String fileName = file.getOriginalFilename();
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            flag = fileService.uploadBlank(fileName,file,Integer.parseInt(examId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("上传填空题成功");
            // 暂时先不返回文档
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("上传填空题失败");

        }
        return responseResult;
    }

}