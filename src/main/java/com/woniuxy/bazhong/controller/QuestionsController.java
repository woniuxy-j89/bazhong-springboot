package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.HomeworkStudent;
import com.woniuxy.bazhong.entity.KnowledgePoint;
import com.woniuxy.bazhong.entity.Questions;
import com.woniuxy.bazhong.service.KnowledgePointService;
import com.woniuxy.bazhong.service.QuestionsService;
import com.woniuxy.bazhong.utils.PageMessage;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhangjy
 * @date 2022/8/2 22:51
 */
@Slf4j
@RestController
@RequestMapping("/questions")
public class QuestionsController {

    @Resource
    private QuestionsService questionsService;

    /**
     * 学生对一个知识点提问
     * @param studentId
     * @param knowledgePointId
     * @param questionsContent
     * @param status
     * @return
     */
    @PostMapping("/addByKnowledgePoint/{student_id}/{knowledge_point_id}/{questions_content}/{status}")
//    public ResponseResult<Boolean> addByKnowledgePoint(@RequestParam("student_id") int studentId, @RequestParam("knowledge_point_id") int knowledgePointId, @RequestParam("questions_content") String questionsContent, @RequestParam("status") String status) {
    public ResponseResult<Boolean> addByKnowledgePoint(@PathVariable("student_id") int studentId, @PathVariable("knowledge_point_id") int knowledgePointId, @PathVariable("questions_content") String questionsContent, @PathVariable("status") String status) {

        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        log.info("学生id：" + studentId + "...知识点id：" + knowledgePointId + "...提问内容：" + questionsContent + "...公开状态：" + status);

        boolean flag = questionsService.addByKnowledgePoint(studentId, knowledgePointId, questionsContent, status);

        // 判断学生根据知识点提问是否成功，并设置返回信息
        if (flag) { // 成功
            responseResult.setCode(200);
            responseResult.setMessage("提问成功");
            responseResult.setStatus(ResponseStatus.SUCCESS);
        } else {    // 失败
            responseResult.setCode(500);
            responseResult.setMessage("提问失败");
            responseResult.setStatus(ResponseStatus.FAIL);
        }

        responseResult.setData(flag);

        // 向前端返回信息
        return  responseResult;
    }

    /**
     * 学生对一个视频提问
     * @param studentId
     * @param videoId
     * @param questionsContent
     * @param status
     * @return
     */
    @PostMapping("/addByVideo/{student_id}/{video_id}/{questions_content}/{status}")
//    public ResponseResult<Boolean> addByVideo(@RequestParam("student_id") int studentId, @RequestParam("video_id") int videoId, @RequestParam("questions_content") String questionsContent, @RequestParam("status") String status)  {
    public ResponseResult<Boolean> addByVideo(@PathVariable("student_id") int studentId, @PathVariable("video_id") int videoId, @PathVariable("questions_content") String questionsContent, @PathVariable("status") String status) {
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        boolean flag = questionsService.addByVideo(studentId, videoId, questionsContent, status);

        // 判断学生根据知识点提问是否成功，并设置返回信息
        if (flag) { // 成功
            responseResult.setCode(200);
            responseResult.setMessage("提问成功");
            responseResult.setStatus(ResponseStatus.SUCCESS);
        } else {    // 失败
            responseResult.setCode(500);
            responseResult.setMessage("提问失败");
            responseResult.setStatus(ResponseStatus.FAIL);
        }

        responseResult.setData(flag);

        return  responseResult;
    }


}
