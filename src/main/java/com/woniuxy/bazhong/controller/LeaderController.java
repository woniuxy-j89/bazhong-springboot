package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.Leader;
import com.woniuxy.bazhong.entity.School;
import com.woniuxy.bazhong.entity.Perms;
import com.woniuxy.bazhong.entity.School;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.service.LeaderService;
import com.woniuxy.bazhong.service.UserService;
import com.woniuxy.bazhong.utils.PageMessage;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/leader")
public class LeaderController {

    @Resource
    private LeaderService leaderService;

    // 创建校长账号并创建User账号并指定权限
    @PostMapping("/create")
    public ResponseResult<Void> createLeader(@RequestBody Leader leader){
        ResponseResult<Void> responseResult = new ResponseResult<>();
        //创建校长账号
        int res = leaderService.createLeader(leader);
        //判断是否创建成功
        if(res>0){
            responseResult.setCode(200);
            responseResult.setMessage("创建校长成功");
            responseResult.setStatus(ResponseStatus.SUCCESS);
            Leader leader1 = leaderService.findLeaderById(leaderService.getMaxId());
            //创建User账号
            User user  = new User();
            user.setAccount(leader1.getAccount());
            user.setPhone(leader1.getPhone());
            user.setPassword(leader1.getPassword());
            user.setSchoolId(leader1.getSchoolId());
            user.setStatus("1");
            leaderService.add(user);
            int id = leaderService.findMaxUserId();
            //创建User_Role权限
            leaderService.addUserRole(id);
        }
        return  responseResult;
    }

    /**
     * 分页展示查询
     * @param page  页码
     * @param size  每页展示数据条数
     * @return
     */
    @GetMapping("/findLeaders/{page}/{size}")
    public ResponseResult<PageMessage<List<Leader>>> findSchools(@PathVariable("page") int page, @PathVariable("size") int size){
        PageMessage<List<Leader>> leaders = leaderService.findLeaders(page, size);
        ResponseResult<PageMessage<List<Leader>>> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(leaders);

        return responseResult;
    }
    @GetMapping("/findByName/{leaderName}")
    public ResponseResult<Leader> findByName(@PathVariable("leaderName") String leaderName){
        Leader leader = leaderService.findByName(leaderName);
        ResponseResult<Leader> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(leader);

        return responseResult;
    }
}
