package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.service.TeacherService;
import com.woniuxy.bazhong.utils.PageMessage;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/teacher")
public class TeacherController {
    @Resource
    private TeacherService teacherService;

    @GetMapping("/findById")
    public ResponseResult<Teacher> findById(int id){
        Teacher teacher = teacherService.findById(id);
        log.debug(teacher.toString());
        ResponseResult<Teacher> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(teacher);
        return responseResult;
    }

    /**
     * 根据教师id分页查询视频
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/findVideoByTid/{page}/{size}")
    public ResponseResult<PageMessage<List<Video>>> findVideoByTid(@PathVariable("page")int page, @PathVariable("size")int size){
        int teacherId = 1; // 后期从token获取
        PageMessage<List<Video>> all = teacherService.findVideoByTid(page,size,teacherId);

        ResponseResult<PageMessage<List<Video>>> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(all);
        return responseResult;
    }

    /**
     * 试卷上传
     * @param exam
     * @return
     */
    @PostMapping("/uploadExam")
    public ResponseResult<Integer> uploadExam(@RequestBody Exam exam) {
        ResponseResult<Integer> responseResult = new ResponseResult<>();
        if(exam != null){
            int result = teacherService.uploadExam(exam);
            if(result != 0){
                responseResult.setCode(200);
                responseResult.setStatus(ResponseStatus.SUCCESS);
                responseResult.setMessage("试卷上传成功");
                responseResult.setData(result);
            } else {
                responseResult.setCode(500);
                responseResult.setStatus(ResponseStatus.FAIL);
                responseResult.setMessage("试卷上传失败");
                responseResult.setData(result);
            }
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("试卷为空");
        }
        return responseResult;
    }



    /**
     * 分页查询该教师并展示所有问题
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/allQuestion/{page}/{size}")
    public ResponseResult<PageMessage<List<Questions>>> answer(@PathVariable("page") int page, @PathVariable("size") int size){

        Teacher teacher = new Teacher();// 教师账号登录后获取
        teacher.setId(1);


        PageMessage<List<Questions>> result = teacherService.findQuestionsByTid(page, size, teacher.getId());

        ResponseResult<PageMessage<List<Questions>>> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(result);
        return responseResult;
    }

    /**
     * 查询具体的一条问题并返回
     * @param Qid
     * @return
     */
    @GetMapping("/findQuestionsByQid/{Qid}")
    public ResponseResult<Questions> findQuestionsByQid(@PathVariable("Qid") int Qid){
        ResponseResult<Questions> responseResult = new ResponseResult<>();

        Questions result = teacherService.findQuestionsByQid(Qid);
        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(result);
        return responseResult;
    }

    /**
     * 老师回答学生问题
     * @param questions
     * @return
     */
    @PutMapping("/answer")
    public ResponseResult<Boolean> answer(@RequestBody Questions questions){
        boolean b = teacherService.answer(questions);

        ResponseResult<Boolean> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("修改数据成功");
        responseResult.setData(b);

        return responseResult;
    }

    /**
     * 自动生成作业
     * @param knowledgepointId
     * @param questionsTypeName
     * @param num
     * @return
     */
    @PostMapping("/uploadHomework/{knowledgepointId}/{questionsTypeName}/{num}")
    public ResponseResult<Boolean> uploadHomework(@RequestBody Homework homework,@PathVariable("knowledgepointId") int knowledgepointId, @PathVariable("questionsTypeName") String questionsTypeName, @PathVariable("num") int num){
        boolean b = teacherService.uploadHomework(homework,knowledgepointId,questionsTypeName,num);
        ResponseResult<Boolean> responseResult = new ResponseResult<>();
        if(b){
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("自动生成作业成功");
            responseResult.setData(b);
        }else {
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("自动生成作业失败");
            // responseResult.setData(b);
        }
        return responseResult;
    }

    /**
     * 设置作业的完成时间
     * @param homeworkId
     * @param finalTime
     * @return
     */
    @PutMapping("/homework/setFinalTime/{homeworkId}/{finalTime}")
    public ResponseResult<Boolean> setFinalTime(@PathVariable("homeworkId") int homeworkId,@PathVariable("finalTime") String finalTime){
        boolean b = teacherService.setFinalTime(homeworkId,finalTime);
        ResponseResult<Boolean> responseResult = new ResponseResult<>();
        if(b){
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("设置完成时间成功");
            responseResult.setData(b);
        }else {
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("设置完成时间失败");
            // responseResult.setData(b);
        }
        return responseResult;
    }

    /**
     * 教师管理，根据教师姓名，查询展示教师当前学期所带班级
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    @GetMapping("/teacherOfClassess/{page}/{size}")
    public ResponseResult<PageMessage<Teacher>> teacherOfClassess(@PathVariable("page")int page, @PathVariable("size")int size){
        int teacherId = 1; // 后期从token获取
        PageMessage<Teacher> all = teacherService.teacherOfClassess(teacherId,page,size);
        ResponseResult<PageMessage<Teacher>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(all);

        return responseResult;
    }


//    public ResponseResult<Boolean> MachineRewinding(){
//
//    }

}
