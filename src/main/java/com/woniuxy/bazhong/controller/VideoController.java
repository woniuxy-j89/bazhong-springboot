package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.KnowledgePoint;
import com.woniuxy.bazhong.entity.Video;
import com.woniuxy.bazhong.service.VideoService;
import com.woniuxy.bazhong.utils.PageMessage;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhangjy
 * @date 2022/8/3 21:13
 */
@RestController
@RequestMapping("/video")
public class VideoController {

    @Resource
    private VideoService videoService;

    /**
     * 条件、分页查询所有视频，并且学生只能查询到审核成功的视频。
     *  暂时没有实现条件查询，默认分页查询所有审核成功的视频。
     * @param studentId
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/find/{student_id}/{page}/{size}")
    public ResponseResult<PageMessage<List<Video>>> all(@PathVariable("student_id") int studentId, @PathVariable("page") int page, @PathVariable("size") int size) {
        ResponseResult<PageMessage<List<Video>>> responseResult = new ResponseResult<>();

        PageMessage<List<Video>> all = videoService.allByCondition(studentId, page, size);

        // 判断是否查成功
        if (all != null) {  // 成功查到
            responseResult.setCode(200);
            responseResult.setMessage("成功查询到所有满足条件的视频");
            responseResult.setStatus(ResponseStatus.SUCCESS);
        } else {
            responseResult.setCode(500);
            responseResult.setMessage("查询失败");
            responseResult.setStatus(ResponseStatus.FAIL);
        }

        responseResult.setData(all);

        // 向前端返回信息
        return responseResult;
    }

    /**
     * 根据视频id查看一个视频
     * @param videoId
     * @return
     */
    @GetMapping("/find/{video_id}")
    public ResponseResult<Video> showOne(@PathVariable("video_id") int videoId) {
        ResponseResult<Video> responseResult = new ResponseResult<>();

        Video video = videoService.findById(videoId);

        // 判断是否查询成功，并设置返回信息
        if (video != null) {   // 成功
            responseResult.setCode(200);
            responseResult.setMessage("查询成功");
            responseResult.setStatus(ResponseStatus.SUCCESS);
        } else {    // 失败
            responseResult.setCode(500);
            responseResult.setMessage("查询失败");
            responseResult.setStatus(ResponseStatus.FAIL);
        }

        responseResult.setData(video);

        // 向前端返回信息
        return responseResult;
    }

}
