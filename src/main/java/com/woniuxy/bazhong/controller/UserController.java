package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.service.UserService;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    // 查询所有
    @GetMapping("/message")
    public ResponseResult<User> personal(){
        ResponseResult<User> responseResult = new ResponseResult<>();

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取信息成功");
        responseResult.setData(user);

        return responseResult;
    }

    @GetMapping("/personal")
    public ResponseResult<Object> findPersonal(){
        ResponseResult<Object> responseResult = new ResponseResult<>();

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(user.toString().contains("ROLE_ADMIN")){
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("超级管理员");
            return responseResult;
        }
        if(user.toString().contains("ROLE_SCHOOL")){
            School school = userService.findSchool(user.getSchoolId());
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("学校");
            responseResult.setData(school);

            return responseResult;
        }
        if(user.toString().contains("ROLE_EDUCATIONBUREAU")){
            EducationBureau educationBureau = userService.findeducationBureau(user.getId());
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("教育局");
            responseResult.setData(educationBureau);
            return responseResult;
        }
        if(user.toString().contains("ROLE_HEADMASTER")){
            Leader leader = userService.findLeader(user.getSchoolId());
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("校长");
            responseResult.setData(leader);
            return responseResult;
        }
        if(user.toString().contains("ROLE_TEACHER")||user.toString().contains("ROLE_HEADTEACHER")||user.toString().contains("ROLE_GRADEDEAN")){
            Teacher teacher = userService.findTeacher(user.getId());
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("老师");
            responseResult.setData(teacher);
            return responseResult;
        }
        if(user.toString().contains("ROLE_STUDENT")){
            Student student = userService.findStudnetByUid(user.getId());
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("学生");
            responseResult.setData(student);
            return responseResult;
        }else{
            responseResult.setCode(400);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("账号有误");
            return responseResult;
        }


    }

    @GetMapping("/school")
    public ResponseResult<Object> findSchool(){
        ResponseResult<Object> responseResult = new ResponseResult<>();

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        School school = userService.findSchool(user.getSchoolId());

        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("学校");
        responseResult.setData(school);
        return responseResult;
    }
    @GetMapping("student/classes")
    public  ResponseResult<Object> findStudentClass(){
        ResponseResult<Object> responseResult = new ResponseResult<>();

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Student student = userService.findStudnetByUid(user.getId());

        Classes classes = userService.findByStudent(student.getClassId());
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("班级");
        responseResult.setData(classes);
        return responseResult;
    }
    @GetMapping("/subject")
    public  ResponseResult<Object> findSubject(){
        ResponseResult<Object> responseResult = new ResponseResult<>();

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Teacher teacher = userService.findTeacher(user.getId());

        Subject subject = userService.findSubject(teacher.getSubjectId());

        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("科目");
        responseResult.setData(subject);
        return responseResult;

    }
    @GetMapping("/inform")
    public  ResponseResult<Object> findInform(){
        ResponseResult<Object> responseResult = new ResponseResult<>();

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<ChatMessage> chatMessages = userService.findInform(user.getId());

        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("科目");
        responseResult.setData(chatMessages);
        return responseResult;

    }
}
