package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.Student;
import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.service.HeadteacherService;
import com.woniuxy.bazhong.utils.PageMessage;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import com.woniuxy.bazhong.utils.SendEmail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by raoyucheng
 * on 2022/7/21 19:47
 */
@Slf4j
@RestController
@RequestMapping("/headteacher")
public class HeadteacherController {
    /**
     * 班主任所属业务类
     */
    @Resource
    private HeadteacherService headteacherService;

    /**
     * 查询展示所有学生
     * @param page 显示页码
     * @param size 每页展示数据条数
     * @return
     */
    @GetMapping("/allStudent/{page}/{size}")
    public ResponseResult<PageMessage<List<Student>>>
            allStudent(@PathVariable("page")int page, @PathVariable("size")int size){
        PageMessage<List<Student>> all = headteacherService.allStudent(page,size);
        ResponseResult<PageMessage<List<Student>>> responseResult=new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setMessage("获取数据成功");
        responseResult.setData(all);

        return responseResult;
    }

    /**
     * 通过Excel表格文件，批量添加新学生
     * @param file
     * @return responseResult
     */
    @PostMapping ("/bulkimport")
    @ResponseBody
    public ResponseResult<Boolean> bulkimportStudents
            (@RequestParam("filename") MultipartFile file){
        log.info("批量导入学生！！");
        boolean flag = false;

        String fileName = file.getOriginalFilename();
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            log.info("准备进入headteacherService");
            flag = headteacherService.addStudents(fileName,file);
            System.out.println(flag);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("上传成功");
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("上传失败");

        }

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        System.out.println(user);

        return responseResult;
    }

    /**
     * 通过Excel表格文件，批量添加高考成绩
     * @param file
     * @return responseResult
     */
    @PostMapping ("/hightestimport")
    @ResponseBody
    public ResponseResult<Boolean> bulkimportEnrollscore
        (@RequestParam("filename") MultipartFile file){
        log.info("批量导入成绩！！");
        boolean flag = false;

        String fileName = file.getOriginalFilename();
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            log.info("准备进入headteacherService");
            flag = headteacherService.addHighTestScores(fileName,file);
            System.out.println(flag);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("上传成功");
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("上传失败");

        }
        return responseResult;
    }

    @PostMapping ("/resetpassword")
    @ResponseBody
    public ResponseResult<Boolean> resetStudentPassword
            (@RequestBody String studentNumber){
        log.info("重置学生密码！！");
        boolean flag = false;
        String email = "";

        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            int studentNumbers = Integer.parseInt(studentNumber);

            email = headteacherService.findStuEmailByNumber(studentNumber);

            flag = headteacherService.resetStudentPassword(studentNumbers);

            System.out.println(flag);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("重置成功");

            SendEmail.sendEmail(email, "949606970@qq.com");
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("重置失败");

        }
        return responseResult;
    }


    /**
     * 通过Excel表格文件，批量添加新学生
     * @param file
     * @return responseResult
     */
    @PostMapping("/totalPoint")
    @ResponseBody
    public ResponseResult<Boolean> studentTotalPoint(@RequestParam("filename") MultipartFile file){
        boolean flag = false;

        String fileName = file.getOriginalFilename();
        ResponseResult<Boolean> responseResult = new ResponseResult<>();

        try {
            flag = headteacherService.addStudentTotalPoint(fileName,file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(flag){
            //成功写入数据
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("写入数据成功");
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("写入数据失败");

        }
        return responseResult;
    }
}




