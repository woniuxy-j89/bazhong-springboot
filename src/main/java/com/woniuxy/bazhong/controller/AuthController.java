package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.User;
import com.woniuxy.bazhong.service.AuthService;
import com.woniuxy.bazhong.service.UserService;
import com.woniuxy.bazhong.utils.JWTUtil;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@RestController
@Slf4j
@RequestMapping("/auth")
public class AuthController {

    @Resource
    private AuthService authService;

    @PostMapping("/login")
    private ResponseResult<Object> login(@RequestBody User user , HttpServletResponse response){
        log.debug(user.toString());
        ResponseResult<Object> responseResult = new ResponseResult<>();
        boolean result = authService.login(user);

        if(result){
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.LOGIN_SUCCESS);
            responseResult.setMessage("登录成功");

            User u= authService.findByAccount(user.getAccount());
            responseResult.setData(u);
            log.debug(user.toString());
            String token = JWTUtil.generateToken(user.getAccount());

            // 将token放到响应头中   自定义头的名字，放token是都习惯用authentication作为头的名字
            response.addHeader("authorization",token);
            // springboot项目中如果要自定义响应头需要手动暴露该头，如果不暴露前端代码是无法获取到头信息
            response.setHeader("Access-Control-Expose-Headers","authorization");
        }else{
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("账号密码有误");
        }


        return responseResult;
    }
}
