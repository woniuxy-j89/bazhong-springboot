package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.Homework;
import com.woniuxy.bazhong.entity.HomeworkStudent;
import com.woniuxy.bazhong.service.HomeworkService;
import com.woniuxy.bazhong.utils.PageMessage;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author zhangjy
 * @date 2022/7/23 15:33
 */
@Slf4j
@RestController
@RequestMapping("/homework")
public class HomeworkController {

    @Resource
    private HomeworkService homeworkService;

    // 获取本地存放图片文件夹的路径
    @Value("${filedir}")
    private String filedir;

    /**
     * 分页查询：
     * 查询学生的所有作业的所有信息（过期和未过期，以及提交和未提交），并且给作业isExpired属性赋值（是否过期）
     *
     * @param studentId
     * @return
     */
    @GetMapping("/all/{student_id}/{page}/{size}")
    public ResponseResult<PageMessage<List<HomeworkStudent>>> all(@PathVariable("student_id") int studentId, @PathVariable("page") int page, @PathVariable("size") int size) {
        ResponseResult<PageMessage<List<HomeworkStudent>>> responseResult = new ResponseResult<>();

        // 分页查询——根据学生id查询所有作业
        PageMessage<List<HomeworkStudent>> all = homeworkService.all(studentId, page, size);

        // 判断是否查询成功
        if (all != null) {
            responseResult.setCode(200);
            responseResult.setMessage("成功查询到所有作业");
            responseResult.setStatus(ResponseStatus.SUCCESS);
        } else {
            responseResult.setCode(500);
            responseResult.setMessage("查询失败");
            responseResult.setStatus(ResponseStatus.FAIL);
        }

        responseResult.setData(all);

        // 向前端返回消息
        return responseResult;
    }

    /**
     * 查看或编写一个作业——其实为查询操作，根据作业是否过期显示不同按钮（过期就显示查看作业，反之显示编写作业）;
     * 1.查看：
     * a.未提交：查询作业题目、参考答案、学生提交的答案（此时为null）；
     * b.已经提交：查询作业题目、参考答案、学生提交的答案；
     * 2.编写：
     * a.未提交：查询作业题目、学生提交的答案（但是后者为null，所以前端只显示题目）；
     * b.已经提交：查询作业题目、学生提交的答案（都显示）；
     *
     * @param studentId
     * @param homeworkId
     * @return
     */
    @GetMapping("/showOrWrite/{student_id}/{homework_id}")
    public ResponseResult<Object> showOrWrite(@PathVariable("student_id") int studentId, @PathVariable("homework_id") int homeworkId) {
        ResponseResult<Object> responseResult = new ResponseResult<>();

        // 查询指定作业的所有信息
        HomeworkStudent homeworkStudent = homeworkService.findByStudentIdAndHomeworkId(studentId, homeworkId);

        // 判断是否能查到指定作业
        if (homeworkStudent != null) {  // 可以
            responseResult.setCode(200);
            responseResult.setMessage("成功查到指定作业");
            responseResult.setStatus(ResponseStatus.SUCCESS);
        } else {    // 没找到
            responseResult.setCode(500);
            responseResult.setMessage("找不到指定作业");
            responseResult.setStatus(ResponseStatus.FAIL);
        }
        responseResult.setData(homeworkStudent);

        return responseResult;
    }

    /**
     * 学生提交一道作业题目答案
     *
     * @param studentId
     * @param homeworkId
     * @param homeworkTopicId
     * @param answer
     * @return
     */
    @PostMapping("/addHomeworkAnswer/{student_id}/{homework_id}/{homework_topic_id}/{answer}")
    /* public ResponseResult<String> addHomeworkAnswer(@RequestParam("studentId") int studentId, @RequestParam("homeworkId") int homeworkId, @RequestParam("homeworkTopicId") int homeworkTopicId, @RequestParam("answer") String answer) {*/
    public ResponseResult<String> addHomeworkAnswer(@PathVariable("student_id") int studentId, @PathVariable("homework_id") int homeworkId, @PathVariable("homework_topic_id") int homeworkTopicId, @PathVariable("answer") String answer) {
        ResponseResult<String> responseResult = new ResponseResult<>();

        log.info("从前端接收的参数studentId、homeworkId、homeworkTopicId和answer分别为：" + studentId + "..." + homeworkId + "..." + homeworkTopicId + "..." + answer);

        boolean flag = homeworkService.addHomeworkAnswer(studentId, homeworkId, homeworkTopicId, answer);

        // 判断是否提交成功
        if (flag) { // 成功
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("提交成功");
            responseResult.setData("答案提交成功");
        } else {    // 失败
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("提交失败");
            responseResult.setData("答案提交失败");
        }

        return responseResult;
    }

    /**
     * 学生修改一道作业题目答案
     *
     * @param studentId
     * @param homeworkId
     * @param homeworkTopicId
     * @param answer
     * @return
     */
//    @PostMapping("/updateHomeworkAnswer")
//    public ResponseResult<String> updateHomeworkAnswer(@RequestParam("studentId") int studentId, @RequestParam("homeworkId") int homeworkId, @RequestParam("homeworkTopicId") int homeworkTopicId, @RequestParam("answer") String answer) {
    @PostMapping("/updateHomeworkAnswer/{student_id}/{homework_id}/{homework_topic_id}/{answer}")
    public ResponseResult<String> updateHomeworkAnswer(@PathVariable("student_id") int studentId, @PathVariable("homework_id") int homeworkId, @PathVariable("homework_topic_id") int homeworkTopicId, @PathVariable("answer") String answer) {
        ResponseResult<String> responseResult = new ResponseResult<>();

        log.info("从前端接收的参数studentId、homeworkId、homeworkTopicId和answer分别为：" + studentId + "..." + homeworkId + "..." + homeworkTopicId + "..." + answer);

        boolean flag = homeworkService.updateHomeworkAnswer(studentId, homeworkId, homeworkTopicId, answer);

        // 判断是否修改成功
        if (flag) { // 成功
            responseResult.setCode(200);
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setMessage("修改成功");
            responseResult.setData("答案修改成功");
        } else {    // 失败
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("修改失败");
            responseResult.setData("答案修改失败");
        }

        return responseResult;
    }

    /**
     * 上传解答题图片（上传一张），即第一次提交图片——前后端分离，尚未测试
     *
     * @param image
     * @return
     */
    @PostMapping("/upload")
    public ResponseResult<String> upload(@RequestParam MultipartFile image, @RequestParam("studentId") int studentId, @RequestParam("homeworkId") int homeworkId, @RequestParam("homeworkTopicId") int homeworkTopicId) {
        ResponseResult<String> responseResult = new ResponseResult<>();

        // 存放路径
        log.info(filedir);

        // 获取文件的名字
        String imageName = image.getOriginalFilename();

        // 重新给文件取名
        String fileName = UUID.randomUUID().toString() + imageName.substring(imageName.lastIndexOf("."));
        log.info(fileName);

        // 得到文件路径
        String filePath = filedir + File.separator + fileName;
        log.info("新的图片存储路径---" + filePath);

        // 将文件存储到指定位置
        try {
            image.transferTo(new File(filePath));

            // 将图片路径存入学生作业答案
            boolean flag = homeworkService.addHomeworkAnswer(studentId, homeworkId, homeworkTopicId, filePath);

            // 判断是否修改成功
            if (flag) { // 成功

                responseResult.setCode(200);
                responseResult.setStatus(ResponseStatus.SUCCESS);
                responseResult.setMessage("图片上传成功");
                responseResult.setData(fileName);
            } else {    // 失败
                responseResult.setCode(500);
                responseResult.setStatus(ResponseStatus.FAIL);
                responseResult.setMessage("图片存储失败");
            }
        } catch (Exception e) {
            e.printStackTrace();

            // 失败
            responseResult.setCode(500);
            responseResult.setStatus(ResponseStatus.FAIL);
            responseResult.setMessage("图片上传失败");
        }

        // 遍历数组，分别上传文件
       /* for (MultipartFile multipartFile : images) {
            // 获取文件的名字
            String imageName = multipartFile.getOriginalFilename();

            // 重新给文件取名
            String fileName = UUID.randomUUID().toString() + imageName.substring(imageName.lastIndexOf("."));
            log.info(fileName);

            // 得到文件路径
            String filePath = filedir + File.separator + fileName;
            log.info("新的图片存储路径---" + filePath);

            // 将文件存储到指定位置
            try {
                multipartFile.transferTo(new File(filePath));

                // 将图片路径存入学生作业答案


                // 成功
                responseResult.setCode(200);
                responseResult.setStatus(ResponseStatus.SUCCESS);
                responseResult.setMessage("图片上传成功");
                responseResult.setData(fileName);
            } catch (Exception e) {
                e.printStackTrace();

                // 失败
                responseResult.setCode(500);
                responseResult.setStatus(ResponseStatus.FAIL);
                responseResult.setMessage("图片上传失败");
            }
        }
*/
        return responseResult;
    }

}
