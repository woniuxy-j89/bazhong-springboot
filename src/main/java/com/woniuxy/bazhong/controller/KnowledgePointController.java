package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.KnowledgePoint;
import com.woniuxy.bazhong.service.KnowledgePointService;
import com.woniuxy.bazhong.utils.PageMessage;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhangjy
 * @date 2022/8/3 15:18
 */
@Slf4j
@RestController
@RequestMapping("/knowledgepoint")
public class KnowledgePointController {

    @Resource
    private KnowledgePointService knowledgePointService;

    /**
     * 条件（科目、知识点重要程度）、分页查询所有知识点
     *
     * @param subjectId 0表示无条件(默认)，1-9分别对应语数外、理化生、政史地
     * @param status    0表示无条件(默认)，1为重点，2是非重点
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/find/{subject_id}/{status}/{page}/{size}")
    public ResponseResult<PageMessage<List<KnowledgePoint>>> all(@PathVariable("subject_id") int subjectId, @PathVariable("status") String status, @PathVariable("page") int page, @PathVariable("size") int size) {
        ResponseResult<PageMessage<List<KnowledgePoint>>> responseResult = new ResponseResult<>();

        PageMessage<List<KnowledgePoint>> all = knowledgePointService.all(subjectId, status, page, size);

        // 判断是否查成功
        if (all != null) {  // 成功查到
            responseResult.setCode(200);
            responseResult.setMessage("成功查询到所有满足条件的知识点");
            responseResult.setStatus(ResponseStatus.SUCCESS);
        } else {
            responseResult.setCode(500);
            responseResult.setMessage("查询失败");
            responseResult.setStatus(ResponseStatus.FAIL);
        }

        responseResult.setData(all);

        // 向前端返回消息
        return responseResult;
    }

    /**
     * 根据知识点id，查询一个知识点的信息
     * @param knowledgePointId
     * @return
     */
    @GetMapping("/find/{knowledge_point_id}")
    public ResponseResult<KnowledgePoint> showOne(@PathVariable("knowledge_point_id") int knowledgePointId) {
        ResponseResult<KnowledgePoint> responseResult = new ResponseResult<>();

        KnowledgePoint knowledgePoint = knowledgePointService.findById(knowledgePointId);

        // 判断是否查询成功，并设置返回信息
        if (knowledgePoint != null) {   // 成功
            responseResult.setCode(200);
            responseResult.setMessage("查询成功");
            responseResult.setStatus(ResponseStatus.SUCCESS);
        } else {    // 失败
            responseResult.setCode(500);
            responseResult.setMessage("查询失败");
            responseResult.setStatus(ResponseStatus.FAIL);
        }

        responseResult.setData(knowledgePoint);

        // 向前端返回信息
        return responseResult;
    }

}
