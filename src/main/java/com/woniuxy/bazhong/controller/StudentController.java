package com.woniuxy.bazhong.controller;

import com.woniuxy.bazhong.entity.Article;
import com.woniuxy.bazhong.entity.Scores;
import com.woniuxy.bazhong.entity.Student;
import com.woniuxy.bazhong.entity.Video;
import com.woniuxy.bazhong.service.ArticleService;
import com.woniuxy.bazhong.service.ScoresService;
import com.woniuxy.bazhong.service.StudentService;
import com.woniuxy.bazhong.service.VideoService;
import com.woniuxy.bazhong.utils.PageMessage;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liwei
 * @data 2022/7/21{} 17:07
 */
@Slf4j
@RestController
@RequestMapping("/student")
public class StudentController {
    @Resource
    private StudentService studentService;
    @Resource
    private ScoresService scoresService;
    @Resource
    private ArticleService articleService;
    @Resource
    private VideoService videoService;

    /**
     * 根据id查找学生
     * @param id
     * @return Student
     */
    @GetMapping("/findById/{id}")
    public ResponseResult<Student> findById(@PathVariable("id") int id){
        ResponseResult<Student> responseResult = new ResponseResult<>();

        if(studentService.findById(id).getId()!=0){
            responseResult.setCode(200);
            responseResult.setMessage("成功");
            responseResult.setStatus(ResponseStatus.SUCCESS);
            responseResult.setData(studentService.findById(id));
        }else{
            responseResult.setCode(201);
            responseResult.setMessage("id不能为空");
            responseResult.setStatus(ResponseStatus.FAIL);
        }
        return responseResult;
    }

    /**
     * 根据学生id查询各科分数
     * @param id
     * @return
     */
    @GetMapping("/findScoresByStId/{id}")
    public ResponseResult<List<Scores>> findScoresByStId(@PathVariable("id") int id){
        ResponseResult<List<Scores>> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(scoresService.findScoresByStId(id));
        return responseResult;
    }

    /**
     * 文章分页查询
     * @param page
     * @param size
     * @param info
     * @return
     */
    @GetMapping("/findArticleByLike/{page}/{size}/{info}")
    public ResponseResult<PageMessage<List<Article>>> findArticleByLike(@PathVariable("page")int page, @PathVariable("size")int size,@PathVariable("info") String info){
        ResponseResult<PageMessage<List<Article>>> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(articleService.findArticleByLike(page,size,info));
        return responseResult;
    }

    /**
     * 视频分页查询
     * @param page
     * @param size
     * @param info
     * @return
     */
    @GetMapping("/findVideoByLike/{page}/{size}/{info}")
    public ResponseResult<PageMessage<List<Video>>> findVideoByLike(@PathVariable("page")int page, @PathVariable("size")int size, @PathVariable("info")String info){
        ResponseResult<PageMessage<List<Video>>> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(videoService.findVideoByLike(page, size, info));
        return responseResult;
    }

    /**
     * 学生文理科选择
     * @param liberaScience
     * @param id
     * @return
     */
    @GetMapping("/updateStudentChoice/{liberaScience}/{id}")
    public ResponseResult<Object> updateStudentChoice(@PathVariable("liberaScience") String liberaScience,@PathVariable("id") int id){
        ResponseResult<Object> responseResult = new ResponseResult<>();
        if (studentService.findById(id).getClasses().getName().substring(0,2).equals("高一")){
            if(studentService.updateStudentChoice(liberaScience, id)){
                responseResult.setCode(200);
                responseResult.setMessage("修改成功");
                responseResult.setStatus(ResponseStatus.SUCCESS);
            }else{
                responseResult.setCode(201);
                responseResult.setMessage("修改失败");
                responseResult.setStatus(ResponseStatus.FAIL);
            }
        }else{
            responseResult.setData(201);
            responseResult.setMessage("该学生不是高一学生");
            responseResult.setStatus(ResponseStatus.FAIL);
        }
        return responseResult;
    }
    @GetMapping("/findAllArticle/{page}/{size}")
    public ResponseResult<PageMessage<List<Article>>> findAllArticle(@PathVariable("page")int page, @PathVariable("size")int size){
        ResponseResult<PageMessage<List<Article>>> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(articleService.findAllArticle(page,size));
        return responseResult;
    }
    @GetMapping("/findAllVideo/{page}/{size}")
    public ResponseResult<PageMessage<List<Video>>> findAllVideo(@PathVariable("page")int page, @PathVariable("size")int size){
        ResponseResult<PageMessage<List<Video>>> responseResult = new ResponseResult<>();
        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(videoService.findAllVideo(page,size));
        return responseResult;
    }

    /**
     * 根据用户id查找学生
     * @param userId
     * @return
     */
    @GetMapping("findByUserId/{user_id}")
    public ResponseResult<Student> findByUserId(@PathVariable("user_id") int userId) {
        ResponseResult<Student> responseResult = new ResponseResult<>();

        Student student = studentService.findByUserId(userId);

        responseResult.setCode(200);
        responseResult.setMessage("查询成功");
        responseResult.setStatus(ResponseStatus.SUCCESS);
        responseResult.setData(student);

        return responseResult;
    }

}
