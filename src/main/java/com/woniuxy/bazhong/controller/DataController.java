package com.woniuxy.bazhong.controller;


import com.woniuxy.bazhong.entity.Data;
import com.woniuxy.bazhong.entity.DataData;
import com.woniuxy.bazhong.entity.Grade;
import com.woniuxy.bazhong.service.DataService;
import com.woniuxy.bazhong.utils.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* @author lzw
* 数据统计
*/
@RestController
@RequestMapping("/data")
@Slf4j
public class DataController {
    @Resource
    private DataService dataService;

    //获取分数
    //semester:学期，格式 2016-2017-1  学年-1(上学期)或2(下学期)   startDate,endDate起止时间
    //grade 年级    year年份：用来查询分数线
    @PostMapping("/scores")
    public ResponseResult<List<Data<Object>>> getDate(@RequestBody DataData dataData){
        List<Long> list = dataService.getTestPaper(dataData.getSemester(), dataData.getStartDate(), dataData.getEndDate());
        ResponseResult<List<Data<Object>>> responseResult= new ResponseResult<>();
        responseResult.setData(dataService.getData(dataData.getGrade(), dataData.getYear(), list));

        return responseResult;
    }

    @PostMapping("/nums")
    //获取升学人数
    public ResponseResult<List<Data<Object>>> getNums(@RequestBody DataData dataData){
        List<Long> list = dataService.getTestPaper(dataData.getSemester(), dataData.getStartDate(), dataData.getEndDate());
        ResponseResult<List<Data<Object>>> responseResult= new ResponseResult<>();
        responseResult.setData(dataService.getNums(dataData.getGrade(), dataData.getYear(),list));

        return responseResult;
    }

    @PostMapping("/rate")
    //获取升学率
    public ResponseResult<List<Data<Object>>> getRate(@RequestBody DataData dataData){
        List<Long> list = dataService.getTestPaper(dataData.getSemester(), dataData.getStartDate(), dataData.getEndDate());
        ResponseResult<List<Data<Object>>> responseResult= new ResponseResult<>();
        responseResult.setData(dataService.getRate(dataData.getGrade(), dataData.getYear(),list));
        return responseResult;
    }
    @GetMapping("/semester")
    //获取学期
    public ResponseResult<List> getSemester(){
        ResponseResult<List> responseResult = new ResponseResult<>();
        responseResult.setData(dataService.getSemester());
        return responseResult;
    }

    @GetMapping("/grades")
    //获取年级
    public ResponseResult<List<Grade>> getGrades(){
        ResponseResult<List<Grade>> responseResult=new ResponseResult<>();
        responseResult.setData(dataService.getGrades());
        return responseResult;
    }
}
