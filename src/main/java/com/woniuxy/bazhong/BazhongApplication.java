package com.woniuxy.bazhong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

@SpringBootApplication
@EnableWebSocket    //启用websocket
@EnableGlobalMethodSecurity(prePostEnabled = true) // 开启security的角色、权限注解功能
public class BazhongApplication {

    public static void main(String[] args) {
        SpringApplication.run(BazhongApplication.class, args);
    }
    @Bean    //在容器中创建bean对象，在WebSocketUtil中需要用到的RemoteEndpoint对象
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
