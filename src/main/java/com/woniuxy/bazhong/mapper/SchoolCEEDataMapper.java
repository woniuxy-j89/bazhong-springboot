package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.EnrolmentRate;
import com.woniuxy.bazhong.entity.School;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SchoolCEEDataMapper {
    /**
     * 各校同级数据对比模块，展示学校列表
     * @return List<School>学校列表
     */
    List<School> allSchool();

    /**
     * 各校同级数据对比模块，展示升学率
     * @param theschoolId 本校id
     * @param otherSchoolId 传入学校id
     * @return List<EnrolmentRate>
     */
    List<EnrolmentRate> showEnrolmentRate(@Param("theSchoolId") int theschoolId, @Param("otherSchoolId") int otherSchoolId);
}
