package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Exam;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ExamMapper {
    List<Exam> findAllExamBySchool(int schoolId);
}
