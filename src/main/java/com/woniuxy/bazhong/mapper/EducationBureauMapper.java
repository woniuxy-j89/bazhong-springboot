package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.EducationBureau;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EducationBureauMapper {
    EducationBureau findOne(int userId);
}
