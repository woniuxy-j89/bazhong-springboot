package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Classes;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ClassesMapper {
    List<Classes> findAllClassBySchool(int schoolId);

    Classes findByClassId(int classId);

    Classes findAllClassByTeacher(int teacherId);
}
