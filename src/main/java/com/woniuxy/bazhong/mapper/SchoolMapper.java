package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.School;
import com.woniuxy.bazhong.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author lzw
*/
@Mapper
public interface SchoolMapper {

    public Integer findMaxId();

    public String findNumber(Integer maxId);

    public int createSchool(School school);

    public School findSchoolById(int maxId);

    public void add(User user);

    public int findMaxUserId();

    public void addUserRole(int id);

    public List<School> findSchools();

    public School findByNumber(String schoolNumber);
}
