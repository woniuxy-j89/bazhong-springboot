package com.woniuxy.bazhong.mapper;


import com.woniuxy.bazhong.entity.HistoryScore;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface HistoryScoreMapper {

    public List<HistoryScore> findAllScoreByName(String exam);

    public List<HistoryScore>
        findAllScoreByClass(@Param("classes") String classes, @Param("subject") String subject,@Param("schoolId")int schoolId);
}
