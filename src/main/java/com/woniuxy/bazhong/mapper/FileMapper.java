package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FileMapper {
    // 上传视频
    public boolean uploadVideo(Video video);

    public Choice findChoiceById(int id);

    public boolean addChoice(Choice choice);

    public Blank findBlankById(int id);

    public boolean addBlank(Blank b);

    public Reading findReadingById(int id);

    public boolean addReading(Reading r);

    public Title findTitleById(int id);

    public boolean addTitle(Title t);

    public String findModel(@Param("subjectId") int subjectId, @Param("type") String type);

    public List<QuestionTypes> findTypeBySid(int subjectId);
}
