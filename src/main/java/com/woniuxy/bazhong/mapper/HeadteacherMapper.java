package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Collegeentranceexamination;
import com.woniuxy.bazhong.entity.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created by raoyucheng
 * on 2022/7/21 21:21
 */
@Mapper
public interface HeadteacherMapper {
    /**
     * 插入一个学生
     * @param student
     * @return 是否成功插入一个学生
     */
    public boolean insertStudent(Student student);

    /**
     * 根据学号找学生的数量（是否存在一个学生）
     * @param number
     * @return 对应学号的学生数量（1 或 0）
     */
    public int findStudentByNumber(String number);

    /**
     * 更新学生信息
     * @param student
     * @return
     */
    public boolean updateStudent(Student student);

    void updateStudentUserId(Student student2);

    int findMaxScoresId();

    List<Student> allStudent(int classId);

    int findUserIdByNumber(int studentNumber);

    String findEmailByNumber(String studentNumber);

    boolean insertScoresCollage(Collegeentranceexamination scoresCollage);

}
