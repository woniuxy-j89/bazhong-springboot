package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.ExamAnswer;
import com.woniuxy.bazhong.entity.Grade;
import com.woniuxy.bazhong.entity.ScoreLine;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DataMapper {
    //根据学期、起始时间得到试卷信息
    public List<Long> getTestPaper(String semester, String startDate, String endDate);

    //根据年份查询分数线
    public ScoreLine getScoreLine(String year);

    //根据年级、id获取答题卡信息
    public List<ExamAnswer> getExamAnswer(String grade, Long id);

    //根据学号查询成绩
    public List<Double> getScore(String number);

    public List getSemester();

    public List<Grade> getGrades();
}
