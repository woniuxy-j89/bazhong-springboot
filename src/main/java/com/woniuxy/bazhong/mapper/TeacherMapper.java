package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TeacherMapper {
    public Teacher findById(int id);

//  通过UserId找到TeacherId
    int findTeacherIdById(int id);


    public int uploadExam(Exam exam);

    public int findSidByexamId(int id);

    public int addExamTopic(ExamTopic examTopic);

    public List<Video> findVideoByTid(int teacherId);


   public Teacher teacherOfClassess(int teacherId,String nowSemester);
   public Teacher findByUserId(int userId);
}
