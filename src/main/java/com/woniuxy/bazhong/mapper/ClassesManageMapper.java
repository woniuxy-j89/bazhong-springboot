package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.entity.Subject;
import com.woniuxy.bazhong.entity.TeacherClasses;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ClassesManageMapper {
    /**
     * 查询展示当前学期的所有班级
     * @return
     */
    public List<Classes> allClasses(@Param("nowSemester") String nowSemester, @Param("schoolId")int schoolId);

    /**
     * 根据所选班级的班级类型查询展示学科
     * @param classesId
     * @return
     */
    public List<Subject> subOfClasses(int classesId);

    /**
     * 授课教师安排中，安排各学科授课教师
     * @param teacherClasses 所要添加班级的信息
     */
     int addsubTeacherOfClasses(TeacherClasses teacherClasses);

    /**
     * 班主任任命中，根据所选班级，展示所有任课教师
     * @param classesId 班级id
     * @return
     */
     Classes allSubTeacherOfClasses(int classesId);

    /**
     *  班主任任命中，任命或更换班主任
     * @param classesId 班级id
     * @param teacherId 老师id
     */
    public int updateHTeacherOfClasses(@Param("classesId")int classesId,@Param("teacherId")int teacherId);

    /**
     * 添加班级
     * @param classes
     * @return
     */
    public  int addClasses(Classes classes);

    /**
     * 修改班级类型
     * @param classes 所要修改班级的信息
     */
    public int updateClassesType(Classes classes);

    /**
     * 更换班主任
     * @param classes 所要修改班级的信息
     */
    public int updateClassesHT(Classes classes);


}
