package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.KnowledgePoint;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface KnowledgePointMapper {

    public boolean addKnowledgePoint(KnowledgePoint knowledgePoint);

    public int findPointByName(String name);

    public boolean updatePoint(KnowledgePoint k);

    public int findPointIdByName(String name);

    public int findPointNameById(int id);

    /**
     * 查询所有知识点——条件查询
     *
     * @param subjectId
     * @param status
     * @return
     */
    public List<KnowledgePoint> findAllKnowledgePoint(@Param("subjectId") int subjectId, @Param("status") String status);

    List<KnowledgePoint> findKnow();

    /**
     * 根据id查询一个知识点的信息
     * @param id
     * @return
     */
    public KnowledgePoint findById(int id);

}
