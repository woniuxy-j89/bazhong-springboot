package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Article;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 文章mapper
 */
@Mapper
public interface ArticleMapper {
    public List<Article> findToAudit(int schoolId);

    public boolean updateStatus(Article article);

    /**
     * 文章的模糊查询
     * @param info
     * @return
     */
    public List<Article> findArticleByLike(String info);

    /**
     * 查询所有文章
     * @return
     */
    public List<Article> findAllArticle();
}
