package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Classes;
import com.woniuxy.bazhong.entity.Teacher;
import com.woniuxy.bazhong.entity.TeacherClasses;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TeacherManageMapper {
    /**
     * 查询展示所有老师
     * @return
     */
    public List<Teacher> allTeacher(int schoolId);

    /**
     * 查询展示科目对应的授课老师
     * @param subjectId 科目id
     * @return
     */
    List<Teacher> teacherOfSubject(@Param("schoolId") int schoolId, @Param("subjectId")int subjectId);

    /**
     * 查询教师信息及所带班级信息，并封装为分页数据
     * @param teacherName
     * @param
     * @return
     */
    //public List<Classes> teacherOfClassess(String teacherName);
    List<Classes> classessOfTeacher(@Param("teacherName") String teacherName, @Param("nowSemester") String nowSemester);

    /**
     * 教师账号管理下，重新指定班級中刪除所选班级
     * @param teacherClasses 所要修改班级的信息
     */
    int delClassesOfTeacher(TeacherClasses teacherClasses);

    /**
     * 教师账号管理中，添加班级下，没有指定老师对应课程的老师的班级
     * @param schoolId 学校id
     * @param nowSemester 当前学期
     * @param subType 指定老师所授学科的类别
     * @param subjectId  指定老师所授学科id
     * @return List<Classes>
     */
    List<Classes> nullSubTeacherOfClasses(@Param("schoolId") int schoolId, @Param("nowSemester") String nowSemester, @Param("subType") String subType, @Param("subjectId") int subjectId);


}
