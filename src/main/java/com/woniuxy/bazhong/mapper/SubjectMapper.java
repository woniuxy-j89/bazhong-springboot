package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Subject;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SubjectMapper {
    Subject findSubject(int subjectId);
}
