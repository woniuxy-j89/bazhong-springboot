package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface QuestionsMapper {
    // 通过id查询问题
    public Questions findQuestionsById(int id);

    // 通过老师的id分页查询所有问题
    public List<Questions> findQuestionsByTid(int teacherId);

    public int answer(Questions questions);

    // 根据科目、题型查询questionsTypeId
    public int findQuestionTypeIdBySidandType(@Param("type") String type,@Param("subjectId") int subjectId);

    public List<Choice> findQuestionsByTypeandNum1(@Param("knowledgepointId") int knowledgeId,@Param("num") int num);
    public List<Blank> findQuestionsByTypeandNum2(@Param("knowledgepointId")int knowledgepointId,@Param("num") int num);
    public List<Reading> findQuestionsByTypeandNum3(@Param("knowledgepointId")int knowledgepointId, @Param("num") int num);
    public List<Title> findQuestionsByTypeandNum4(@Param("knowledgepointId")int knowledgeId,@Param("num") int num);

    /**
     * 插入学生提问（不管是根据知识点or视频）
     * @param questions
     * @return
     */
    public boolean add(Questions questions);

    /**
     * 问题知识点第三方关联表新增数据
     * @param questionsId
     * @param knowledgePointId
     * @return
     */
    public boolean addQuestionsAndKnowledgePoint(@Param("questionsId") int questionsId, @Param("knowledgePointId") int knowledgePointId);

    /**
     * 问题视频第三方关联表新增数据
     * @param questionsId
     * @param videoId
     * @return
     */
    public boolean addQuestionsAndVideo(@Param("questionsId") int questionsId, @Param("videoId") int videoId);
}
