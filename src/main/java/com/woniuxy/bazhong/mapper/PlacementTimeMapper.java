package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.PlacementTime;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PlacementTimeMapper {

    //添加分班时间
    public boolean addTime(PlacementTime placementTime);

    PlacementTime findPlacementTime();
}
