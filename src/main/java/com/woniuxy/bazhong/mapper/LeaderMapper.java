package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Leader;
import com.woniuxy.bazhong.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface LeaderMapper {

    public int createLeader(Leader leader);

    public int getMaxId();

    public Leader findLeaderById(int id);

    public void add(User user);

    public int findMaxUserId();

    public void addUserRole(int id);

    public List<Leader> findLeaders();

    public Leader findByName(String leaderName);

    public Leader findLeaderBySchoolId(int schoolId);
}
