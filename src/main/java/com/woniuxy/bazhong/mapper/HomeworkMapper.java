package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.*;
import com.woniuxy.bazhong.entity.HomeworkStudent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhangjy
 * @date 2022/7/23 16:02
 */
@Mapper
public interface HomeworkMapper {
    // 根据学生id查询所有作业
//    public List<Homework> all(int studentId);

    // 根据作业id查询
    public Homework findById(int id);

    // 根据学生id查询该学生所有学生作业
    public List<HomeworkStudent> findAllByStudentId(int studentId);

    // 根据学生id和作业id查询一个学生作业的信息，题目信息及学生提交的答案
    public HomeworkStudent findByStudentIdAndHomeworkId(@Param("studentId") int studentId, @Param("homeworkId") int homeworkId);

    // 查询一道题的作业学生答案
    public HomeworkAnswer findHomeworkAnswerByStuIdAndHomIdAndHomeTopicId(@Param("studentId")int studentId, @Param("homeworkId")int homeworkId, @Param("homeworkTopicId")int homeworkTopicId);

    // 查询一道选择题的所有信息（不包括作业学生答案）
    public Choice findChoice(@Param("questiontypesId") int questiontypesId, @Param("topicId") int topicId);

    // 查询一道填空题的所有信息
    public Blank findBlank(@Param("questiontypesId") int questiontypesId, @Param("topicId") int topicId);

    // 查询一道判断题的所有信息
    public Bank findBank(@Param("questiontypesId") int questiontypesId, @Param("topicId") int topicId);

    // 查询一道阅读题的所有信息
    public Reading findReading(@Param("questiontypesId") int questiontypesId, @Param("topicId") int topicId);

    // 查询一道解答题的所有信息
    public Responsequestion findResponsequestion(@Param("questiontypesId") int questiontypesId, @Param("topicId") int topicId);

    // 查询一道作文题的所有信息
    public Title findTitle(@Param("questiontypesId") int questiontypesId, @Param("topicId") int topicId);

    // 插入一道学生作业题答案
    public boolean addHomeworkAnswer(@Param("studentId") int studentId, @Param("homeworkId") int homeworkId, @Param("homeworkTopicId") int homeworkTopicId, @Param("answer") String answer);

    // 修改学生作业答案（学生提交的答案、实际得分、老师评语）
    public boolean updateHomeworkAnswer(HomeworkAnswer homeworkAnswer);

    // 修改作业学生（作业总得分、提交状态等）
    public boolean updateHomeworkStudent(HomeworkStudent homeworkStudent);

    // 自动生成作业
    public int autoHomework(Homework homework);

    public int addHomeworkTopic(HomeworkTopic homeworkTopic);

    public int addHomeworkStudent(HomeworkStudent homeworkStudent);

   public  int setFinalTime(@Param("homeworkId") int homeworkId,@Param("finalTime") String finalTime);

}
