package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.HighestScore;
import com.woniuxy.bazhong.entity.Scores;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface HighestScoreMapper {
    int findhas(HighestScore h);

    boolean addHighestScore(HighestScore h);

    boolean updateHighestScore(HighestScore h);

    List<HighestScore> findAll(@Param("classes") String classes, @Param("description") String description,@Param("schoolId")int schoolId);

    List<HighestScore> findClassStudents(@Param("classes") String classes,@Param("description") String description,@Param("schoolId")int schoolId);
}
