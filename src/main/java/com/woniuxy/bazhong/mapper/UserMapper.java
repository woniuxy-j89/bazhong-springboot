package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    public List<User> all();



    //查询一个学校的所有老师
    public List<User> findTeacherUserBySchool(int i);

    //查询某一届某一学校的所有学生用户
    public List<User> findPlacementStudent(@Param("enrollmentYear")int enrollmentYear, @Param("schoolId")int schoolId);

    int findUserByAccountAndSchoolId(@Param("account") String account, @Param("schoolId") Integer schoolId);

    boolean insertUser(User user1);

    boolean updateUser(User user1);

    int getUserIdByAccountAndSchoolId(@Param("account") String account, @Param("schoolId") int schoolId);

    boolean setStudentRole(int userId);

    boolean resetStudentPassword(int userId);

    /**
     * 重置教师密码
     * @param userId
     * @return
     */
    public int resetTeacherPassword(int userId);

    public User findUserByAccount(String account);

    /**
     * 根据用户id查询用户信息
     * @param id
     * @return
     */
    public User findById(int id);
}
