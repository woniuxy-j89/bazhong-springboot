package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface StudentMapper {
    /**
     * 根据id查找学生
     * @param id
     * @return Student
     */
    public Student findById(int id);


    /**
     * 根据classId查询学生
     * @param classId
     * @return
     */
    public List<Student> findStudentsByClassId(int classId);

    /**
     * 根据学生id修改文理科
     * @param liberaScience
     * @param id
     * @return
     */
    public boolean updateStudentChoice(@Param("liberaScience")String liberaScience,@Param("id")int id);

    Student findByUserId(int userId);
}
