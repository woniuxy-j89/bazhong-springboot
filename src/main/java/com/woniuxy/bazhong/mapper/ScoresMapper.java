package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Scores;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学生成绩
 */
@Mapper
public interface ScoresMapper {
    //根据学生id和考试名称查询各科成绩
    public List<Scores> findScoresByStId(int id);

    //查询这次考试所有的本校的学生成绩 高到低排序
    List<Scores> findAll(@Param("classes") String classes, @Param("exam") String exam,@Param("schoolId")int schoolId);

    //查询这次考试所有的本班的学生成绩
    List<Scores> findClassStudents(@Param("classes") String classes, @Param("exam")String exam,@Param("schoolId")int schoolId);

    List<Scores> findAllStudentScores(@Param("classes")String classes, @Param("exam")String exam, @Param("schoolId")int schoolId);
}
