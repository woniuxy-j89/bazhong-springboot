package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.ChatMessage;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChatMessageMapper {
    public boolean addMessage(ChatMessage chatMessage);

    List<ChatMessage> findInform(int userId);
}
