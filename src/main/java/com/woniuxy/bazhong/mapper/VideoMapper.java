package com.woniuxy.bazhong.mapper;

import com.woniuxy.bazhong.entity.Video;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface VideoMapper {


    public List<Video> findToAudit(int schoolId);


    public boolean updateStatus(Video video);

    /**
     * 视频的模糊查询
     * @param info
     * @return
     */
    public List<Video> findVideoByLike(String info);

    /**
     * 条件查询所有视频
     * @param video
     * @return
     */
    public List<Video> findAllByCondition(Video video);

    /**
     * 根据视频id查询一个视频
     * @param id
     * @return
     */
    public Video findById(int id);

    /**
     * 查询所有视频
     * @return
     */
    public List<Video> findAllVideo();
}
