package com.woniuxy.bazhong.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 全局的异常处理解析器：指定在用户无权限操作时处理方式
 */
@ControllerAdvice
public class MyExceptionHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        // 返回结果
        // 设置响应头：告知浏览器返回的是JSON数据
        httpServletResponse.setContentType("application/json;charset=utf-8");

        ResponseResult<Object> responseResult = new ResponseResult<>();
        responseResult.setCode(403);
        responseResult.setStatus(ResponseStatus.NO_PERMS);
        responseResult.setMessage("你没有权限");

        // 将对象转换成JSON
        String json = new ObjectMapper().writeValueAsString(responseResult);

        // 写出数据
        httpServletResponse.getWriter().write(json);
    }
}