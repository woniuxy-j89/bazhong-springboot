package com.woniuxy.bazhong.handler;


import com.woniuxy.bazhong.utils.WebSocketUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

@Slf4j
@RestController
@ServerEndpoint("/WebSocketHandler/{account}")		//表示接受的是STOMP协议提交的数据
public class WebSocketHandler {

    //建立连接
    @OnOpen
    public void openSession(@PathParam("account")String account,Session session) {
        //加入聊天室
        WebSocketUtil.MESSAGEMAP.put(account, session);

    }

    @OnMessage
    public void onMessage(@PathParam("account")String account,String message) {
        message = account+":"+message;

    }

    //离开聊天室
    @OnClose
    public void onClose(@PathParam("account")String account,Session session) {
        //将当前用户从map中移除 注销
        WebSocketUtil.MESSAGEMAP.remove(account);

        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //连接异常
    @OnError
    public void onError(Session session,Throwable throwable) {
        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
