package com.woniuxy.bazhong.handler;

import com.woniuxy.bazhong.utils.ResponseResult;
import com.woniuxy.bazhong.utils.ResponseStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.security.sasl.AuthenticationException;

@RestControllerAdvice
public class ClassAuthenticationExceptionHandle {

    @ExceptionHandler({AuthenticationException.class, BadCredentialsException.class})
    public ResponseResult<Object> handleAuthenticationException(Exception e){

        ResponseResult<Object> responseResult = new ResponseResult<>();

        responseResult.setCode(500);
        responseResult.setStatus(ResponseStatus.FAIL);
        responseResult.setMessage("账号密码有误");

        return responseResult;
    }
}